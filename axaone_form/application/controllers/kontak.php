<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends Ci_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('kontak_model');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('email');
		$config = array(
		    'protocol' => 'mail',
		    'smtp_host' => 'ssl://sales.axa.co.id',
		    'smtp_port' => 465,
		    'smtp_timeout'=>30,
		    'smtp_user' => 'noreply@cc.axa.co.id',
		    'smtp_pass' => '@x@indocU5t0m3rC@r3',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('excel',$config);
	}

	function index(){		
		
	}

	function submit_data(){
		date_default_timezone_set('Asia/Bangkok');

		if(empty($_POST['nama_lengkap']) && $_POST['nama_lengkap'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Nama lengkap tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
		if(empty($_POST['no_tlp']) && $_POST['no_tlp'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'No Handphone tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
		if(empty($_POST['email'])){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Email wajib diisi dengan benar!';
			echo json_encode($_POST);
			exit;
		}elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Email yang Anda masukkan tidak valid!';
			echo json_encode($_POST);
			exit;
		}
		$tanggal_lahir='';
		if(isset($_POST['tgl_lahir']) && $_POST['tgl_lahir'] != ''){
			$tanggal_lahir = $_POST['tgl_lahir'];
		}else{
			
			$_POST['status'] = 'error';
			$_POST['message'] = 'Tanggal lahir yang Anda masukkan tidak valid!';
			echo json_encode($_POST);
			exit;
		}

		if(empty($_POST['propinsi']) && $_POST['propinsi'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Propinsi tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
		if(empty($_POST['kota']) && $_POST['kota'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Kota tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
		$data = array('nama_lengkap'=> $this->input->post('nama_lengkap', TRUE),
					'no_tlp' => $this->input->post('no_tlp'),					
					'email' => $this->input->post('email', TRUE),
					'tgl_lahir' => $tanggal_lahir,
					'propinsi' => $this->input->post('propinsi', TRUE),
					'kota' => $this->input->post('kota', TRUE),
					'submit_time' => date('Y-m-d/H:i:s'),
					'product_matrix' => $this->input->post('product_matrix', TRUE),
					'nama_produk' => $this->input->post('nama_produk', TRUE),
					'banner_source' => $this->input->post('banner_source', TRUE),
					'utm_source' => $this->input->post('utm_source', TRUE),
					'utm_medium' => $this->input->post('utm_medium', TRUE),
					'utm_term' => $this->input->post('utm_term', TRUE),
					'utm_content' => $this->input->post('utm_content', TRUE),
					'utm_campaign' => $this->input->post('utm_campaign', TRUE),
					'gclid' => $this->input->post('gclid', TRUE),							
					'source'=>'lead-product'
					);
				
		$this->kontak_model->insertData('kontak',$data);
		
		/*EMAIL FEEDBACK TO REGISTRAN*/
		$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
		$this->email->from('noreply@cc.axa.co.id','Solusi AXA');
		$this->email->to($this->input->post('email'));
		$this->email->cc('');
		$this->email->bcc(''); 
		$this->email->subject("AXA | Thank You");
		$this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'.
			'<html>'.
			    '<head>'.
			        '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'.
			        '<!-- Facebook sharing information tags -->'.
			        '<meta property="og:title" content="80% Guaranteed Fund">'.
			        '<title>Selamat Datang di Dunia Kesehatan tanpa Hambatan</title>'.
				'<style type="text/css">'.
					'#outlook a{'.
						'padding:0;'.
					'}'.
					'body{'.
						'width:100% !important;'.
					'}'.
					'.ReadMsgBody{'.
						'width:100%;'.
					'}'.
					'.ExternalClass{'.
						'width:100%;'.
					'}'.
					'body{'.
						'-webkit-text-size-adjust:none;'.
					'}'.
					'body{'.
						'margin:0;'.
						'padding:0;'.
					'}'.
					'img{'.
						'border:0;'.
						'height:auto;'.
						'line-height:100%;'.
						'outline:none;'.
						'text-decoration:none;'.
					'}'.
					'table td{'.
						'border-collapse:collapse;'.
					'}'.
					'#backgroundTable{'.
						'height:100% !important;'.
						'margin:0;'.
						'padding:0;'.
						'width:100% !important;'.
					'}'.
					'body,#backgroundTable{'.
						'background-color:#ffffff;'.
					'}'.
					'#templateContainer{'.
						'border:1px solid #DDDDDD;'.
					'}'.
					'h1,.h1{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:34px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'h2,.h2{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:30px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'h3,.h3{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:26px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'h4,.h4{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:22px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'#templatePreheader{'.
						'background-color:#FAFAFA;'.
					'}'.
					'.preheaderContent div{'.
						'color:#505050;'.
						'font-family:Arial;'.
						'font-size:10px;'.
						'line-height:100%;'.
						'text-align:left;'.
					'}'.
					'.preheaderContent div a:link,.preheaderContent div a:visited,.preheaderContent div a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'#templateHeader{'.
						'background-color:#FFFFFF;'.
						'border-bottom:0;'.
					'}'.
					'.headerContent{'.
						'color:#202020;'.
						'font-family:Arial;'.
						'font-size:34px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'padding:0;'.
						'text-align:center;'.
						'vertical-align:middle;'.
					'}'.
					'.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'#headerImage{'.
						'height:auto;'.
						'max-width:600px !important;'.
					'}'.
					'#templateContainer,.bodyContent{'.
						'background-color:#FFFFFF;'.
					'}'.
					'.bodyContent div{'.
						'color:#505050;'.
						'font-family:Arial;'.
						'font-size:14px;'.
						'line-height:150%;'.
						'text-align:left;'.
					'}'.
					'.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'.bodyContent img{'.
						'display:inline;'.
						'height:auto;'.
					'}'.
					'#templateFooter{'.
						'background-color:#FFFFFF;'.
						'border-top:0;'.
					'}'.
					'.footerContent div{'.
						'color:#707070;'.
						'font-family:Arial;'.
						'font-size:12px;'.
						'line-height:125%;'.
						'text-align:left;'.
					'}'.
					'.footerContent div a:link,.footerContent div a:visited,.footerContent div a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'.footerContent img{'.
						'display:inline;'.
					'}'.
					'#social{'.
						'background-color:#FAFAFA;'.
						'border:0;'.
					'}'.
					'#social div{'.
						'text-align:center;'.
					'}'.
					'#utility{'.
						'background-color:#FFFFFF;'.
						'border:0;'.
					'}'.
					'#utility div{'.
						'text-align:center;'.
					'}'.
					'#monkeyRewards img{'.
						'max-width:190px;'.
					'}'.
			'</style></head>'.
			    '<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #ffffff;width: 100% !important;">'.
			    	'<center>'.
			        	'<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;background-color: #ffffff;height: 100% !important;width: 100% !important;">'.
			            	'<tr>'.
			                	'<td align="center" valign="top" style="border-collapse: collapse;">'.
			                        '<!-- // Begin Template Preheader \\ -->'.
			                        '<table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader" style="background-color: #FFFFFF;">'.
			                            '<tr>'.
			                            	'<td></td>'.
			                            '</tr>'.
			                        '</table>'.
			                        '<!-- // End Template Preheader \\ -->'.
			                    	'<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="border: 1px solid #FFFFFF;background-color: #FFFFFF;">'.
			                        	'<tr>'.
			                            	'<td align="center" valign="top" style="border-collapse: collapse;">'.
			                                    '<!-- // Begin Template Body \\ -->'.
			                                	'<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">'.
			                                    	'<tr>'.
			                                            '<td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">'.					                                                '<!-- // Begin Module: Standard Content \\ -->'.
			                                                '<table border="0" cellpadding="40" cellspacing="0" width="100%">'.
			                                                    '<tr>'.
			                                                        '<td valign="top" style="border-collapse: collapse;">'.
			                                                            '<div style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;"><span style="color:#003399; font-size:14px;">Dear Bpk/Ibu '.$this->input->post('nama_lengkap', TRUE).',<br>'.
																	'<br>'.
																	'Terima kasih atas ketertarikan Anda.<br>'.
																	'<p>Data Anda telah masuk ke dalam sistem dan agent kami akan segera melayani Anda. </p>'.
																	'<br>'.																	
																	'<br>'.
																	'Salam,<br>'.
																	'Tim AXA Indonesia<br>'.
																	'<br>'.
																'</td>'.
			                                                    '</tr>'.
			                                                '</table>'.

			                                                '<!-- // End Module: Standard Content \\ -->'.
			                                            '</td>'.
			                                        '</tr>'.
			                                    '</table>'.
			                                    '<!-- // End Template Body \\ -->'.
			                                '</td>'.
			                            '</tr>'.
			                        	'<tr>'.
			                            	'<td align="center" valign="top" style="border-collapse: collapse;">'.
			                                '</td>'.
			                            '</tr>'.
			                        '</table>'.
			                        '<br>'.
			                    '</td>'.
			                '</tr>'.
			            '</table>'.
			        '</center>'.
			    '</body>'.
			'</html>'
				);
		if($this->email->send()) {			
			?>
				<script> window.location = "<?php echo base_url(); ?>home"; </script>
			<?php

		}else{
			echo "Data tidak terkirim";
		}
		
		/*EMAIL FEEDBACK TO REGISTRAN*/
		
		//realtime 
		// $sql = "select * from entity_customer_care order by nama_entity asc";
		
		// $query = $this->db->query($sql);
		// $result = $query->result_array();

		// foreach($result as $row)
		// {
		// 	$this->email->clear(TRUE);
		// 	$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
		// 	$this->email->from('noreply@cc.axa.co.id','AXA Indonesia');
		// 	$this->email->to($row['email']);
		// 	$this->email->cc('digital@axa.co.id','anggi.putri@axa.co.id');
		// 	$this->email->bcc('emild@definite.co.id');
		// 	$this->email->subject("AXA indonesia [Lead Produk]");
		// 	$this->email->message(
		// 		'<p>Dear tim AXA Indonesia,</p>'. 
		// 		'<p>Terlampir registran yang mendaftar melalui halaman product page website AXA Mandiri.</br>'. 
		// 		'<p>Mohon di follow up dalam 1x24 jam untuk tetap menjaga ketertarikan pendaftar.</p>'.
		// 		'<br>'.
		// 		'<p>Nama Lengkap 	: '.$this->input->post('nama_lengkap').'</p>'.
		// 		'<p>No Tlp       	: '.$this->input->post('no_tlp').'</p>'.
		// 		'<p>Email From 		: '.$this->input->post('email').'</p>'.
		// 		'<p>Tgl Lahir 	 	: '.$tanggal_lahir.'</p>'.
		// 		'<p>Provinsi	 	: '.$this->input->post('propinsi').'</p>'.
		// 		'<p>Kota   		   	: '.$this->input->post('kota').'</p>'.
		// 		'<p>Entity Produk  	: '.$this->input->post('product_matrix').'</p>'.
		// 		'<p>Nama Produk   	: '.$this->input->post('nama_produk').'</p>'.
		// 		'<p>Waktu Input   	: '.date('Y-m-d/H:i:s').'</p>'.
		// 		'</br>'.
		// 		'</br>'.				  
		// 		'<p>Terima kasih</p>'.
		// 		'<p>Salam</p>'
		// 	);
		
		
		
	}

	function daily(){
		date_default_timezone_set('Asia/Bangkok');
		$source="lead-product";
		$sql = "select * from entity_customer_care order by nama_entity asc";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();

		foreach($result as $row)
		{
			$sql2 = "SELECT * from kontak where product_matrix = '".$row['nama_entity']."' and source='$source' and daily = 0 order by submit_time asc";
			
			$query2 = $this->db->query($sql2);
			$result2 = $query2->result_array();
			$count = 2;
			
			if ($query2->num_rows() > 0)
			{	
				$this->email->clear(TRUE);
				$sheet = new PHPExcel();
				$sheet->setActiveSheetIndex(0);
				//name the worksheet
				$sheet->getActiveSheet()->setTitle($row['nama_entity']);
				$sheet->getActiveSheet()
								->setCellValue('A1', 'No')
								->setCellValue('B1', 'Nama Lengkap')
								->setCellValue('C1', 'No. Telepon')
								->setCellValue('D1', 'Email')
								->setCellValue('E1', 'Tgl. Lahir')
								->setCellValue('F1', 'Propinsi')
								->setCellValue('G1', 'Kota')
								->setCellValue('H1', 'Product Matrix')
								->setCellValue('I1', 'Nama Produk')
								->setCellValue('J1', 'source')
								->setCellValue('K1', 'Waktu Submit');
							
				$sheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$sheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('E')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('F')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('G')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('H')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);
			

				$sheet->getActiveSheet()->getStyle('A')->getNumberFormat()->setFormatCode('0');
				
				
				$sheet->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


				foreach($result2 as $row2)
				{
					$sheet->setActiveSheetIndex()->setCellValue("A".$count, $count-1);
					$sheet->setActiveSheetIndex()->setCellValue("B".$count, $row2['nama_lengkap']);
					$sheet->getActiveSheet()->setCellValueExplicit("C".$count,$row2['no_tlp'],PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->setActiveSheetIndex()->setCellValue("D".$count, $row2['email']);
					$sheet->setActiveSheetIndex()->setCellValue("E".$count, $row2['tgl_lahir']);
					$sheet->setActiveSheetIndex()->setCellValue("F".$count, $row2['propinsi']);
					$sheet->setActiveSheetIndex()->setCellValue("G".$count, $row2['kota']);
					$sheet->setActiveSheetIndex()->setCellValue("H".$count, $row2['product_matrix']);
					$sheet->setActiveSheetIndex()->setCellValue("I".$count, $row2['nama_produk']);
					$sheet->setActiveSheetIndex()->setCellValue("J".$count, $row2['source']);
					$sheet->setActiveSheetIndex()->setCellValue("K".$count, $row2['submit_time']);
					$count++;
				}

				$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');  
				
				$objWriter->save('./axa_temp/DailyReport'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				
				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from('noreply@cc.axa.co.id','Sales Lead Web AXA Indonesia');
				$this->email->to($row['email']); 
				//$this->email->to('rianny.tambuwun@gmail.com , adhit@definite.co.id');
				$this->email->cc('digital@axa.co.id, anggie.putri@axa.co.id');
				$this->email->bcc('axa.report.dump@gmail.com');
				
				$this->email->subject('[Daily Report Leads Product]'.$row['nama_entity']);
				$this->email->message(
					'<p>Dear tim AXA,</p>'. 
					'<p>Terlampir database registrant yang mendaftar melalui halaman Product Page website AXA Indonesia.</br>'. 
					'<p>Mohon di follow up dalam 1x24 jam untuk tetap menjaga ketertarikan pendaftar.</p>'.
					  
					'<p>Terima kasih. </p>'.
					'<p>Digital AXA</p>'
				);
				$this->email->attach('./axa_temp/DailyReport'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				if($this->email->send()) {
					$data3['daily'] = 1;
					$this->db->where('daily', 0);
					$this->db->where('product_matrix', $row['nama_entity']);
					$this->db->update('kontak', $data3);

					unlink('./axa_temp/DailyReport'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				}else{
					echo "Email cannot send";
				}
				
				$count = 2;
			}
		}
		$this->daily_solution_advisor();
	}

	function daily_solution_advisor(){
		date_default_timezone_set('Asia/Bangkok');
		$source="solution-advisor";
		$sql = "select * from entity_customer_care order by nama_entity asc";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();

		foreach($result as $row)
		{
			$sql2 = "SELECT * from kontak where product_matrix = '".$row['nama_entity']."' and source='$source' and daily = 0 order by submit_time asc";
			
			$query2 = $this->db->query($sql2);
			$result2 = $query2->result_array();
			$count = 2;
			
			if ($query2->num_rows() > 0)
			{	
				$this->email->clear(TRUE);
				$sheet = new PHPExcel();
				$sheet->setActiveSheetIndex(0);
				//name the worksheet
				$sheet->getActiveSheet()->setTitle($row['nama_entity']);
				$sheet->getActiveSheet()
								->setCellValue('A1', 'No')
								->setCellValue('B1', 'Nama Lengkap')
								->setCellValue('C1', 'No. Telepon')
								->setCellValue('D1', 'Email')
								->setCellValue('E1', 'Product Matrix')
								->setCellValue('F1', 'Nama Produk')
								->setCellValue('G1', 'Perlindungan')
								->setCellValue('H1', 'Umur')
								->setCellValue('I1', 'Penghasilan')
								->setCellValue('J1', 'Status Nikah')
								->setCellValue('K1', 'Tanggungan')
								->setCellValue('L1', 'Bersedia Investasi')
								->setCellValue('M1', 'source')
								->setCellValue('N1', 'Waktu Submit');
							
				$sheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$sheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('E')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('F')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('G')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('H')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('M')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('N')->setWidth(20);
			

				$sheet->getActiveSheet()->getStyle('A')->getNumberFormat()->setFormatCode('0');
				
				
				$sheet->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


				foreach($result2 as $row2)
				{
					$sheet->setActiveSheetIndex()->setCellValue("A".$count, $count-1);
					$sheet->setActiveSheetIndex()->setCellValue("B".$count, $row2['nama_lengkap']);
					$sheet->getActiveSheet()->setCellValueExplicit("C".$count,$row2['no_tlp'],PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->setActiveSheetIndex()->setCellValue("D".$count, $row2['email']);
					$sheet->setActiveSheetIndex()->setCellValue("E".$count, $row2['product_matrix']);
					$sheet->setActiveSheetIndex()->setCellValue("F".$count, $row2['nama_produk']);
					$sheet->setActiveSheetIndex()->setCellValue("G".$count, $row2['perlindungan']);
					$sheet->setActiveSheetIndex()->setCellValue("H".$count, $row2['umur']);
					$sheet->setActiveSheetIndex()->setCellValue("I".$count, $row2['gaji']);
					$sheet->setActiveSheetIndex()->setCellValue("J".$count, $row2['mar_status']);
					$sheet->setActiveSheetIndex()->setCellValue("K".$count, $row2['tanggungan']);
					$sheet->setActiveSheetIndex()->setCellValue("L".$count, $row2['bersedia']);
					$sheet->setActiveSheetIndex()->setCellValue("M".$count, $row2['source']);
					$sheet->setActiveSheetIndex()->setCellValue("N".$count, $row2['submit_time']);
					$count++;
				}

				$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');  
				
				$objWriter->save('./axa_temp/Daily-solution-advisor-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				
				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from('noreply@cc.axa.co.id','Sales Lead Web AXA Indonesia');
				$this->email->to($row['email']); 
				//$this->email->to('rianny.tambuwun@gmail.com , adhit@definite.co.id');
				$this->email->cc('digital@axa.co.id, anggie.putri@axa.co.id');
				$this->email->bcc('axa.report.dump@gmail.com');
				
				$this->email->subject("[Daily Report Solution Advisor]".$row['nama_entity']);
				$this->email->message(
					'<p>Dear tim AXA,</p>'. 
					'<p>Terlampir database registrant yang mendaftar melalui halaman Solution Advisor website AXA Indonesia.</br>'. 
					'<p>Mohon di follow up dalam 1x24 jam untuk tetap menjaga ketertarikan pendaftar.</p>'.
					  
					'<p>Terima kasih. </p>'.
					'<p>Digital AXA</p>'
				);
				$this->email->attach('./axa_temp/Daily-solution-advisor-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				if($this->email->send()) {
					$data3['daily'] = 1;
					$this->db->where('daily', 0);
					$this->db->where('product_matrix', $row['nama_entity']);
					$this->db->update('kontak', $data3);

					unlink('./axa_temp/Daily-solution-advisor-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				}else{
					echo "Email cannot send";
				}
				
				$count = 2;
			}
		}
	}
	

}