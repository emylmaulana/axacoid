<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advisor extends Ci_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->model('advisor');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('excel');
		$config = Array(
		    'protocol' => 'mail',
		    'smtp_host' => 'ssl://sales.axa.co.id',
		    'smtp_port' => 465,
		    'smtp_timeout'=>30,
		    'smtp_user' => 'noreply@cc.axa.co.id',
		    'smtp_pass' => '@x@indocU5t0m3rC@r3',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email',$config);
	}

	function index(){		
		//$this->load->view('hubungi_kami_view');
	}

	function submit_data(){
		// date_default_timezone_set('Asia/Bangkok');
		
		if ($this->input->post('gaji')=='90000000') {
			$gaji="Rp. 20 juta - 150 juta";
		}else if($this->input->post('gaji')=="200000000"){
			$gaji="Rp. 151 juta - 225 juta";
		}else if($this->input->post('gaji')=="250000000"){
			$gaji='Rp. 225 juta - 250 juta';
		}else if($this->input->post('gaji')=='300000000'){
			$gaji="Rp. 250 juta lebih";
		}else{
			$gaji='';
		}

		if ($this->input->post('umur')=='20') {
			$umur="18 - 25";
		}else if($this->input->post('umur')=="30"){
			$umur="26 - 40";
		}
		else if($this->input->post('umur')=="50"){
			$umur="41 - 55";
		}
		else if($this->input->post('umur')=="57"){
			$umur="56 lebih";
		}else{
			$umur='';
		}

		if ($this->input->post('entity')=='axa-financial-indonesia') {
			$entity='AXA Financial Indonesia';
		}else if ($this->input->post('entity')=='axa-life-indonesia') {
			$entity='AXA Life Indonesia';
		}else if ($this->input->post('entity')=='axa-general-insurance-indonesia') {
			$entity='AXA General Insurance Indonesia';
		}else if ($this->input->post('entity')=='axa-asset-management-indonesia') {
			$entity='AXA Asset Management Indonesia';
		}else{
			$entity='';
		}
		$data = array('nama_lengkap' => $this->input->post('nama'),
					'email' => $this->input->post('email'),
					'no_tlp' => $this->input->post('tlp'),					
					'product_matrix' => $entity,					
					'nama_produk' => $this->input->post('produk'),					
					'perlindungan' => $this->input->post('perlindungan'),
					'mar_status' => $this->input->post('mar_status'),
					'umur' => $umur,
					'gaji' => $gaji,
					'tanggungan' => $this->input->post('tanggungan'),
					'bersedia' => $this->input->post('bersedia'),
					'submit_time' => date('Y-m-d/H:i:s'),							
					'banner_source' => $this->input->post('banner_source'),
					'utm_source' => $this->input->post('utm_source'),
					'utm_medium' => $this->input->post('utm_medium'),
					'utm_term' => $this->input->post('utm_term'),
					'utm_content' => $this->input->post('utm_content'),
					'utm_campaign' => $this->input->post('utm_campaign'),
					'gclid' => $this->input->post('gclid'),
					'source'=>'solution-advisor'
					);
				
		// $this->hubungi_kami_model->insertData('advisor',$data);
		
		// $this->db->insert('advisor',$data);
		if ($this->db->insert('kontak',$data)) {
			echo json_encode(array('message' =>'success'));
		}else{
			echo json_encode(array('message' =>'failed'));

		}
		/*kirim email*/
		$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE, 'priority' => 1));
		
		$this->email->from('noreply@cc.axa.co.id', 'AXA Indonesia');
		$this->email->to($this->input->post('email')); 
		$this->email->subject('AXA Indonesia');
		$this->email->message('Terima Kasih bapak/ibu '.$this->input->post('nama').'<p>Terimakasih Anda telah berminat dengan produk asuransi ini, Agen kami akan segera menghubungi Anda dalam waktu 1x24 jam.</p><br><p>Regards,</p><p>AXA Indonesia</p>');
		$this->email->send(); 

		

		// $this->email->clear(TRUE);
		// $this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
		// $this->email->from('noreply@cc.axa.co.id','AXA Indonesia');

		// if ($this->input->post('entity')=='axa-life-indonesia') {
		// 	$this->email->to('customer@axa-life.co.id');
		// 	// $this->email->to('emilddurkheim@gmail.com');

		// }
		// else if ($this->input->post('entity')=='axa-financial-indonesia') {
		// 	$this->email->to('customer@axa-financial.co.id');
		// 	// $this->email->to('emilddurkheim@gmail.com');
		// }
		
		// else if ($this->input->post('entity')=='mandiri-axa-general-insurance') {
		// 	$this->email->to('customer@axa-mandiri.co.id');
		// 	// $this->email->to('emilddurkheim@gmail.com');
		// }
		// else if ($this->input->post('entity')=='axa-life-indonesia') {
		// 	$this->email->to('customer@axa-life.co.id');
		// 	// $this->email->to('emilddurkheim@gmail.com');
		// }
		// else if ($this->input->post('entity')=='axa-mandiri-financial-services') {
		// 	$this->email->to('customer@axa-mandiri.co.id');
		// 	// $this->email->to('emilddurkheim@gmail.com');
		// }else{
		// 	$this->email->to('axa.news@axa.co.id');
		// 	// $this->email->to('emilddurkheim@gmail.com');
		// }
		
		// $this->email->cc('digital@axa.co.id','anggi.putri@axa.co.id');
		// $this->email->bcc('axa.report.dump@gmail.com'); 

		// $this->email->subject('AXA Indonesia [Solution Advisor]');
		// $this->email->message(
		// 	'<p>Dear tim AXA Indonesia,</p>'. 
		// 	'<p>Berikut data yg terdaftar di Solution Advisor</p>'. 

		// 	'<p>Email From 		: '.$this->input->post('email').'</p>'.
		// 	'<p>Perlindungan  	: '.$this->input->post('perlindungan').'</p>'.
		// 	'<p>Jenis Entity   	: '.$this->input->post('entity').'</p>'.
		// 	'<p>Nama Produk   	: '.$this->input->post('produk').'</p>'.
		// 	'<p>Nama Lengkap 	: '.$this->input->post('nama').'</p>'.
		// 	'<p>Memiliki tanggungan 	: '.$this->input->post('tanggungan').'</p>'.
		// 	'<p>Bersedia investasi 	: '.$this->input->post('bersedia').'</p>'.
		// 	'<p>No Tlp       	: '.$this->input->post('tlp').'</p>'.
		// 	'<p>Usia       		: '.$umur.'</p>'.
		// 	'<p>Penghasilan     : '.$gaji.'</p>'.
		// 	'<p>Status nikah    : '.$this->input->post('mar_status').'</p>'.
			
			
			  
		// 	'<p>Terima kasih</p>'.
		// 	'<p>Salam</p>'
		// );
		// $this->email->send();

		
	

	}

}
?>