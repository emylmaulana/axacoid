<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hub extends Ci_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Hubungi_kami_model');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('email');
		$this->load->library('excel');
	}

	function index(){		
		$this->load->view('hubungi_kami_view');
	}
	function view_sample(){		
		$this->load->view('hubungi_kami_view');
	}
	function submit_sample(){
		date_default_timezone_set('Asia/Bangkok');

		if(empty($_POST['nama']) && $_POST['nama'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Nama lengkap tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}

		$tanggal_lahir='';
		if(isset($_POST['tgl_lahir']) && $_POST['tgl_lahir'] != ''){
			$tanggal_lahir = $_POST['tgl_lahir'];
		}else{
			
			$_POST['status'] = 'error';
			$_POST['message'] = 'Tanggal lahir yang Anda masukkan tidak valid!';
			echo json_encode($_POST);
			exit;
		}

		if(empty($_POST['email'])){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Email wajib diisi dengan benar!';
			echo json_encode($_POST);
			exit;
		}elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Email yang Anda masukkan tidak valid!';
			echo json_encode($_POST);
			exit;
		}

		if(empty($_POST['alamat']) && $_POST['alamat'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Alamat tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
		if(empty($_POST['no_hp']) && $_POST['no_hp'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'No Handphone tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
		if(empty($_POST['subjek']) && $_POST['subjek'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Subjek tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}
	    if(empty($_POST['pesan']) && $_POST['pesan'] == ""){
			$_POST['status'] = 'error';
			$_POST['message'] = 'Pesan tidak boleh kosong!';
			echo json_encode($_POST);
			exit;
		}

			$data2 = array('nama' => $this->input->post('nama', TRUE),
						'tgl_lahir' => $tanggal_lahir,
						'email' => $this->input->post('email', TRUE),
						'alamat' => $this->input->post('alamat', TRUE),
						'no_hp' => $this->input->post('no_hp', TRUE),	
						'no_tlp' => $this->input->post('no_tlp'),					
						'no_polis' => $this->input->post('no_polis', TRUE),
						'kategori' => $this->input->post('kategori', TRUE),
						'subjek' => $this->input->post('subjek', TRUE),
						'pesan' => $this->input->post('pesan', TRUE),
						'submit_time' => date('Y-m-d/H:i:s'),							
						'banner_source' => $this->input->post('banner_source', TRUE),
						'utm_source' => $this->input->post('utm_source', TRUE),
						'utm_medium' => $this->input->post('utm_medium', TRUE),
						'utm_term' => $this->input->post('utm_term', TRUE),
						'utm_content' => $this->input->post('utm_content', TRUE),
						'utm_campaign' => $this->input->post('utm_campaign', TRUE),
						'gclid' => $this->input->post('gclid', TRUE),
						
						);
					
			$this->hubungi_kami_model->insertData('hubungi_kami',$data2);
			$no_polis=$this->input->post('no_polis', TRUE);
			
			$this->email->clear(TRUE);
			$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
			$this->email->from('AXA Indonesia');
			
			// $sql="select * from polis";
			// $query = $this->db->query($sql);
			// $results = $query->result_array();

			if(preg_match('/^101|507|508|307|308|506|CMS0|CL|63|60|502|201/',$no_polis)){
					//AFI
				// $this->email->to('customer@axa-financial.co.id');
				$this->email->to('emilddurkheim@gmail.com, a507@emild.com');
			}elseif(preg_match('/^100-100|100-200|1001100|1010101|1010102|201|501|502/',$no_polis)){
				//ALI
				$this->email->to('emilddurkheim@gmail.com, a1001100@emild.com');
				// $this->email->to('customer@axa-life.co.id');
			}else{
				$this->email->to('emilddurkheim@gmail.com, no@polis.com');
				// $this->email->to('axa.news@axa.co.id');

			}
			// $this->email->cc('digital@axa.co.id');
			
			$this->email->bcc('axa.report.dump@gmail.com'); 

			$this->email->subject('[Contact Us] '.$this->input->post('subjek'));
			$this->email->message(
				'<p>Dear tim AXA,</p>'. 
				'<p>Email : '.$this->input->post('email').'</p>'.
				'<p>Nama Lengkap : '.$this->input->post('nama').'</p>'.
				'<p>Alamat 	   	 : '.$this->input->post('alamat').'</p>'.
				'<p>Tgl Lahir 	 : '.$this->input->post('tgl_lahir').'</p>'.
				'<p>No Handphone : '.$this->input->post('no_hp').'</p>'.
				'<p>No Tlp       : '.$this->input->post('no_tlp').'</p>'.
				'<p>No Polis     : '.$this->input->post('no_polis').'</p>'.
				'<p>Kategori     : '.$this->input->post('kategori').'</p>'.
				'</br>'.
				'</br>'.
				'<p>Pesan : </p>'.
				'<p><i>"'.
				$this->input->post('pesan').
				'"</i></p>'.
				
				  
				'<p>Terima kasih</p>'.
				'<p>Salam</p>'
			);
			$this->email->send();

			
			?>
				<script> 
				//window.location = "<?php echo base_url(); ?>hubungi-kami/"; </script>
			<?php

	}
}
?>