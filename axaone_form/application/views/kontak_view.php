<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<html>
<head>
	<title>kontak</title>
</head>

<body>
	<h3 class="fw-normal f-17 uppercase m-bottom-0 m-top-45">Kontak</h3>

<h4 class="c-blue">Kami akan menghubungi Anda</h4>
<p class="f-16">Untuk mendapatkan informasi yang lebih lengkap tentang produk dan layanan AXA, isi formulir berikut dan kami akan menghubungi Anda</p>
		<form action="<?php echo base_url('kontak/submit_data')?>" method="post">
		<table border="0">
		<tr>
			<td>
				<input type="text" name="nama_lengkap" placeholder="Nama Lengkap"></td>
			<td>
				<input type="text" name="no_tlp" placeholder="No. Telepon">
			</td>
		</tr>
		<tr>
			<td>
				<input type="text" name="email" placeholder="Email"></td>
			<td>
				<input type="date" name="tgl_lahir" placeholder="Tanggal Lahir">
			</td>
		</tr>
		<tr>
			<td>
				<select name="propinsi" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
					<option value="Propinsi">Propinsi</option>
					<option value="Aceh">Aceh</option>
					<option value="Sumatera Utara">Sumatera Utara</option>
					<option value="Sumatera Barat">Sumatera Barat</option>
					<option value="Riau Ibukotanya">Riau Ibukotanya</option>
					<option value="Kepulauan Riau">Kepulauan Riau</option>
					<option value="Jambi">Jambi</option>
					<option value="Sumatera Selatan">Sumatera Selatan</option>
					<option value="Bangka Belitung">Bangka Belitung</option>
					<option value="Bengkulu">Bengkulu</option>
					<option value="Lampun">Lampun</option>
					<option value="DKI Jakarta">DKI Jakarta</option>
					<option value="Jawa Barat">Jawa Barat</option>
					<option value="Banten">Banten</option>
					<option value="Jawa Tengah">Jawa Tengah</option>
					<option value="Daerah Istimewa Yogyakarta">Daerah Istimewa Yogyakarta</option>
					<option value="Jawa Timur">Jawa Timur</option>
					<option value="Bali">Bali</option>
					<option value="Nusa">Nusa</option>
					<option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
					<option value="Kalimantan Utara">Kalimantan Utara</option>
					<option value="Kalimantan Barat">Kalimantan Barat</option>
					<option value="Kalimantan Tengah">Kalimantan Tengah</option>
					<option value="Kalimantan Selatan">Kalimantan Selatan</option>
					<option value="Kalimantan Timur">Kalimantan Timur</option>
					<option value="Sulawesi Utara">Sulawesi Utara</option>
					<option value="Sulawesi Barat">Sulawesi Barat</option>
					<option value="Sulawesi Tengah">Sulawesi Tengah</option>
					<option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
					<option value="Sulawesi Selatan">Sulawesi Selatan</option>
					<option value="Gorontalo">Gorontalo</option>
					<option value="Maluku">Maluku</option>
					<option value="Maluku Utara">Maluku Utara</option>
					<option value="Papua Barat">Papua Barat</option>
					<option value="Papua">Papua</option>
				</select>
			</td>
			<td>
				<input type="text" name="kota" placeholder="Kota/Area">
			</td>
		</tr>
		<tr>
			<td></td>
			<td> 
				<input type="submit" value="Kirim">
			</td>
		</tr>
	</table>
		</form>
	
</body>
</html>