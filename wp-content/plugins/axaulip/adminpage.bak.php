<?php 

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class ulipListTable extends WP_List_Table {

function get_ulip_data(){
global $wpdb;

	$example_data = $wpdb->get_results('SELECT * FROM wp_ulip_rate ORDER BY Date ASC', ARRAY_A);
	return $example_data;
}

function __construct(){
global $status, $page;

    parent::__construct( array(
        'singular'  => __( 'uliprate', 'uliplisttable' ),     //singular name of the listed records
        'plural'    => __( 'uliprates', 'uliplisttable' ),   //plural name of the listed records
        'ajax'      => false        //does this table support ajax?
	) );
	
	}
	
function column_default( $item, $column_name ) {
	switch( $column_name ) { 
        case 'Date':
			echo date("F d, Y",strtotime($item[$column_name]));	
			break;
        case 'MaestroLink_Fixed_Income_Plus_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
        case 'MaestroLink_Fixed_Income_Plus_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;			
        case 'MaestroLink_Cash_Plus_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroLink_Cash_Plus_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
        case 'MaestroLink_Equity_Plus_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroLink_Equity_Plus_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'MaestroLink_Fixed_Income_Plus_USD_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroLink_Fixed_Income_Plus_USD_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'MaestroLink_Balanced_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroLink_Balanced_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
  //       case 'MaestroPiece_Platinum_USD_BID':
		// 	echo number_format($item[$column_name],2,',','.');
		// 	break;
		// case 'MaestroPiece_Platinum_USD_OFFER':
		// 	echo number_format($item[$column_name],2,',','.');
		// 	break;	
  //       case 'MaestroPiece_Platinum_02_USD_BID':
		// 	echo number_format($item[$column_name],2,',','.');
		// 	break;
		// case 'MaestroPiece_Platinum_02_USD_OFFER':
		// 	echo number_format($item[$column_name],2,',','.');
		// 	break;	
        case 'Maestro_Equity_Syariah_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Maestro_Equity_Syariah_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'Maestro_Balance_Syariah_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Maestro_Balance_Syariah_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'MaestroLink_Dynamic_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroLink_Dynamic_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'MaestroLink_Aggresive_Equity_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroLink_Aggresive_Equity_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'AFI_Dynamic_Money_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'AFI_Dynamic_Money_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'AFI_Progressive_Money_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
        case 'AFI_Progressive_Money_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
        case 'AFI_Secure_Money_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'AFI_Secure_Money_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;		
        case 'Secure_Money_USD_BID':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'Secure_Money_USD_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;		
        case 'Money_Market_IDR_BID':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'Money_Market_IDR_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'Syariah_Dynamic_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Syariah_Dynamic_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
        case 'Syariah_Progressive_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Syariah_Progressive_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'Maestro_Progressive_Equity_Syariah_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Maestro_Progressive_Equity_Syariah_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Maestrolink_Maxi_Advantage_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Maestrolink_Maxi_Advantage_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'Maestrolink_Maxi_Advantage_GP':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroBerimbang_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'MaestroBerimbang_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'AXA_CitraDinamis_BID':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'AXA_CitraDinamis_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'CitraGold_BID':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'CitraGold_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'AXA_MaestroDollar_BID':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'AXA_MaestroDollar_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'AXA_MaestroObligasi_Plus_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'AXA_MaestroObligasi_Plus_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'AXA_MaestroSaham_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'AXA_MaestroSaham_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'ALI_Dynamic_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'ALI_Dynamic_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'ALI_Progressive_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'ALI_Progressive_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'ALI_Secure_BID':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'ALI_Secure_OFFER':
			echo number_format($item[$column_name],2,',','.');
			break;	
		case 'edit':
			echo '<a href="admin.php?page=ulip-add&id='.$item['id'].'&action=edit">edit</a>';
		}
	}

function get_columns(){
        $columns = array(
            'Date' => __( 'Date', 'uliplisttable' ),
            'MaestroLink_Fixed_Income_Plus_IDR_BID' => __( 'MaestroLink Fixed Income Plus IDR BID', 'uliplisttable' ),
            'MaestroLink_Fixed_Income_Plus_IDR_OFFER' => __( 'MaestroLink Fixed Income Plus IDR OFFER', 'uliplisttable' ),
            'MaestroLink_Cash_Plus_IDR_BID' => __( 'MaestroLink_Cash Plus IDR BID', 'uliplisttable' ),
			'MaestroLink_Cash_Plus_IDR_OFFER' => __( 'MaestroLink_Cash Plus IDR OFFER', 'uliplisttable' ),
            'MaestroLink_Equity_Plus_IDR_BID' => __( 'MaestroLink Equity Plus IDR BID', 'uliplisttable' ),
            'MaestroLink_Equity_Plus_IDR_OFFER' => __( 'MaestroLink Equity Plus IDR OFFER', 'uliplisttable' ),
            'MaestroLink_Fixed_Income_Plus_USD_BID' => __( 'MaestroLink Fixed Income Plus USD BID', 'uliplisttable' ),
            'MaestroLink_Fixed_Income_Plus_USD_OFFER' => __( 'MaestroLink Fixed Income Plus USD OFFER', 'uliplisttable' ),
            'MaestroLink_Balanced_IDR_BID' => __( 'MaestroLink Balanced IDR BID', 'uliplisttable' ),
            'MaestroLink_Balanced_IDR_OFFER' => __( 'MaestroLink Balanced IDR OFFER', 'uliplisttable' ),
            // 'MaestroPiece_Platinum_USD_BID' => __( 'MaestroPiece Platinum USD BID', 'uliplisttable' ),
            // 'MaestroPiece_Platinum_USD_OFFER' => __( 'MaestroPiece Platinum USD OFFER', 'uliplisttable' ),
            // 'MaestroPiece_Platinum_02_USD_BID' => __( 'MaestroPiece Platinum 02 USD BID', 'uliplisttable' ),
            // 'MaestroPiece_Platinum_02_USD_OFFER' => __( 'MaestroPiece Platinum 02 USD OFFER', 'uliplisttable' ),
            'Maestro_Equity_Syariah_IDR_BID' => __( 'Maestro Equity Syariah IDR BID', 'uliplisttable' ),
            'Maestro_Equity_Syariah_IDR_OFFER' => __( 'Maestro Equity Syariah IDR OFFER', 'uliplisttable' ),
            'Maestro_Balance_Syariah_IDR_BID' => __( 'Maestro Balance Syariah IDR BID', 'uliplisttable' ),
            'Maestro_Balance_Syariah_IDR_OFFER' => __( 'Maestro Balance Syariah IDR OFFER', 'uliplisttable' ),
            'MaestroLink_Dynamic_IDR_BID' => __( 'MaestroLink Dynamic IDR BID', 'uliplisttable' ),
            'MaestroLink_Dynamic_IDR_OFFER' => __( 'MaestroLink Dynamic IDR OFFER', 'uliplisttable' ),
            'MaestroLink_Aggresive_Equity_IDR_BID' => __( 'MaestroLink Aggresive Equity IDR BID', 'uliplisttable' ),
            'MaestroLink_Aggresive_Equity_IDR_OFFER' => __( 'MaestroLink Aggresive Equity IDR OFFER', 'uliplisttable' ),
            'AFI_Dynamic_Money_IDR_BID' => __( 'AFI Dynamic Money IDR BID', 'uliplisttable' ),  
            'AFI_Dynamic_Money_IDR_OFFER' => __( 'AFI Dynamic Money IDR OFFER', 'uliplisttable' ),  
            'AFI_Progressive_Money_IDR_BID' => __( 'AFI Progressive Money IDR BID', 'uliplisttable' ),  
            'AFI_Progressive_Money_IDR_OFFER' => __( 'AFI Progressive Money IDR OFFER', 'uliplisttable' ),  
            'AFI_Secure_Money_IDR_BID' => __( 'AFI Secure Money IDR BID', 'uliplisttable' ),  
            'AFI_Secure_Money_IDR_OFFER' => __( 'AFI Secure Money IDR OFFER', 'uliplisttable' ),  
            'Secure_Money_USD_BID' => __( 'Secure Money USD BID', 'uliplisttable' ),  
            'Secure_Money_USD_OFFER' => __( 'Secure Money USD OFFER', 'uliplisttable' ),  
            'Money_Market_IDR_BID' => __( 'Money Market IDR BID', 'uliplisttable' ),  
            'Money_Market_IDR_OFFER' => __( 'Money Market IDR OFFER', 'uliplisttable' ),  
            'Syariah_Dynamic_BID' => __( 'Syariah Dynamic IDR BID', 'uliplisttable' ),
            'Syariah_Dynamic_OFFER' => __( 'Syariah Dynamic IDR OFFER', 'uliplisttable' ),  
            'Syariah_Progressive_BID' => __( 'Syariah Progressive IDR BID', 'uliplisttable' ),
            'Syariah_Progressive_OFFER' => __( 'Syariah Progressive IDR OFFER', 'uliplisttable' ),
            'Maestro_Progressive_Equity_Syariah_BID' => __( 'Maestro_Progressive_Equity_Syariah_BID', 'uliplisttable' ),
            'Maestro_Progressive_Equity_Syariah_OFFER' => __( 'Maestro_Progressive_Equity_Syariah_OFFER', 'uliplisttable' ),
            'Maestrolink_Maxi_Advantage_BID' => __( 'Maestrolink_Maxi_Advantage_BID', 'uliplisttable' ),
			'Maestrolink_Maxi_Advantage_OFFER' => __( 'Maestrolink_Maxi_Advantage_OFFER', 'uliplisttable' ),
			'Maestrolink_Maxi_Advantage_GP' => __( 'Maestrolink_Maxi_Advantage_GP', 'uliplisttable' ),
			'MaestroBerimbang_BID' => __( 'MaestroBerimbang_BID', 'uliplisttable' ),
            'MaestroBerimbang_OFFER' => __( 'MaestroBerimbang_OFFER', 'uliplisttable' ),
            'AXA_CitraDinamis_BID' => __( 'AXA_CitraDinamis_BID', 'uliplisttable' ),
            'AXA_CitraDinamis_OFFER' => __( 'AXA_CitraDinamis_OFFER', 'uliplisttable' ),
            'CitraGold_BID' => __( 'CitraGold_BID', 'uliplisttable' ),
            'CitraGold_OFFER' => __( 'CitraGold_OFFER', 'uliplisttable' ),
            'AXA_MaestroDollar_BID' => __( 'AXA_MaestroDollar_BID', 'uliplisttable' ),
            'AXA_MaestroDollar_OFFER' => __( 'AXA_MaestroDollar_OFFER', 'uliplisttable' ),
            'AXA_MaestroObligasi_Plus_BID' => __( 'AXA_MaestroObligasi_Plus_BID', 'uliplisttable' ),
            'AXA_MaestroObligasi_Plus_OFFER' => __( 'AXA_MaestroObligasi_Plus_OFFER', 'uliplisttable' ),
            'AXA_MaestroSaham_BID' => __( 'AXA_MaestroSaham_BID', 'uliplisttable' ),
            'AXA_MaestroSaham_OFFER' => __( 'AXA_MaestroSaham_OFFER', 'uliplisttable' ),
            'ALI_Dynamic_BID' => __( 'ALI_Dynamic_BID', 'uliplisttable' ),
            'ALI_Dynamic_OFFER' => __( 'ALI_Dynamic_OFFER', 'uliplisttable' ),
            'ALI_Progressive_BID' => __( 'ALI_Progressive_BID', 'uliplisttable' ),
            'ALI_Progressive_OFFER' => __( 'ALI_Progressive_OFFER', 'uliplisttable' ),
            'ALI_Secure_BID' => __( 'ALI_Secure_BID', 'uliplisttable' ),
            'ALI_Secure_OFFER' => __( 'ALI_Secure_OFFER', 'uliplisttable' ),

            'edit' => __( 'Edit', 'xratelisttable' )
        );
         return $columns;
    } 

function get_sortable_columns(){
	$sortable_columns = array(
		'Date' => array('Date',true)
	);
	
	return $sortable_columns;
}
 
function prepare_items() {
	
	$per_page = 20;
	
	$columns  = $this->get_columns();
	$hidden   = array();
	$sortable = $this->get_sortable_columns();
	$this->_column_headers = array( $columns, $hidden, $sortable );
	
	$data = $this->get_ulip_data();

        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'Date'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
		
	$current_page = $this->get_pagenum();
    $total_items = count($data);
    $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
    $this->items = $data;
	
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
	} 

}

function ulip_admin_page_add(){
	require_once('edit-ulip.php');
}

function ulip_render_page(){
  $myListTable = new ulipListTable();
  echo '</pre><div class="wrap"><h2>Today\'s ULIP Rate</h2>'; 
  $myListTable->prepare_items(); 
  $myListTable->display(); 
  echo '</div>'; 
}

?>