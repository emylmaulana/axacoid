<?php
/*
Plugin Name: AXA ULIP Module
Plugin URI: 
Description: ULIP Plugin for AXA Financial Website
Version: 0.1
Author: Definite Studio
Author URI: http://www.definitelyworks.com
License: -
*/

//Initialize Admin Menu
add_action('admin_menu','register_ulip_admin_menu');

include_once dirname(__FILE__).'/adminpage.php';
include_once dirname(__FILE__).'/axaulip_class.php';

function register_ulip_admin_menu(){
	add_menu_page( 'ULIP', 'ULIP', 'manage_options', 'ulip-rate', 'ulip_render_page', '', 22 );
	$page = add_submenu_page('ulip-rate', 'Add ULIP Rate', 'Add ULIP Rate', 'manage_options', 'ulip-add','ulip_admin_page_add');
	
	$page3 = add_submenu_page('ulip-rate', 
	'Pruduct ULIP',//page title
	'Product ULIP', //menu title
	'manage_options', //capabilities
	'ulip-list', //menu slug
	'ulip_list' //function
	);
	
	//this is a submenu
	$page2 = add_submenu_page('ulip-rate', //parent slug
	'Add New Ulip', //page title
	'Add New Ulip', //menu title
	'manage_options', //capability
	'add-ulip-product', //menu slug
	'add_ulip_product'); //function

	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page('admin_ulip_list', //parent slug
	'test', //page title
	'test', //menu title
	'manage_options', //capability
	'ulip-update', //menu slug
	'ulip_update'); //function

	add_action('admin_print_scripts-'.$page,'ulip_scripts');
	add_action('admin_print_scripts-'.$page3,'ulip_scripts');
	add_action('admin_print_scripts-'.$page2,'ulip_scripts');
}

function ulip_scripts(){
	wp_enqueue_script('jquery-validate',plugins_url('/js/jquery.validate.min.js',__FILE__));
	wp_enqueue_script('init-validation',plugins_url('/js/init.validation.js',__FILE__));
	wp_enqueue_style('axaulip-style', plugins_url('/css/style.css',__FILE__));
}

?>