<?php
/*
Plugin Name: AXA ULIP Module
Plugin URI: 
Description: ULIP Plugin for AXA Financial Website
Version: 0.1
Author: Definite Studio
Author URI: http://www.definitelyworks.com
License: -
*/

//Initialize Admin Menu
add_action('admin_menu','register_ulip_admin_menu');

include_once dirname(__FILE__).'/adminpage.php';
include_once dirname(__FILE__).'/axaulip_class.php';

function register_ulip_admin_menu(){
	add_menu_page( 'ULIP', 'ULIP', 'manage_options', 'ulip-rate', 'ulip_render_page', '', 22 );
	$page = add_submenu_page('ulip-rate', 'Add ULIP Rate', 'Add ULIP Rate', 'manage_options', 'ulip-add','ulip_admin_page_add');

	add_action('admin_print_scripts-'.$page,'ulip_scripts');
}

function ulip_scripts(){
	wp_enqueue_script('jquery-validate',plugins_url('/js/jquery.validate.min.js',__FILE__));
	wp_enqueue_script('init-validation',plugins_url('/js/init.validation.js',__FILE__));
	wp_enqueue_style('axaulip-style', plugins_url('/css/style.css',__FILE__));
}

?>