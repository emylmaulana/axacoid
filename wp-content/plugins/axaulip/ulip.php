<?php

global $wpdb;

$infos = array();
$columns = $wpdb->get_results("show columns from wp_ulip_rate where Field!='Date' and Field!='id'"); 	 	
$string = '';

if($_POST['action']=='add' && wp_verify_nonce($_POST['ulip_nonce_field'],'ulip-add')){
	foreach($columns as $column){
		$fields = $column->Field;
		//$fieldsPost=$column->Field;
		if($string == ''){
			$string .= $_POST[$column->Field];
		}
		else{
			$string .= "," . "'".$_POST[$column->Field]."'";
		}
	}

	$UlipDate = date('Y-m-d H:i:s', mktime(0,0,0, $_POST['mm'],$_POST['jj'],$_POST['aa']));
	
	if(check_date_duplication()){
	
	$query = $wpdb->query("INSERT INTO wp_ulip_rate VALUES('', '$UlipDate', $string)");
		
		if($query) : $infos[] .= $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'].' ULIP rate is successfully added';
		else : $infos[] .= 'something went wrong, please try again';
		endif;
	} else {
		$infos[] .= 'There\'s already a rate for '.$_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'].', please use edit to change rate';		
	}
}



if($_POST['action']=='edit' && wp_verify_nonce($_POST['ulip_nonce_field'],'ulip-add')){
	$ulipid = $_POST['ulipid'];
	
	unset($_POST['ulipid']);
	unset($_POST['action']);
	unset($_POST['ulip_nonce_field']);
	unset($_POST['_wp_http_referer']);
	unset($_POST['jj']);
	unset($_POST['aa']);
	unset($_POST['mm']);
	unset($_POST['aa']);
	unset($_POST['submit']);

	$query = $wpdb->update('wp_ulip_rate',$_POST,array('id' => $ulipid));
	
	if( $query >= 0 && $query != '') : $infos[] .= 'ULIP rate is successfully modified'; 
	else : $infos[] .= 'something went wrong, please try again';
	endif;
}

function check_date_duplication(){
	global $wpdb;		
	$today = $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'];
	$rowdate = $wpdb->get_var("SELECT * FROM wp_ulip_rate WHERE Date LIKE '%$today%'");

	if(empty($rowdate)){
		return true;
	} else {
		return false;
	}
}

?>