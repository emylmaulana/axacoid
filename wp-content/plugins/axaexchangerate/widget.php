<?php

class AXA_Xrate_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
	 		'xrate_widget', // Base ID
			'AXA_Xrate_Widget', // Name
			array( 'description' => __( 'Widget to display Exchange Rate', 'text_domain' ), ) // Args
		);
	}

	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	public function widget( $args, $instance ) {
		global $wpdb;
		extract( $args );

		$query = $wpdb->get_row("SELECT * FROM wp_exchange_rate ORDER BY last_update DESC LIMIT 1");
		
		$timestamp = strtotime($query->last_update);
		
		setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		
		?>
		<div class="bottom-widget clearfix">
			<div class="left section">
				<h5><?php echo __('Kurs Rupiah','axa-financial') ?></h5>
				<p class="date"><?php echo strftime('%e %h %Y',$timestamp); ?></p>
			</div>
			
			<div class="right section">
				<table cellspacing="0;">
					<tr>
						<td></td>
						<td style="text-align:center;"><small><?php echo __('Beli BI','axa-financial') ?></small></td>
						<td style="text-align:center;"><small><?php echo __('Tengah BI','axa-financial') ?></small></td>
					</tr>
					<tr>
						<td>USD</td>
						<td style="text-align:center;"><span><?php echo number_format($query->beli,'2','.',','); ?></span></td>
						<td style="text-align:center;"><span><?php echo number_format($query->tengah,'2','.',','); ?></span></td>
					</tr>
				</table>
			</div>
		</div><!--end bottom widget-->
		<?php
	}

}

add_action( 'widgets_init', create_function( '', 'register_widget( "AXA_Xrate_Widget" );' ) );

?>