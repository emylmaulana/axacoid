<?php 

global $wpdb;

$infos = array();
	
if($_POST['action']=='add' && wp_verify_nonce($_POST['xrate_nonce_field'],'xrate-add') ){
		
	$beliBI = $_POST['beliBI'];
	$tengahBI = $_POST['tengahBI'];	
	$exchangeDate = date('Y-m-d H:i:s', mktime(0,0,0, $_POST['mm'],$_POST['jj'],$_POST['aa']));	
	
	if(check_xrate_currentdate()){
		$query = $wpdb->query("INSERT INTO wp_exchange_rate(kurs,beli,tengah,last_update) VALUES('".EXCHANGE_CURRENCY."',$beliBI,$tengahBI,'$exchangeDate')");
		if($query) : $infos[] .= $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'].' exchange rate is successfully added';
		else : $infos[] .= 'something went wrong, please try again';
		endif;
	} else {
		$infos[] .= 'There\'s already a rate for that day, please use edit to change rate';
	}
					
} 


if($_POST['action']=="edit" && wp_verify_nonce($_POST['xrate_nonce_field'],'xrate-add')){
	$xrateid = $_POST['xrateid'];
	$beliBI = $_POST['beliBI'];
	$tengahBI = $_POST['tengahBI'];		

	$query = $wpdb->update('wp_exchange_rate',array(
				'beli' => $beliBI,
				'tengah' => $tengahBI,
				),array(
				'xrate_id' => $xrateid
				)
			);
		
	if( $query >= 0 ) : $infos[] .= 'exchange rate is successfully modified';
	else : $infos[] .= 'something went wrong, please try again';
	endif;
}

function check_xrate_currentdate(){
	global $wpdb;		
	$today = $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'];
	$rowdate = $wpdb->get_var("SELECT * FROM wp_exchange_rate WHERE last_update LIKE '%$today%'");

	if(empty($rowdate)){
		return true;
	} else {
		return false;
	}
}

?>