<?php
/*
Plugin Name: AXA Financial Exchange Rate
Plugin URI: 
Description: Exchange Rate Plugin for AXA Financial Website
Version: 0.1
Author: Definite Studio
Author URI: http://www.definitelyworks.com
License: -
*/
?>
<?php

//Initialize Admin Menu
add_action('admin_menu','register_xrate_admin_menu');

include_once dirname(__FILE__).'/adminpage.php';
include_once dirname(__FILE__).'/widget.php';

function register_xrate_admin_menu(){
	add_menu_page( 'Exchange Rate', 'Exchange Rate', 'manage_options', 'exchange-rate', 'xrate_render_page', '', 23 );
	$page = add_submenu_page('exchange-rate', 'Add USD Exchange Rate', 'Add Rate', 'manage_options', 'xrate-add','xrate_admin_page_add');

	add_action('admin_print_scripts-'.$page,'xrate_scripts');
}

function xrate_scripts(){
	wp_enqueue_script('jquery-validate',plugins_url('/js/jquery.validate.min.js',__FILE__));
	wp_enqueue_script('init-validation',plugins_url('/js/init.validation.js',__FILE__));
	wp_enqueue_style('axaulip-style', plugins_url('/css/style.css',__FILE__));
}

//settings
define('EXCHANGE_CURRENCY', 'USD')

?>