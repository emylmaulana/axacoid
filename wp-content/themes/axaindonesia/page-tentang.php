<?php/** * Template Name: Tentang AXA Indonesia */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570028&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="content large-4">
			<h1 class="layanan"><?php the_title(); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">
		<section id="content-details" class="clearfix">
			<?php if( have_posts() ) : the_post(); ?>
				<?php the_content();?>
			<?php endif;?>

			<ul id="icons" class="clearfix">
				<li class="karyawan"></li>
				<li class="pemasaran"></li>
				<li class="nasabah"></li>
			</ul>
		</section>

		<section id="nilai-axa" class="blue-sections clearfix">
			<?php if(get_field('nilai_axa')): ?>
				<?php while(has_sub_field('nilai_axa')): ?>
					<h3><?php _e("<!--:en-->POINTS OF AXA<!--:--><!--:id-->NILAI-NILAI AXA<!--:-->"); ?></h3>
					<div class="large-6 column first-child">
						<h4><?php the_sub_field('title');?></h4>
						<?php the_sub_field('content');?>
					</div>
					<ul id="icons" class="relative large-6 column">
						<li class="profesionalism">
							<span class="tooltip absolute">
							<strong>Professionalism</strong>
								Kami berkomitmen untuk memberikan pelayanan yang optimal dan berstandar tinggi.
							</span>
						</li>

						<li class="integrity">
							<span class="tooltip absolute">
							<strong>Integrity</strong>
								Kami mengambil langkah nyata untuk memenuhi kebutuhan nasabah dan karyawan secara efisien, akurat, dan terpercaya.
							</span>
						</li>

						<li class="pragmatism">
							<span class="tooltip absolute profesionalism">
							<strong>Pragmatism</strong>
								Kami mengimplementasikan ide ke dalam langkah nyata,<br/>serta mengomunikasikannya secara jelas dan terbuka.
							</span>
						</li>

						<li class="innovation">
							<span class="tooltip absolute profesionalism">
							<strong>Innovation</strong>
								Kami juga secara konsisten menciptakan inovasi baru sebagai nilai tambah bagi semua pemangku kepentingan.
							</span>
						</li>

						<li class="spirit">
							<span class="tooltip absolute profesionalism">
							<strong>Spirit</strong>
								Kami lakukan dengan menjunjung team spirit, semangat kebersamaan sebagai satu perusahaan, AXA.
							</span>
						</li>
					</ul>
				<?php endwhile;?>
			<?php endif;?>
		</section>

		<section id="axa-group" class="clearfix">
			<h3><?php _e("<!--:en-->AXA INDONESIA COMPANY<!--:--><!--:id-->PERUSAHAAN AXA INDONESIA<!--:-->"); ?></h3>
			<h4><?php _e("<!--:en-->AXA Indonesia Company<!--:--><!--:id-->Perusahaan AXA Indonesia<!--:-->"); ?></h4>
			<p><?php _e("<!--:en-->AXA operates with a focus on life insurance, general insurance and asset management through a variety of distribution channels under PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Indonesia, and PT AXA Asset Management Indonesia.
						 <!--:--><!--:id-->AXA beroperasi dengan fokus pada asuransi jiwa, asuransi umum dan manajemen aset melalui beragam jalur distribusi dibawah PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Indonesia, dan PT AXA Asset Management Indonesia.<!--:-->"); 
				?>
			</p>
			<?php if(get_field('axa_indonesia_group')): ?>
				<ul class="group clearfix">
					<?php while(has_sub_field('axa_indonesia_group')): ?>
					<li> 
						<h4><span class="red-switch"></span><?php the_sub_field('title');?></h4>
						<?php the_sub_field('content');?>
					</li>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</section><!--end axa group-->
		
		<section id="axa-group" class="clearfix" style="display:none">
			<h3><?php _e("<!--:en-->AXA INDONESIA COMPANY<!--:--><!--:id-->PERUSAHAAN AXA INDONESIA<!--:-->"); ?></h3>
			<h4><?php _e("<!--:en-->AXA Indonesia Company<!--:--><!--:id-->Perusahaan AXA Indonesia<!--:-->"); ?></h4>
			<p><?php _e("<!--:en-->AXA operates with a focus on life insurance, general insurance and asset management through a variety of distribution channels under PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Indonesia, and PT AXA Asset Management Indonesia.
						 <!--:--><!--:id-->AXA beroperasi dengan fokus pada asuransi jiwa, asuransi umum dan manajemen aset melalui beragam jalur distribusi dibawah PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Indonesia, dan PT AXA Asset Management Indonesia.<!--:-->"); 
				?>
			</p>

			<?php if(get_field('axa_indonesia_group')): ?>
			<div class="group-tabs">
				<dl class="tabs" data-tab>
					<?php while(has_sub_field('axa_indonesia_group')): ?>
						<?php $string = str_replace(' ','', get_sub_field('title'));?>
				  		<dd class="" data-entity="<?php echo $string; ?>"><a href="#<?php echo $string;?>"><?php the_sub_field('title');?></a></dd>

				  	<?php endwhile;?>
				</dl>
				<div class="tabs-content">
					<?php while(has_sub_field('axa_indonesia_group')): ?>
					<?php $string = str_replace(' ','', get_sub_field('title'));?>
					<?php if(get_sub_field('title')=='AXA Service Indonesia'): ?>
						<?php $active="active"; ?>
					<?php else: ?>
						<?php $active=""; ?>
					<?php endif; ?>
					  <div class="content <?=$active?>" id="<?php echo $string;?>">
					  	<?php 
					   		$nama = get_sub_field("ceo"); 
					   		if($nama[0]['nama'] != ""): ?>					   
					    <div class="large-6 columns">
					    <?php else:?>
					    	<div class="large-12 columns">
						<?php endif;?>
					    	 <h4><span class="red-switch"></span><?php the_sub_field('title');?></h4>
					    	<?php the_sub_field('content');?>
					    </div>

					    <?php if(get_sub_field('ceo')): ?>
					   	<?php 
					   		$nama = get_sub_field("ceo"); 
					   		if($nama[0]['nama'] != ""): ?>
						    <div class="large-6 columns">
						    	<div class="ceo-section bg-grey o-hidden radius-all-5">
						    		<?php while(has_sub_field('ceo')): ?>
						    			<div class="large-3 columns text-center">
						    				<img src="<?php the_sub_field('photo');?>">	
						    			</div>
						    			<div class="large-9 columns">
						    				<h4><?php the_sub_field('nama');?></h4>
						    				<h5><?php the_sub_field('jabatan');?></h5>					    				
						    			</div>
						    			<div class="description clearfix">
						    				<p><?php the_sub_field('deskripsi');?></p>
						    			</div>
						    		<?php endwhile;?>
						    	</div>
						    </div>
						    <?php endif;?>
						<?php endif;?>
					  </div>
				  	<?php endwhile;?>
				</div>
			</div>				
			<?php endif;?>
		</section><!--end axa group_test-->

		<section id="board-director" class="clearfix" style="display:none;">
			<h3 class="text-center f-18">BOARD OF DIRECTORS</h3>
			<?php if(get_field('board_of_director')): ?>
				<ul class="bod-section clearfix" style="margin:0;">
					<?php while(has_sub_field('board_of_director')): ?>
					<?php if(get_sub_field('entity')!='-'): ?>
					<div class="filter <?php the_sub_field('entity'); ?>" data-entity2="<?php the_sub_field('entity'); ?>" style="display:none;">
						<li style="margin:10px;">
							<div class="panel bg-greydark o-hidden p-all-25 m-all-10 radius-all-5">
								<div class="large-3 columns p-left-0"><img src="<?php the_sub_field('image');?>"/></div>
								<div class="large-9 columns p-all-0 m-top-20">
									<h4><?php the_sub_field('name');?></h4>
									<h5><?php the_sub_field('jabatan');?></h5>
									<h6><?php echo get_sub_field_object('entity')['choices'][ get_sub_field('entity') ];  ?></h6>
								</div>
								<div class="details p-top-20" style="clear:both;">
									<p><?php the_sub_field('details');?></p>
								</div>
							</div> 
						</li>
					</div>
					<?php endif; ?>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</section><!--end board of director-->

		<section id="karir-section" class="clearfix">
			<h3 class="small-title"><?php _e("<!--:en-->CAREER<!--:--><!--:id-->KARIR<!--:-->"); ?></h3>
			<div class="large-6 column first-child">
			<h4><?php _e("<!--:en-->Be Part of AXA Indonesia<!--:--><!--:id-->Jadi Bagian dari AXA Indonesia<!--:-->"); ?></h4>
				<p><?php _e("<!--:en-->To be the company of choice for employees, AXA always create a comfortable working environment and ensures that each employee obtain various facilities to improve their competence. AXA improve employee engagement success led the company won the award for Best Employer in Indonesia 2011 from Aon Hewitt Consulting Asia Pacific.<!--:-->
							<!--:id-->Untuk menjadi perusahaan pilihan bagi karyawan, AXA senantiasa menciptakan lingkungan kerja yang nyaman dan memastikan bahwa setiap karyawan memperoleh berbagai fasilitas untuk meningkatkan kompetensi mereka. Keberhasilan AXA meningkatkan employee engagement membawa perusahaan meraih penghargaan sebagai Best Employer in Indonesia 2011 dari Aon Hewitt Consulting Asia Pacific.<!--:-->"
				); ?></p>
				<a href="<?php echo  site_url('karir');?>" class="button red">
					<?php _e("<!--:en-->Join with AXA<!--:--><!--:id-->Bergabung dengan AXA<!--:-->"); ?></a>
			</div>
			<div class="large-6 column">
				<ul class="career-list">
					<li class="world"><span><h6>Miliki kesempatan memimpin dalam perusahaan bertaraf internasional</h6></span></li>
					<li class="innovation"><span><h6>Ciptakan produk-produk inovatif dan pelayanan yang lebih baik</h6></span></li>
					<li class="environments"><span><h6>Bekerja di lingkungan yang terbuka terhadap pengembangan diri Anda</h6></span></li>
					<li class="ladder"><span><h6>Miliki kesempatan bekerja dan belajar serta mengembangkan karir</h6></span></li>
				</ul>
			</div>
		</section><!--end karir-->

		<section id="kontak" class="grey sections clearfix relative">
			<!-- <a href="" class="button floating">Download Company Profile</a> -->
			<!-- <h3 class="small-title"></h3> -->
			<h4><?php _e("<!--:en-->Financial Statements<!--:--><!--:id-->Laporan Keuangan<!--:-->"); ?></h4>

			<div class="large-4 columns show-for-large-only first-child p-left-0">
                <p class="f-16"><?php _e("<!--:en-->AXA Indonesia provides information on the annual financial statements and monthly performance reports, which penyusunananya done periodically.<!--:-->
							<!--:id-->AXA Indonesia menyediakan informasi laporan keuangan tahunan dan laporan kinerja bulanan, yang penyusunananya dilakukan secara periodik.<!--:-->"
				); ?></p>
	        </div>
	        <div class="large-7 columns">
	        		<div class="bg-greydark clearfix w-48p columns radius-all-5">
	        			<div class="box p-all-10">
	        				<h5>Laporan Kinerja Bulanan</h5>
	        				<form id="kinerja-bulanan">
					        	<select id="kinerja-bulan">
									<option>Pilih Bulan </option>
									<option value="fmonth=1"> Januari </option>
									<option value="fmonth=2"> Februari </option>
									<option value="fmonth=3"> Maret </option>
									<option value="fmonth=4"> April </option>
									<option value="fmonth=5"> Mei </option>
									<option value="fmonth=6"> Juni </option>
									<option value="fmonth=7"> Juli </option>
									<option value="fmonth=8"> Agustus </option>
									<option value="fmonth=9"> September </option>
									<option value="fmonth=10"> Oktober </option>
									<option value="fmonth=11"> November </option>
									<option value="fmonth=12"> Desember </option>
								</select>

								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'kinerja_bulanan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="kinerja-tahun">
										<option>Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo get_post_type_archive_link( 'media' ).'&fyear='.$result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>
		        				<button type="submit" class="button red right">Cari</button>
	        				</form>
	        			</div>
	        		</div>

	        		<div class="bg-greydark clearfix w-48p columns radius-all-5">
	        			<div class="box p-all-10">
	        				<h5>Laporan Triwulan dan Tahunan</h5>
	        				<form id="laporan-tahunan" action="<?php echo site_url('laporan-tahunan');?>" name="laporan-tahunan">
								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'laporan_keuangan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="laporan-tahun" name="fyear" class="required">
										<option disabled selected>Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo get_post_type_archive_link( 'media' ).$result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>
								<select id="periode" name="fperiode" class="required">
									<option disabled selected>Pilih Periode</option>
									<option value="triwulan-i">Triwulan I</option>
									<option value="triwulan-ii">Triwulan II</option>
									<option value="triwulan-iii">Triwulan III</option>
									<option value="triwulan-iv">Triwulan IV</option>
									<option value="tahunan">Tahunan</option>
								</select>
								<?php
									$args = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'laporan_entity',

									); 
								  $categories = get_categories($args);
								  
								  $select = "<select id='laporan-entity' class='required' name='fentity'>";
								  $select.= "<option disabled selected>Pilih Entity</option>";
								  
								  foreach($categories as $category){
								    if($category->count > 0){
								        $select.= "<option value='".$category->slug."'>".$category->name."</option>";
								    }
								  }
								  
								  $select.= "</select>";
								  
								  echo $select;
								?>

		        				<button type="submit" class="button red right">Cari</button>
	        				</form>
	        			</div>
	        		</div>
	        </div>
	       <!-- <div class="large-4 columns branch">
	                <strong><?php _e("<!--:en-->Branch Office<!--:--><!--:id-->Kantor Cabang<!--:-->"); ?></strong>
            		<p><?php _e("<!--:en-->Find the nearest AXA branch office in the your city<!--:--><!--:id-->Cari kantor cabang AXA terdekat di kota Anda<!--:-->"); ?></p>
        		<select id="kantor-cabang">
					<option>Pilih Kota</option>
				</select>
				<button type="input" class="button red">Cari</button>
	        </div>-->
		</section><!--end section contact us-->

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page-container-->
<script type="text/javascript">
	$(document).ready(function(){
		$(".group-tabs dd:first-child").addClass("active");
		$(".tabs-content .content:first-child").addClass("active");
		var en=$('dd:first-child').data('entity');
		console.log(en);
		$("#board-director .owl-item .filter."+en).addClass('active');
		$("div.filter."+en).show();
	});
		$('dd').click(function(){
			$("#board-director .owl-item").removeClass('active');
			var href=$(this).data('entity');
			$( "div.filter" ).not( $('.'+href)).hide();
			$("div.filter."+href).addClass('active');
			$("div.filter."+href+".active").show();

			$("#board-director .owl-item .filter."+href).each(function() {
				var entity=$("#board-director .owl-item .active").attr('class');
				var en_arr=entity[1];
				console.log(en_arr);
			 
			});
		});
</script>
<?php get_footer();