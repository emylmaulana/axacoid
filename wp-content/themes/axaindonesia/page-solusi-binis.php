<?php/** * Template Name: Solusi Archive:Bisnis */?>
<?php get_header();?>
<?php $slug = basename(get_permalink());?>
<?php if($slug=='solusi-kesehatan'):?>
	<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569870&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<?php elseif($slug=='solusi-proteksi'):?>
	<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569874&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<?php elseif($slug=='solusi-investasi'):?>
	<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569880&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<?php elseif($slug=='solusi-umum'):?>
	<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569884&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<?php endif;?>
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php the_field('header_image');?>);"></div>
		<div class="content large-6">
			<h1 class="bisnis"><?php the_title(); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
			
			<ul id="separate" class="bisnis relative" style="top:0;">
				
				<li class="selected"><a href="<?php echo site_url('/') .$slug;?>">Personal</a></li>
				<li><a href="<?php echo site_url('bisnis/') . $slug;?>">Bisnis</a></li>
			</ul>	
		</div><!--end large 6-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">
		<section id="content-details" class="clearfix">
			<?php if( have_posts() ) : the_post(); ?>
				<div class="large-9 columns f-16"><?php the_content();?></div>
				<div class="large-3 columns slides">
					<h3 class="c-blue">Tahukah Anda?</h3>
					<div id="tahukah-anda">
						<?php while(has_sub_field('tahukah_anda')): ?>
							<div class="elements">
								<img src="<?php the_sub_field('image');?>" class="block" alt="tahukah-anda"><br>
								<p><?php the_sub_field('description');?></p>
							</div>
						<?php endwhile;?>
					</div>
				</div>
			<?php endif;?>
		</section>

		<section id="body-2" class="blue-sections clearfix text-white">
			<?php if(get_field('solusi_archive_body_2')): ?>
				<?php while(has_sub_field('solusi_archive_body_2')): ?>
					<div class="large-6 columns">
						<?php the_sub_field("content");?>
					</div>
					<div class="large-6 columns text-center">
						<img src="<?php the_sub_field('image');?>" alt="solusi-archive"/>
					</div>
				<?php endwhile;?>
			<?php endif;?>
			<?php wp_reset_postdata();?>
		</section>

		<section id="manfaat-luas" class="clearfix sections">
			<h3 class="m-bottom-45"><?php _e("<!--:en-->Extensive Benefits<!--:--><!--:id-->Manfaat Luas<!--:-->"); ?></h3>
			<?php if(get_field('solusi_archive_manfaat')): ?>
			<ul class="list-with-icon small-block-grid-1 medium-block-grid-2 large-block-grid-3 clearfix">
				<?php while(has_sub_field('solusi_archive_manfaat')): ?>
				<li><a href="" class="clearfix h-85 o-hidden"><span class="icon-95x85" style="background: url(<?php the_sub_field('image');?>)"></span><p class="m-all-0 p-top-25"><?php the_sub_field('title');?></p></a></li>
				<?php endwhile;?>
			</ul>
			<?php endif;?>
			<?php wp_reset_postdata();?>
		</section><!--end manfaat luas-->


		<section id="product-listing" class="clearfix sections bg-white2 radius-bottom-5">
			<div id="floating" class="absolute top-0 right-55">
				<a href="<?php echo site_url('ulasan-pasar-bulanan'); ?>" class="button grey " style="display:none">Ulasan Pasar Bulanan</a>
				<a href="<?php echo site_url('ulasan-pasar-harian'); ?>" class="button grey " style="display:none">Ulasan Pasar Harian</a>
				<a href="<?php echo site_url('bandingkan-produk');?>" class="button red "><?php _e("<!--:en-->Compare Products<!--:--><!--:id-->Bandingkan Produk<!--:-->"); ?></a>
			</div>
			<h3><?php _e("<!--:en-->Special products for you<!--:--><!--:id-->Produk Khusus untuk Anda<!--:-->"); ?></h3>
			<h4 class="small-title m-bottom-45"><?php _e("<!--:en-->Select AXA products according to your requirements<!--:--><!--:id-->Pilih produk asuransi AXA sesuai dengan kebutuhan Anda<!--:-->"); ?></h4>
			
			<section id="options" class="clearfix" style="display:none">
				<ul id="listing-header" data-option-key="filter" class="desktop-content option-set list-with-icon small-block-grid-1 medium-block-grid-2 large-block-grid-4 clearfix m-bottom-45">
					<li class="all"><a href="#filter" class="selected clearfix o-hidden h-70" data-option-value="*"><span class="icon-78x70"></span><p class="m-all-0 p-top-18 right">Semua<br>Produk</p></a></li>
					<li class="perlindungan-rs"><a href="#filter" class="clearfix o-hidden h-70" data-option-value=".perlindungan-rumah-sakit-bisnis"><span class="icon-78x70"></span><p class="m-all-0 p-top-18 right">perlindungan<br>rumah sakit</p></a></li>
					<li class="perlindungan-kanker"><a href="#filter" class="clearfix o-hidden h-70" data-option-value=".perlindungan-dari-resiko-kanker-bisnis"><span class="icon-78x70"></span><p class="m-all-0 p-top-18 right">PERLINDUNGAN DARI<br>RESIKO KANKER</p></a></li>
					<li class="perlindungan-dental"><a href="#filter" class="clearfix o-hidden h-70" data-option-value=".perlindungan-dental-bisnis"><span class="icon-78x70"></span><p class="m-all-0 p-top-18 right">perlindungan<br>dental</p></a></li>
				</ul>
			</section>
			<?php 
			$posts = get_field('solusi_relationship');
			if( $posts ): ?>
			    <ul id="listing-items" class="clearfix desktop-content">
			    <?php foreach( $posts as $post):?>
			        <?php setup_postdata($post); ?>
			        <?php 
			        	$terms = wp_get_post_terms( $post->ID, "matrix_section" ); 
			        	$term_string = array();
			        	foreach ($terms as $term) {
			        		$term_string[] = $term->slug;
			        	}
			        	$term_strings = join(" ",$term_string);
			         ?>
			        <li class="clearfix elements <? echo $term_strings; ?>" data-category="<? echo $term_strings; ?>">
			        	<div class="box w-244 h-315 radius-all-5 bg-white relative">
			        	<a href="<?php the_permalink(); ?>" class="postThumbnail h-80 block relative">
							<span class="separate-small absolute"></span>
							<?php the_post_thumbnail('matrix_small');?>
						</a>
						<div class="details p-all-20">
			            	<h2 class="f-16 c-blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			            	<ul class="list-grey">
			            	<?php $count = 1; while(has_sub_field('matrix_manfaat')): ?>
			            	<?php if($count < 4): ?>
			            		<li class="f-12"><?php the_sub_field('title');?></li>
			            	<?php endif; ?>
			            	<?php $count++; endwhile;?>
			           	 	</ul>
			            </div>
			            <a href="<?php the_permalink(); ?>" class="view-more bg-bluelight block text-center p-all-10 f-13 absolute">Lebih Lanjut</a>
			        </div>
			        </li>
			    <?php endforeach; ?>
			    </ul>
			    <?php wp_reset_postdata();?>
			<?php endif; ?>

			<form id="product-listing-mobile" class="mobile-content" action="">
				<?php 
					$posts = get_field('solusi_relationship');
					if( $posts ): ?>
				<select name="" class="" tabindex="1">

					<option value="0"><?php _e("<!--:en-->Choose Product<!--:--><!--:id-->Pilih Produk<!--:-->"); ?></option>
					<?php foreach( $posts as $post):?>
				        <?php setup_postdata($post); ?>
				        <?php $term = wp_get_post_terms( $post->ID, "matrix_section" ); ?>
					<option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>				
					<?php endforeach; ?>
				</select>
				 <?php wp_reset_postdata();?>
				 <button type="submit" class="button red">Lihat Produk</button>
			</form>
			<?php endif; ?>

		</section><!--end product listing-->
		<script type="text/javascript">
			$("#product-listing-mobile select").change(function(){
				$("#product-listing-mobile").attr("action", this.value);
			});
		</script>



		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page-container-->

<?php get_footer();?>