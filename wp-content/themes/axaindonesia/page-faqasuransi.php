<?php/** * Template Name: Layanan Nasabah: FAQ Asuransi */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570026&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1>Layanan Nasabah</h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>
		<section id="page-half" class="white sections clearfix">
			<div class="large-8 columns p-all-0">
				 <?php if( have_posts() ) : the_post(); ?>
				 <?php the_content();?>
				 <?php endif;?>
			
				 <dl class="accordion" data-accordion="">
				  <dd>
				    <a href="#pengantar">Pengantar Asuransi</a>
				    <div id="pengantar" class="content active">
				    	<div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				    	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				    		<?php 
								$args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"pengantar-asuransi");
								query_posts( $args );
								if(have_posts()): while(have_posts()):the_post();
							?>
				     		<li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				     		<?php endwhile;?>
				    	 </ul>
				    	 <?php endif;?>
				    </div>

				  </dd>
				  <dd>
				    <a href="#klaim">Pengajuan Klaim</a>
				    <div id="klaim" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"pengajuan-klaim");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>
				  <dd>
				    <a href="#premi">Pembayaran Premi</a>
				    <div id="premi" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"pembayaran-premi");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>

				  	<dd>
				    <a href="#polis">Polis</a>
				    <div id="polis" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"polis");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>

				  <dd>
				    <a href="#reksadana">Reksa Dana dan Unit Link</a>
				    <div id="reksadana" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"reksa-dana-dan-unit-link");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>

				  <dd>
				    <a href="#istilah">Istilah Asuransi</a>
				    <div id="istilah" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"istilah-asuransi");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>

				 <dd>
				    <a href="#exlusive">Program Eksklusif AXA</a>
				    <div id="exlusive" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"program-eksklusif-axa");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>

				  <dd>
				    <a href="#lainlain">Lain-lain</a>
				    <div id="lainlain" class="content">
				    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
				      	<ul id="faq-list" class="two-list-content small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
				        <?php 
				        $args = array("post_type" => "faq","posts_per_page" =>-1, "faq_category" =>"lain-lain");
				        query_posts( $args );
				        if(have_posts()): while(have_posts()):the_post();
				      ?>
				        <li class="m-bottom-5"><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php the_title();?></strong></a></li>
				        <?php endwhile;?>
				       </ul>
				       <?php endif;?>
				    </div>
				   
				  </dd>
				</dl>

			</div>
			<aside class="columns widget w-322"> 
				<div class="m-bottom-25">
					<?php get_template_part("widget/sidebar-pencarian-form");?>
				</div>
				<div class="m-bottom-25">
					<?php get_template_part("widget/sidebar-pengajuan-klaim");?>
				</div>
			</aside>

		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>