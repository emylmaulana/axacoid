<?php/** * Template Name: Direktori:list*/?>
<?php get_header();?>
<?php 
$x =($_GET['x']) ? $_GET['x'] :'';
$y =($_GET['y']) ? $_GET['y'] :'';
$tax=($_GET['tax']) ? $_GET['tax'] :'';
$nama_providers = ($_GET['nama_providers']) ? $_GET['nama_providers'] : '-1';
$nama_produk = ($_GET['nama_produk']) ? $_GET['nama_produk'] : '-1';
$keyword=($_GET['q']) ? $_GET['q'] :'';
// $tax_=explode(",", $_GET['tax']);
?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569999&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="mapContainer" class="desktop-content"><img src="<?php bloginfo('template_url'); ?>/images/swipe.png" class="tablet-content swipe-button absolute" alt="swipe"><div id="mapContent" class="h-650"></div></div>
<div id="mobile-map" class="mobile-content"></div>
	<div id="wrapper" class="row">
		<div id="searchdir" class="p-lr-30 p-tb-15 clearfix block relative bottom-10 bg-white-70p radius-all-5" >
			<form id="caridirektori" class="m-bottom-0" name="cari-direktori" action="<?php echo site_url('direktori/rumah-sakit');?>" method="get">
				<input type="hidden" name="tax" id="tax">
				<input type="hidden" name="t" value="rumah_sakit" />
				<input type="text" name="q" class="inputdir left m-bottom-0" placeholder="Nama Rumah Sakit, Kota, Wilayah"/>
				<!-- <select name="nama_produk" id="nama_produk">
					<option value="">Pilih produk</option>
					<option value="1331">Smart Care Executive Personal</option>
				</select> -->
				<button type="submit" class="button red btn-dir right m-top-3 m-bottom-0">Cari</button>
			
			<ul id="selectEntity" class="small-block-grid-1 medium-block-grid-2 large-block-grid-4">
				<li><input type="checkbox" name="entity" id="afi" value="solusi-kesehatan"><label for="afi">Solusi Kesehatan</label></li>
				<li><input type="checkbox" name="entity" id="ali" value="solusi-proteksi"><label for="ali">Solusi Proteksi</label></li>
				<li><input type="checkbox" name="entity" id="agi" value="solusi-umum"><label for="agi">Solusi Umum</label></li>
				<!-- <li><input type="checkbox" name="entity" id="ami" value="axa-asset-management-indonesia"><label for="ami">AXA Asset Management Indonesia</label></li> -->
				
			</ul>
			<div id="advanced-search" >
			
			<?php 
				$providers = get_categories( array('taxonomy'=>'providers') );
				// $providers_string = array();
	   //      	foreach ($providers as $provider) {
	   //      		$providers_string[] = $provider->slug;
	   //      	}
	   //      	$providers_strings = join(",",$providers_string);
			?>
			<?php //$my_query = new WP_Query('post_type=product_matrix&providers='.$providers_strings.'&post_per_page=-1'); ?>  
			<select name="nama_providers" id="nama_provider" class="postform">
					<option value="-1">Pilih Provider</option>
			     <?php foreach ($providers as $provider): {
			    	# code...
			    } ?>
			    	 <option value="<?php echo $provider->slug; ?>"><?php echo $provider->name; ?></option>
			    <?php endforeach;?>
			</select>  
			<?php 
				$providers = get_categories( array('taxonomy'=>'providers') );
				$providers_string = array();
	        	foreach ($providers as $provider) {
	        		$providers_string[] = $provider->slug;
	        	}
	        	$providers_strings = join(",",$providers_string);
			?> 
			<?php $products = get_categories( array('taxonomy'=>'hospital_products','hide_empty'=>0) );
			// var_dump($products);?>
			<select name="nama_produk" id="nama_produk" class="postform">
					<option value="-1"><?php _e("<!--:en-->Select Product<!--:--><!--:id-->Pilih Produk<!--:-->"); ?></option>
			    <?php #while ($my_query->have_posts()) : $my_query->the_post(); ?>
			    <?php foreach ($products as $product):?>
			    	 <option value="<?php echo $product->slug;?>"><?php echo $product->name; ?></option>
			    	<?php endforeach;?>
			    <?php #endwhile;?>
			    <?php# wp_reset_query(); ?>
			</select>   
			</form>    
			</div>
		</div>
		
		<section class="bg-white clearfix relative radius-all-5">
			<div class="mobile-content p-lr-15 p-tb-15">
				<h3 class="f-16 c-blue m-bottom-10">Pilih jasa bantuan yang Anda butuhkan</h3>
				<select id="dir-menu">
					<option value="<?php echo site_url('direktori');?>">Seluruh Direktori</option>
					<option value="<?php echo site_url('direktori/rumah-sakit');?>"  selected="selected">Rumah Sakit</option>
					<option value="<?php echo site_url('direktori/bengkel');?>">Bengkel</option>
				</select>
			</div>
			<ul id="chooseDir" class="desktop-content clearfix p-all-30 small-block-grid-1 medium-block-grid-1 large-block-grid-3">
				<li class="clearfix semua">
					<a href="<?php echo site_url('direktori');?>?x=<?php echo $x."&";?>y=<?php echo $y;?>">
						<span class="icon left"></span>
						<div class="details right">
							<strong class="f-20 block">Seluruh Direktori</strong>
							<p class="f-12">Temukan Rumah Sakit dan Bengkel AXA</p>
						</div>
					</a>
				</li>
				<li class="clearfix rumah-sakit selected">
					<a href="<?php echo site_url('direktori/rumah-sakit');?>?t=rumah_sakit&x=<?php echo $x."&";?>y=<?php echo $y;?>">
						<span class="icon left"></span>
						<div class="details right">
							<strong class="f-20 block">Rumah Sakit</strong>
							<p class="f-12">Rumah Sakit &amp; Klinik Rekanan AXA</p>
						</div>
					</a>
				</li>
				<li class="clearfix bengkel">
					<a href="<?php echo site_url('direktori/bengkel');?>?t=bengkel&x=<?php echo $x."&";?>y=<?php echo $y;?>">
						<span class="icon left"></span>
						<div class="details right">
							<strong class="f-20 block">Bengkel</strong>
							<p class="f-12">Bengkel mobil rekanan AXA untuk pemegang polis AXA General</p>
						</div>
					</a>
				</li>
			</ul>
		</section>
		<section id="maincontent">
			<div id="direktori-wrapper" class="large-8 columns clearfix">
				<div id="geodata"></div>
					<div id="head-direktori" class="clearfix p-lr-30 p-tb-15 direktori-list ">
						<div style="display:none">
							<span id="lat"></span>
							<span id="long"></span>
						</div>
						<h1 class="c-blue m-bottom-0 uppercase f-24"><?php the_title();?></h1>
						<p class="m-all-0">*Rekanan AXA berlaku bagi pemilik produk asuransi kesehatan AXA.</p>
					</div>
				<ul id="main-direktori" class="m-all-0 list-style-none bg-greylight radius-all-5 o-hidden">

					<?php 
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
						$i = ceil(($paged - 1) * 10);
						$tax_arr=explode(",", $_GET['tax']);
						$tax_count=count($tax_arr);
						global $wpdb;

						//query baru
						if($x!=''&& $y!=''){
							$orderdistance="order by distance asc";
							$distance=",( 6371 * ACOS( COS( RADIANS(".$x."))*COS(RADIANS(directory_advanced.latitude))*COS(RADIANS(directory_advanced.longitude)-RADIANS(".$y."))+SIN(RADIANS(".$x."))*SIN(RADIANS(directory_advanced.latitude)))) AS distance";
						}else{
							$distance="";
							$orderdistance=" order by post_title asc";
						}

						if($keyword != '' && $tax == '' && $nama_providers=='-1' && $nama_produk=='-1'){//keyword 
							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  // 'paged' => $paged,
									  // 's' => $keyword
								);
							$sql= $wpdb->get_results("select *$distance from directory_advanced	
																where ((directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
																) $orderdistance");
							// echo "select *".$distance." from directory_advanced	
							// 									where ((directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
							// 									) $orderdistance";
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									// $args['orderby'] = 'post__in';
									$args['paged']=$paged;
									// $args['order']=ASC;
						}
						elseif($tax != '' && $nama_providers!='-1' && $keyword!='' && $nama_produk=="-1"){//tax, providers, keyword 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC,
									  'paged' => $paged
									  
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select*$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select*$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
								
								
							}
							$sql = array_splice($sql, 0);
							
							$new_array_args_id = array();

							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									// $args['orderby'] = 'post__in';
									// $args['paged']=$paged;
									// $args['order']=ASC;
							// var_dump($args);

						}
						elseif($tax != '' && $nama_providers!='-1' && $keyword=='' && $nama_produk=='-1'){//tax, providers 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged,
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
								// echo "select *$distance from directory_advanced	
								// 							where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
								// 							and (directory_advanced.providers='".$nama_providers."')) $orderdistance";
							}
							// var_dump($sql);	
							$sql = array_splice($sql, 0);

							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
									
							// var_dump($args);

						}
						elseif($tax != '' && $nama_providers=='-1' && $keyword=='' && $nama_produk!='-1'){//tax, produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance");
								// echo "select *$distance from directory_advanced	
								// 							where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
								// 							and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance";
							}
							// var_dump($sql);	
							$sql = array_splice($sql, 0);

							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
									
							// var_dump($args);

						}
						elseif($tax != '' && $nama_providers!='-1'&& $nama_produk!='-1' && $keyword=='' ){//tax, providers, produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
							}
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

						}
						elseif($tax == '' && $keyword!='' && $nama_providers!='-1' && $nama_produk!='-1' ){ //provider, produk, keyword

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

						}
						elseif($tax != '' && $keyword!='' && $nama_providers!='-1' && $nama_produk!='-1' ){//tax,keyword,providers,produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							// echo "select *$distance from directory_advanced	
							// 								where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
							// 								and (directory_advanced.providers='".$nama_providers."')
							// 								and (directory_advanced.nama_produk like '%".$nama_produk."%')
							// 								and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance";
							}
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

						}
						elseif($tax=='' && $keyword == '' && $nama_providers!='-1' && $nama_produk=='-1') {//providers
							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  //'s'=>$keyword,
									  //'paged' => $paged
									  //'solusi'=>$tax
									 // 'tax_query'=>array(
									 // 				'relation'=>'OR',
										// 				array(
										// 						'taxonomy'=>'solusi',
										// 						'field'=>'slug',
										// 						'terms'=>$tax_arr
																
										// 					)
										// 				)
									);	
							
							
								$sql = $wpdb->get_results("select*$distance from directory_advanced	
															where (directory_advanced.providers='".$nama_providers."') $orderdistance");
							
							
							
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									//$args['orderby'] = 'post__in';
									$args['paged']=$paged;
									//$args['order']=ASC;
							// var_dump($args);
						}
						elseif($tax=='' && $keyword != '' && $nama_providers!='-1' && $nama_produk=='-1') {//providers, keyword
							$args = array("post_type" => "rumah_sakit",
															  "posts_per_page" =>10, 
															  'orderby' => 'post__in', 
															  'order' => ASC,
															  'paged' => $paged
															  
															);
													
							$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.providers='".$nama_providers."' ) 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															) $orderdistance");
							// echo "select *$distance from directory_advanced	
							// 								where ((directory_advanced.providers='".$nama_providers."' ) 
							// 								and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
							// 								) $orderdistance";
							$sql = array_splice($sql, 0);
							
							$new_array_args_id = array();

							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									// $args['orderby'] = 'post__in';
									// $args['paged']=$paged;
									// $args['order']=ASC;
							// var_dump($args);
						}
						elseif($tax =='' && $nama_providers!='-1'&& $nama_produk!='-1' && $keyword=='' ){//providers, produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							
							$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
							
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
							// echo "select *$distance from directory_advanced	
							// 								where ((directory_advanced.providers='".$nama_providers."')
							// 								and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance";
									
							// var_dump($args);

						}
						elseif($tax != '' && $keyword!='' && $nama_providers=='-1' && $nama_produk!='-1' ){//tax,keyword,produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' )
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							// echo "select *$distance from directory_advanced	
							// 								where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."')
							// 								and (directory_advanced.nama_produk like '%".$nama_produk."%')
							// 								and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance";
							}
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

						}
						elseif($tax== '' && $keyword!='' && $nama_providers=='-1' && $nama_produk!='-1' ){//keyword,produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
								echo "select *$distance from directory_advanced	
															where ((directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance";
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

						}
						elseif($tax== '' && $keyword=='' && $nama_providers=='-1' && $nama_produk!='-1' ){//produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>10, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where (directory_advanced.nama_produk like '%".$nama_produk."%') $orderdistance");
							
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

						}
						else{ // default
							$args = array("post_type" => "rumah_sakit",
										  "posts_per_page" =>10, 
										  'orderby' => 'title', 
										  'order' => ASC
										  // 'paged' => $paged,
										  // 's' => $keyword

									);
							if(isset($x) && $x && $y != ''){
							$post_id_and_distance=$wpdb->get_results("SELECT post_id, post_title, ( 6371 * ACOS( COS( RADIANS($x))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS($y))+SIN(RADIANS($x))*SIN(RADIANS(latitude)))) AS distance
																	FROM  `directory_coordinates` 
																	ORDER BY  `distance` ASC");	
							$post_id_and_distance = array_splice($post_id_and_distance, 0);	
							$new_array_post_id = array();
							foreach($post_id_and_distance as $row => $value)
								{
									$new_array_post_id[] = intval($value->post_id);
								}

									$args['post__in'] = $new_array_post_id;
									$args['orderby'] = 'post__in';
									$args['paged']=$paged;
									$args['order']=ASC;
									
									// var_dump($post_id_and_distance);


							}
						}

						//end query baru


						// if($tax != '' && $keyword == '' ){
						// 	$args = array("post_type" => "rumah_sakit",
						// 			  "posts_per_page" =>10, 
						// 			  'orderby' => 'title', 
						// 			  'order' => ASC, 
						// 			  'paged' => $paged,
						// 			 'tax_query'=>array(
						// 			 					'relation'=>'AND',
						// 								array(
						// 										'taxonomy'=>'solusi',
						// 										'field'=>'slug',
						// 										'terms'=>$tax_
																
						// 									)
						// 								)
						// 			);	
						// }
						// if(isset($x) && $x && $y != ''){ //memfilter latitude dan longitude, kalo ada maka akan lanjut ke query untuk men-sorting jarak terdekat.
						// /*
						// note :
						// directory_coordinates adalah table view hasil custom query dari wp-posts untuk menagmbil latitude dan longitude dari setiap post.
						// */ 
						// 	$post_id_and_distance=$wpdb->get_results("SELECT post_id, post_title, ( 6371 * ACOS( COS( RADIANS($x))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS($y))+SIN(RADIANS($x))*SIN(RADIANS(latitude)))) AS distance
						// 											FROM  `directory_coordinates` 
						// 											ORDER BY  `distance` ASC");	
						// 	$post_id_and_distance = array_splice($post_id_and_distance, 0);	
						// 	$new_array_post_id = array();
						// 	foreach($post_id_and_distance as $row => $value)
						// 		{
						// 			$new_array_post_id[] = intval($value->post_id);
						// 		}

						// 			$args = array(
						// 		   'post_type' => array('rumah_sakit'),
						// 		   "posts_per_page" =>10,
						// 		   'post__in'   	=> $new_array_post_id,
						// 		   'orderby'  		=> 'post__in',
						// 		   'order' 			=> ASC,
						// 		   'paged'			=> $paged
						// 		);
						// 	}
						// else{
						// 	$args = array("post_type" => "rumah_sakit","posts_per_page" =>10, 'orderby' => 'title', 'order' => ASC, 'paged' => $paged);
						// }
						$myquery= new WP_Query( $args );
						if($myquery->have_posts()): while($myquery->have_posts()):$myquery->the_post();
						$field = get_field_object('jenis_rs');
						$value = get_field('jenis_rs');
						$label = $field['choices'][ $value ];
						$locations = get_field('rs_map');
						$coords = explode(",", $location['coordinates']);
						$terms = get_the_terms($post->id, 'solusi');
						$location = explode(',', $locations['coordinates']);
					?>

					<li class="adr clearfix direktori-list p-lr-30 p-tb-15 c-grey"  data-latitude="<?php echo $location[0]?>" data-longitude="<?php echo $location[1]?>">
						<div class="details left">
							<?php if ($post->post_type == "rumah_sakit"):?>
							<strong class="block f-18 c-blue"><?php the_title();?>, <?php echo $label;?></strong>
							<?php else:?>
							<strong class="block f-18 c-blue"><?php the_title();?></strong>
							<?php endif;?>
							<div class="mobile-map-details">
								<a class="get-direction" target="_blank" href="https://www.google.com/maps/dir//''/@<?php echo $locations['coordinates']?>,15z/data=!4m6!4m5!1m0!1m3!2m2!1d<?php echo $location[1]?>!2d<?php echo $location[0]?>" class="c-blue f-14 maps-link">
									<span class="left bg-iconlocation"></span>Get direction <span class="mobile-distance"></span><span class="c-blue"><i class="fa fa-chevron-circle-right"></i></span></a>
							</div>
							<span><i class="fa fa-map-marker street-address"></i> <?php the_field('rs_alamat');?></span><br/>

							<?php if(get_field('rs_telepon')):?><span>
								<i class="fa fa-phone"></i> <?php the_field('rs_telepon');?> 
							<?php endif;?>
							<?php if(get_field('rs_fax')):?><span>
								&nbsp;&nbsp; <i class="fa fa-print"></i> <?php the_field('rs_fax');?></span>
							<?php endif;?>
							<br/>
							<?php foreach ($terms as $term) {?>
								<span class="tag f-12 p-all-5 bg-white radius-all-5 c-grey"><?php echo $term->name; ?></span>
							<?php } ?>	
						</div>

					<div class="map-details text-right right ">
							<a class="c-red f-14 view-map" href="#" data-id-infowindow="<?php echo $i; ?>" data-location="<?php echo $locations['coordinates']; ?>">Lihat peta</a><br/>
							<span class="nearby f-14 distance"></span><br/>
							<a target="_blank" href="https://www.google.com/maps/dir//''/@<?=$locations['coordinates']?>,15z/data=!4m6!4m5!1m0!1m3!2m2!1d<?=$location[1]?>!2d<?=$location[0]?>" class="c-red f-14 maps-link">Get direction >></a>
						</div>
					</li>
						<?php $i++; endwhile;?>
						<li class="direktori-list text-center clearfix block p-tb-20"> <?php wp_pagenavi( array( 'query' => $myquery )); ?></li>
						<?php  wp_reset_postdata(); ?>
					<?php endif;?>
				
				</ul>
			</div>
			<?php wp_reset_query(); ?>
			<aside class="columns w-322 desktop-content">
				<div class="widget"><?php get_template_part("widget/footer-banner-left");?></div>
				<div class="widget"><?php get_template_part("widget/footer-banner-other");?></div>
				<div class="widget"><?php get_template_part("widget/footer-banner-right");?></div>
			</aside>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div>
<?php get_template_part("widget/hargaunit");?>

<script type="text/javascript">
		var markers = [];
		var center = new google.maps.LatLng("<?php if(isset($coord)){ echo $coord[0]; }else{ echo "-5.3107012"; }?>", "<?php if(isset($coord)){ echo $coord[1]; }else{ echo "119.8605666"; } ?>");
		var map = new google.maps.Map(document.getElementById('mapContent'), {
          zoom: <?php if(isset($coord)){ echo "15"; }else{ echo "5"; } ?>,
          center: center,
         scrollwheel: false
        });
		var infowindowArray = [];
		function addInfoWindow(marker, message) {
            var info = message;

            var infoWindow = new google.maps.InfoWindow({
                content: message
            });

            google.maps.event.addListener(marker, 'click', function () {
				map.setCenter(marker.getPosition());
				map.panBy(0, -100);
				for (var i = 0; i < infowindowArray.length; i++)
					infowindowArray[i].close();
				infoWindow.open(map, marker);
            });
			infowindowArray.push(infoWindow);
        }
		function initialize() {
			jQuery.getJSON('<?php echo site_url();?>/?page_id=2912&q=<?=$keyword?>&t=rumah_sakit&tax=<?=$tax?>&nama_providers=<?=$nama_providers?>&nama_produk=<?=$nama_produk?>&x=<?php echo $x."&";?>y=<?php echo $y;?>' ,function(data){				for (var i = 0; i < data.length; i++) {
					var dataPhoto = data[i];
					var iconpng = data[i].options.iconpng;
					var latLng = new google.maps.LatLng(dataPhoto["latLng"][0],dataPhoto["latLng"][1]);

					var marker = new google.maps.Marker({
						position: latLng,
						icon: iconpng					
					});
					addInfoWindow(marker, dataPhoto["data"]);
					markers.push(marker);
				}
				var mcOptions = {maxZoom: 15};
				var markerCluster = new MarkerClusterer(map, markers, mcOptions);
			});
		}
      	google.maps.event.addDomListener(window, 'load', initialize);

      	$(document).ready(function(){
      		
      		$('input[name=entity]').change(function(){
      			var tax = new Array();
      			$('input[name=entity]:checked').each(function(){
      				tax.push(this.value);
      			});
      			tax = tax.join(",");
      			$('#tax').val(tax);
      		});
			
			//console.log(latLang);
			$(".view-map").click(function(){
				var latLang = $(this).attr("data-location");
				var idinfowindow = $(this).attr("data-id-infowindow");
	       		var latlon_array = latLang.split(','); 
    	  		var lat = latlon_array[0]; 
      			var lon = latlon_array[1]; 
      			var b = new google.maps.LatLng(lat,lon);

      			 $('html, body').animate({
			          scrollTop: $("#mapContainer").offset().top
			      }, 1000);

				map.setCenter(b);
				google.maps.event.trigger(markers[idinfowindow], 'click');
				map.setZoom(16);
				
			});
		});

      		$(document).ready(function() {
			    //$("#direktori-wrapper").geolocator({ distanceBigStr: 'km', distanceSmallStr: 'm',debugMode: true, sorting: false, enableHighAccuracy: true });
				var currentLat;
				var currentLong;


		     function initGeolocation()
		     {
		        if( navigator.geolocation )
		        {
		           // Call getCurrentPosition with success and failure callbacks
		           navigator.geolocation.getCurrentPosition( success, fail );
		        }
		        else
		        {
		           alert("Sorry, your browser does not support geolocation services.");
		        }
		     }

		     function success(position)
		     {

		         $('#long').text(position.coords.longitude);
		         $('#lat').text(position.coords.latitude);
		         <?php if($_GET["x"] == null && $_GET["y"] == null || $_GET["x"] =='' && $_GET["y"] ==''): ?>
			         thisUrl="<?php echo site_url('direktori/rumah-sakit');?>"
			         window.location.href=thisUrl+"?t=rumah_sakit&x="+position.coords.latitude+"&y="+position.coords.longitude+"&q=<?=$keyword?>&tax=<?=$tax?>&wilayah=<?=$wilayah?>&nama_providers=<?php echo $nama_providers;?>&nama_produk=<?=$nama_produk?>";
		         <?php endif; ?>
		     }

		     function fail()
		     {
		        // Could not obtain location
		     }
				
		     initGeolocation();

		     
			});



		function distance(lat1,lon1,lat2,lon2) {
       var R = 6371; // km (change this constant to get miles)
       var dLat = (lat2-lat1) * Math.PI / 180;
       var dLon = (lon2-lon1) * Math.PI / 180;
       var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
               Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
               Math.sin(dLon/2) * Math.sin(dLon/2);
       var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
       var d = R * c;
       if (d>1) return Math.round(d)+"km";
       else if (d<=1) return Math.round(d*1000)+"m";
       return d;
}


$(window).load(function(){

	currentLat = $('#lat').html();
		currentLong = $('#long').html();
		//only calculate distance when found 
		if(currentLat != "" && currentLong !="") {
			$('.distance').each(function(){		
				var latLang = $(this).prev().prev().attr("data-location");
				var idinfowindow = $(this).attr("data-id-infowindow");
					var latlon_array = latLang.split(','); 
					var lat = latlon_array[0]; 
					var lon = latlon_array[1]; 
				$(this).text(distance(currentLat,currentLong,lat,lon));
				$(this).parent().prev().find(".mobile-distance").text(" ("+distance(currentLat,currentLong,lat,lon)+") ");
			});	
		}else{
			$('.distance').each(function(){		
				$(this).text("");
			});
		}	

});

</script>

<?php #var_dump($post);?>
<?php get_footer();?>