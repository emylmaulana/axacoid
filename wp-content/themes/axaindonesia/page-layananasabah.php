<?php/** * Template Name: Layanan Nasabah */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570009&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1 class="layanan"><?php _e("<!--:en-->Customer Service<!--:--><!--:id-->Layanan Nasabah<!--:-->"); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>

		<section id="premi" class="clearfix sections grey">
			<h3 class="m-bottom-45"><?php _e("<!--:en-->Popular Service<!--:--><!--:id-->Layanan Terpopuler<!--:-->"); ?></h3>
			<div  id="page-half">
			<div class="large-8 columns clearfix">
				<div class="clarge-6 columns bg-white clearfix w-48p relative">
					<div class="box  p-all-15 clearfix">
						<h5 class="f-16 c-blue"><?php _e("<!--:en-->PREMIUM PAYMENT<!--:--><!--:id-->PEMBAYARAN PREMI<!--:-->"); ?></h5>
						<p><?php _e("<!--:en-->AXA Indonesia are always ready to help manifest your desires in planning and preparing to welcome the best protection an uncertain future. To increase your comfort, AXA Indonesia provides various solutions ease the premium payment service continued.<!--:-->
									<!--:id-->AXA Indonesia selalu siap untuk membantu mewujudkan keinginan Anda dalam mempersiapkan perencanaan serta perlindungan terbaik untuk menyambut masa depan yang pasti. Untuk meningkatkan kenyamanan Anda, AXA Indonesia memberikan berbagai solusi kemudahan dalam pelayanan pembayaran premi lanjutan.<!--:-->"); 
						?></p>
						<!-- <ul class="list-with-arrow">
							<li><a href="https://www.axa-financial.co.id/wp-content/uploads/2012/11/AXA-Virtual-Account-via-BCA-ID.pdf" target="_blank"><?php _e("<!--:en-->Payment via Virtual Account BCA<!--:--><!--:id-->Pembayaran melalui Virtual Account BCA<!--:-->"); ?></a></li>
							<li><a href="https://www.axa-financial.co.id/wp-content/uploads/2012/11/AXA-Virtual-Account-via-Bank-Mandiri-ID.pdf" target="_blank"><?php _e("<!--:en-->Payment via Virtual Account Bank Mandiri<!--:--><!--:id-->Pembayaran melalui Virtual Account Bank Mandiri<!--:-->"); ?></a></li>
						</ul> -->
						<a href="<?php echo site_url('layanan-nasabah/pembayaran-premi/');?>" class="button red small right"><?php _e("<!--:en-->Premium Payment Method<!--:--><!--:id-->Cara Pembayaran Premi<!--:-->"); ?></a>
					</div>
				</div><!--end content box-->

				<div class="clarge-6 columns bg-white clearfix w-48p relative">
					<div class="box  p-all-15 clearfix">
						<h5 class="f-16 c-blue"><?php _e("<!--:en-->CLAIM ONLINE & CHECKING STATUS OF CLAIM<!--:--><!--:id-->PENGAJUAN KLAIM ONLINE & PENGECEKAN STATUS KLAIM<!--:-->");?></h5>
						<p><?php _e("<!--:en-->Enjoy the ease of travel insurance claims reported and view the status of your vehicle insurance claims whenever, wherever you are.<!--:-->
									<!--:id-->Nikmati kemudahan melaporkan klaim asuransi perjalanan dan melihat status klaim asuransi kendaraan Anda kapanpun, dimanapun Anda berada. <!--:-->"); 
						?></p>
						<ul class="list-with-arrow">
							<li><a href="https://indonesia.merimen.com/claims/index.cfm?fusebox=MTRcmt&fuseaction=dsp_claimant&INS=PTAXA" target="_blank"><?php _e("<!--:en-->Vehicle Insurance Claim Status<!--:--><!--:id-->Status Klaim Asuransi Kendaraan<!--:-->"); ?></a></li>
							<li><a href="https://www.axa-insurance.co.id/form-terms-conditions.aspx?form_id=501" target="_blank"><?php _e("<!--:en-->Travel Insurance Claims<!--:--><!--:id-->Pengajuan Klaim Asuransi Perjalanan<!--:-->"); ?></a></li>
						</ul>
					</div>
				</div><!--end content box-->


			</div><!--end large 8-->

			
			<aside class="columns w-322 desktop-content">
				<div class="widget"><?php get_template_part("widget/sidebar-pengajuan-klaim");?></div>
				
			</aside>
			</div>
		</section>

		<section id="page-half" class="sections clearfix grey-3">
			<div class="large-8 columns p-all-0">
				<h5 class="f-16"><?php _e("<!--:en-->INSURANCE FAQ<!--:--><!--:id-->FAQ ASURANSI<!--:-->"); ?></h5>
				<p><?php _e("<!--:en-->Find answers to questions about the most popular insurance services and other general information<!--:--><!--:id-->Temukan jawaban dari pertanyaan-pertanyaan terpopuler tentang layanan asuransi dan informasi umum lainnya<!--:-->"); ?>
				<ul id="faq-list" class="desktop-content two-list-content small-block-grid-1 medium-block-grid-2 large-block-grid-2 clearfix">
				<?php 
					$args = array("post_type" => "faq","posts_per_page" =>8);
					query_posts( $args );
					if(have_posts()): while(have_posts()):the_post();
				?>
					<li><a href="<?php the_permalink();?>" title="<?php the_title();?>" class="f-13"><strong><?php if (strlen($post->post_title) > 60) {
echo substr(the_title($before = '', $after = '', FALSE), 0, 60) . '...'; } else {
the_title();
} ?></strong></a></li>
					<?php endwhile;?>
				</ul>
				<?php endif;?>
				<?php wp_reset_query(); ?>
				<a href="<?php echo site_url('layanan-nasabah/faq-asuransi/');?>" class="button red small right"><?php _e("<!--:en-->All Answer & Questions<!--:--><!--:id-->Semua Pertanyaan & Jawaban<!--:-->"); ?></a>
			</div>
			<aside class="columns w-322 desktop-content"> 
				<?php get_template_part("widget/sidebar-axa-akses");?>
			</aside>
		</section>

		<section  id="page-half" class="sections clearfix grey">
			<div class="large-8 columns p-all-0">
				<h5 class="f-16 c-blue"><?php _e("<!--:en-->FORM<!--:--><!--:id-->FORMULIR<!--:-->"); ?></h5>
				<p><?php _e("<!--:en-->Find important documents and forms that support our services for you<!--:--><!--:id-->Cari dokumen dan formulir penting yang mendukung layanan kami untuk Anda<!--:-->"); ?>
				<ul class="pdf-list desktop-content two-list-content small-block-grid-2 medium-block-grid-2 large-block-grid-2 clearfix">
				<?php 
					$args = array("post_type" => "form","posts_per_page" =>6);
					query_posts( $args );
					if(have_posts()): while(have_posts()):the_post();
				?>
					<li><a href="<?php the_field('forms_upload_file');?>" class="clearfix block white shadow-grey-3" download><span class="icon"></span><strong class="w-80p"><?php the_title();?></strong></a></li>
					<?php endwhile;?>
				</ul>
				<?php endif;?>
				<?php wp_reset_query(); ?>
				<a href="<?php echo site_url('layanan-nasabah/formulir/'); ?>" class="button red small right"><?php _e("<!--:en-->See all document & forms<!--:--><!--:id-->Lihat semua dokumen & formulir<!--:-->"); ?></a>
			</div>
			<aside class="columns w-322 desktop-content">
				<?php get_template_part("widget/sidebar-pencarian-form");?>
			</aside>
		</section>

			<div class="bg-white p-lr-15 mobile-content p-top-20 p-bottom-20">
			<div class="widget m-bottom-10 w-100p">
				<?php get_template_part('widget/sidebar-pengajuan-klaim');?>
			</div>

			<div class="widget m-bottom-10 w-100p">
				<?php get_template_part('widget/sidebar-axa-akses-mobile');?>
			</div>

			<div class="widget m-bottom-10 w-100p">
				<?php get_template_part('widget/sidebar-pencarian-form');?>
			</div>
		</div>

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>