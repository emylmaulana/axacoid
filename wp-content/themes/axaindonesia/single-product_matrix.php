
<?php/** * Template Name: Karir: Agent */?>
<?php get_header();?>

<?php
	//$succ=$_GET=['succ'];
	//session_start();
	$cap = 'notEq';
	//session_start();
	$ranStr = md5(microtime());
	$ranStr = substr($ranStr, 0, 6);
	
	// $this->session->set_userdata('cap_code', $ranStr);
	$_SESSION['cap_code']=$ranStr;
	$newImage = imagecreatefromjpeg(get_bloginfo('template_url').'/images/cap_bg.jpg');
	$txtColor = imagecolorallocate($newImage, 0, 0, 0);
	imagestring($newImage, 5, 5, 5, $ranStr, $txtColor);
	//header("Content-type: image/jpeg");
	// imagejpeg($newImage, get_bloginfo('template_url').'/images/captcha.jpg');
	//imagejpeg($newImage, '/Applications/XAMPP/xamppfiles/htdocs/axaone/wp-content/themes/axaindonesia/images/captcha.jpg');
	imagejpeg($newImage, get_theme_root().'/axaindonesia/images/captcha.jpg');
	
?>

<div id="page-container">
	<div id="masthead" class="row relative p-all-0">
		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'matrix_large' ); ?>
		<?php endif; ?>
		<div id="page-head" class="relative m-top-150" style="background:#fcfdff url(<?php echo $image[0]; ?>) no-repeat top right">
			<div class="separate-large absolute"></div>
			<div class="box absolute set-1 large-5">
				<?php $category = wp_get_post_terms($post->ID, 'matrix_category', array("fields" => "names")); ?>


				<p><?php echo $category[1];?></p>
				<h1 class="f-45 lh-1"><?php the_title();?></h1>
				<!-- mathtag -->
				<?php $slug = $post->post_name; ?>
				<?php if($slug=='maestro-hospital-plan'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569887&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-hospital-save'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569889&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-axa-cancer-and-sav'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569891&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='extra-health-insurance'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569893&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='extra-cancer-insurance'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569895&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='international-exclusive'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569897&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='promedicare'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569900&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='smart-care-executive-2'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569902&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestro-elite-care'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569904&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-hospital-plus-life'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569906&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-medicare-group-health'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569909&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='group-international-exclusive'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569911&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='smartcare-executive'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569913&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestro-term'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=571621&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-life-save'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569917&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-care-protection'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569920&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='extra-life-insurance'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569922&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='smartcare-prime'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569924&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestrolink-plus'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569927&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestro-syariah'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569929&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestrolink-maxiadvantage'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569931&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-jiwa-berjangka-kumpulan'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569933&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-medicare-group-personal-accident'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569935&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-medicare-group-total-permanent-disability'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569938&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-citradinamis'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569940&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestroberimbang'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569942&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='citragold'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569944&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestrodollar'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569947&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-maestrosaham-3'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569949&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-maestro-obligasi-plus'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569951&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-citradinamis-2'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=571622&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestroberimbang-2'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569955&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='citragold-2'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569958&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='maestrodollar-2'):?>	
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569960&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='axa-maestroobligasi-plus-2'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569964&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='smart-home'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569966&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='smartdrive'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=571623&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='smarttraveller'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569970&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='international-student-protection'):?>	
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569974&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-semua-resiko-kontraktor'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569976&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='business-advantage'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569978&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='polis-semua-resiko'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569981&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='kebakaran'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569983&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='tanggung-jawab-hukum-kepada-publik'):?>	
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569986&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-uang'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569988&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-alat-berat-2'):?>
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569990&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-peralatan-elektronik-2'):?>	
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569992&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
				<?php elseif($slug=='asuransi-rangka-kapal-2'):?>	
					<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569995&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>

				<?php endif;?>
				<!-- end mathtag -->
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:description="Temukan solusi perlindungan yang tepat sesuai kebutuhan Anda di AXA Indonesia"  addthis:title="Temukan solusi perlindungan yang tepat sesuai kebutuhan Anda di AXA Indonesia">
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_print"></a>
				<a class="addthis_button_email"></a>
				</div>
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e88dff717990fa"></script>
				<!-- AddThis Button END -->
			</div>
		</div>
	</div><!--end masthead-->
	<div class="row p-all-0">
	<div id="page-box" class="sections white clearfix">
		<section id="body-1">
			<h3 class="uppercase fw-normal f-17"><?php _e("<!--:en-->Introduction<!--:--><!--:id-->Pengantar<!--:-->"); ?></h3>
			<h4 class="c-blue f-24"><?php the_field('sub_title');?></h4>
			<div class="column-2 f-16 c-greylight">
			<?php if( have_posts() ) : the_post(); ?>
				 <?php the_content();?>
			<?php endif;?>
			</div>
		</section>
	</div>

	<section id="manfaat" class="bg-bluedark sections clearfix">
		<h3 class="c-white fw-normal uppercase f-17"><?php _e("<!--:en-->Benefit<!--:--><!--:id-->Manfaat<!--:-->"); ?></h3>
		<h4 class="c-white m-bottom-45"><?php the_field('manfaat_sub_title');?></h4>
		<ul id="manfaat-slide">
			<?php $i = 1; while(has_sub_field('matrix_manfaat')): ?>
				<li class="text-center elements <?php if(($i % 2) == 0) echo 'dark'; ?>">
					<div class="box p-all-40">
						<img src="<?php the_sub_field("icon");?>" alt="manfaat">
						<p><strong><?php the_sub_field("title");?></strong></p>
						<?php the_sub_field("body");?>
					</div>
				</li>
			<?php $i++; endwhile;?>
		</ul>
	</section>
	<?php# if(get_field('matrix_syarat2')!=""):?>
	<section id="syarat" class="sections bg-bluelight2 relative o-hidden">
		<div class="transparent-separate absolute" style="left:0;top:0;"><img src="<?php bloginfo('template_url'); ?>/images/separate-transparent.png" alt="sparate-transparent"/></div>
		<div class="relative" style="z-index:1000;">
		<h3 class="fw-normal f-17 uppercase m-bottom-40">Syarat &amp; Ketentuan</h3>
		<ul id="list-syarat" class="small-block-grid-1 medium-block-grid-2 large-block-grid-2 clearfix">
			<?php while (has_sub_field('matrix_syarat2')):?>
				<?php if(get_sub_field('usia_masuk')!=""):?>
				<li class="usia-masuk">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Usia Masuk</strong></p>
								<p class="f-16"><?php the_sub_field('usia_masuk');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('masa_pertanggungan')!=""):?>
				<li class="masa_pertanggungan">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Masa Pertanggungan</strong></p>
								<p class="f-16"><?php the_sub_field('masa_pertanggungan');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('mata_uang')!=""):?>
				<li class="mata_uang">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Mata Uang</strong></p>
								<p class="f-16"><?php the_sub_field('mata_uang');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('minimum_premi')!=""):?>
				<li class="minimum_premi">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Minimum Premi</strong></p>
								<p class="f-16"><?php the_sub_field('minimum_premi');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('pengembalian_premi')!=""):?>
				<li class="pengembalian_premi">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Pengembalian Premi</strong></p>
								<p class="f-16"><?php the_sub_field('pengembalian_premi');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('cara_bayar')!=""):?>
				<li class="cara_bayar">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Cara Bayar</strong></p>
								<p class="f-16"><?php the_sub_field('cara_bayar');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('family_discount')!=""):?>
				<li class="family_discount">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Family Discount</strong></p>
								<p class="f-16"><?php the_sub_field('family_discount');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
				<?php if(get_sub_field('jalur_distribusi')!=""):?>
				<li class="jalur_distribusi">
					<div class="bg-white o-hidden radius-all-5">
						<span class="icon-125x100"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong>Jalur Distribusi</strong></p>
								<p class="f-16"><?php the_sub_field('jalur_distribusi');?></p>
							</div>
					</div>
				</li>
				<?php endif;?>
			<?php endwhile;?>
			<?php while (has_sub_field('matrix_syarat_aami')):?>
				<?php if(get_sub_field('kebijakan_investasi')!=""):?>
					<li class="kebijakan_investasi">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Investment Policy<!--:--><!--:id-->Kebijakan Investasi<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('kebijakan_investasi');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('tujuan_investasi')!=""):?>
					<li class="tujuan_investasi">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Tujuan Investasi<!--:--><!--:id-->Tujuan Investasi<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('tujuan_investasi');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('karakteristik_investasi')!=""):?>
					<li class="karakteristik_investasi">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Karakteristik Investasi<!--:--><!--:id-->Karakteristik Investasi<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('karakteristik_investasi');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('biaya_biaya')!=""):?>
					<li class="biaya_biaya">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Biaya-biaya<!--:--><!--:id-->Biaya-biaya<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('biaya_biaya');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('bank_kustodian')!=""):?>
					<li class="bank_kustodian">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Bank Kustodian<!--:--><!--:id-->Bank Kustodian<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('bank_kustodian');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('rekening_reksadana')!=""):?>
					<li class="rekening_reksadana">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Rekening Reksadana<!--:--><!--:id-->Rekening Reksadana<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('rekening_reksadana');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('minimum_investasi')!=""):?>
					<li class="minimum_investasi">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Minimum Investment<!--:--><!--:id-->Minimum Investasi<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('minimum_investasi');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('minimum_penjualan_kembali')!=""):?>
					<li class="minimum_penjualan_kembali">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Minimum Penjualan Kembali<!--:--><!--:id-->Minimum Penjualan Kembali<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('minimum_penjualan_kembali');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
				<?php if(get_sub_field('minimum_saldo_kepemilikan_unit')!=""):?>
					<li class="minimum_saldo_kepemilikan_unit">
						<div class="bg-white o-hidden radius-all-5">
							<span class="icon-125x100"></span>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Minimum Saldo Kepemilikan Unit<!--:--><!--:id-->Minimum Saldo Kepemilikan Unit<!--:-->"); ?></strong></p>
									<p class="f-16"><?php the_sub_field('minimum_saldo_kepemilikan_unit');?></p>
								</div>
						</div>
					</li>
				<?php endif;?>
			<?php endwhile;?>
			<?php while (has_sub_field('custom_syarat_&_ketentuan')): ?>
				<li class="custom-syarat">
					<div class="bg-white o-hidden radius-all-5">
					<?php   $image=get_field('icon');?>
						<span class="icon-125x100"><img src="<?php the_sub_field('icon');?>" alt="custom-syarat"></span>
							<div class="details">
								<p class="uppercase f-18 m-bottom-0"><strong><?php the_sub_field('title'); ?></strong></p>
								<p class="f-16"><?php the_sub_field('content');?></p>
							</div>
					</div>
				</li>
			<?php endwhile; ?>
	
		</ul>
		</div>
	</section>
	<?php# endif;?>
		<?php if(get_field('matrix_brochure2')!=""):?>
		<section id="brochure-download" class="sections bg-blue clearfix">
			<ul class="pdf-list small-block-grid-2 medium-block-grid-2 large-block-grid-2 clearfix">
				<?php while (has_sub_field('matrix_brochure2')):?>
					<li><a href="<?php the_sub_field('file');?>" class="bg-white block clearfix"><span class="icon"></span><strong><?php the_sub_field('title');?></strong> <i class="fa fa-download right"></i></a></li>
				<?php endwhile;?>
			</ul>
		</section>
		<?php endif;?>
	<section id="kontak" class="sections grey clearfix">
		<div id="floating" class="absolute top-0 right-55">
			<a href="https://indonesia.merimen.com/epolicy/index.cfm?fusebox=MICsec&fuseaction=act_ssologin&lf=EPLAXAID&GCOID=712001&USERID=AXANOLOGIN&SESSIONID=axa67$x&LANGID=2" class="button red beli">Beli Online</a>
			<?php if($category[1]=="Solusi Investasi"):?>
			<a href="<?php echo site_url('laporan-kinerja-bulanan'); ?>" class="button blue">Fund Fact Sheet</a>
			<?php endif;?>
			<a href="<?php echo site_url('bandingkan-produk'); ?>" class="button red">Bandingkan Produk</a>
			<?php if(get_field('matrix_brochure2')!=""):?>
				<?php# while (has_sub_field('matrix_brochure2')):?>
				<?php# if(get_sub_field('file')!=""):?>
					<a href="#" class="button blue brochure-menu">Download</a>
				<?php# endif; endwhile;?>
			<?php endif;?>
		</div>
		<h3 class="fw-normal f-17 uppercase m-bottom-0 m-top-45">Kontak</h3>
		
		<div class="large-4 columns p-left-0">
			<h4 class="c-blue">Kami akan menghubungi Anda</h4>
			<p class="f-16">Untuk mendapatkan informasi yang lebih lengkap tentang produk dan layanan AXA, isi formulir berikut dan kami akan menghubungi Anda</p>
		</div>
		<div class="large-6 columns contact-form">

			<!-- form -->
			<form action="<?=site_url()?>/axaone_form/kontak/submit_data" id="form" method="post" class="wpcf7-form" novalidate="novalidate">
				<?php 
					$terms = wp_get_post_terms($post->ID, 'matrix_entity');
					$entity = '';
					foreach($terms as $term){
						$entity = $term->name;
					}
				?>
				<div style="display: none;">
				<input type="hidden" name="_wpcf7" value="4">
				<input type="hidden" name="_wpcf7_version" value="3.6">
				<input type="hidden" name="_wpcf7_locale" value="en_US">
				<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p1330-o1">
				<input type="hidden" name="_wpnonce" value="21955172aa">
				<input type="hidden" name="product_matrix" value="<?php echo $entity;?>">
				<input type="hidden" name="nama_produk" value="<?php the_title();?>">
				<input type="hidden" name="banner_source" value="">
				<input type="hidden" name="utm_source" value="">
				<input type="hidden" name="utm_medium" value="">
				<input type="hidden" name="utm_term" value="">
				<input type="hidden" name="utm_content" value="">
				<input type="hidden" name="utm_campaign" value="">
				<input type="hidden" name="gclid" value="">
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap nama_lengkap">
						<input type="text" name="nama_lengkap" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Lengkap">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap no_tlp">
						<input type="tel" name="no_tlp" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nomor Telepon">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap email">
						<input type="email" name="email" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap tgl_lahir">
						<input type="text" name="tgl_lahir" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required datepicker " aria-required="true" aria-invalid="false" placeholder="Tanggal Lahir" id="dp1392957616851">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap propinsi">
						<select name="propinsi" class="required wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
							<option value="Propinsi">Propinsi</option>
							<option value="Aceh">Aceh</option>
							<option value="Sumatera Utara">Sumatera Utara</option>
							<option value="Sumatera Barat">Sumatera Barat</option>
							<option value="Riau Ibukotanya">Riau Ibukotanya</option>
							<option value="Kepulauan Riau">Kepulauan Riau</option>
							<option value="Jambi">Jambi</option>
							<option value="Sumatera Selatan">Sumatera Selatan</option>
							<option value="Bangka Belitung">Bangka Belitung</option>
							<option value="Bengkulu">Bengkulu</option>
							<option value="Lampun">Lampun</option>
							<option value="DKI Jakarta">DKI Jakarta</option>
							<option value="Jawa Barat">Jawa Barat</option>
							<option value="Banten">Banten</option>
							<option value="Jawa Tengah">Jawa Tengah</option>
							<option value="Daerah Istimewa Yogyakarta">Daerah Istimewa Yogyakarta</option>
							<option value="Jawa Timur">Jawa Timur</option>
							<option value="Bali">Bali</option>
							<option value="Nusa">Nusa</option>
							<option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
							<option value="Kalimantan Utara">Kalimantan Utara</option>
							<option value="Kalimantan Barat">Kalimantan Barat</option>
							<option value="Kalimantan Tengah">Kalimantan Tengah</option>
							<option value="Kalimantan Selatan">Kalimantan Selatan</option>
							<option value="Kalimantan Timur">Kalimantan Timur</option>
							<option value="Sulawesi Utara">Sulawesi Utara</option>
							<option value="Sulawesi Barat">Sulawesi Barat</option>
							<option value="Sulawesi Tengah">Sulawesi Tengah</option>
							<option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
							<option value="Sulawesi Selatan">Sulawesi Selatan</option>
							<option value="Gorontalo">Gorontalo</option>
							<option value="Maluku">Maluku</option>
							<option value="Maluku Utara">Maluku Utara</option>
							<option value="Papua Barat">Papua Barat</option>
							<option value="Papua">Papua</option>
						</select>
					</span>
				</div>
				<div class="fieldset">
				 <span class="required wpcf7-form-control-wrap area">
				 	<input type="text" name="kota" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Kota / Area">
				 </span>
				</div>
				
				<div class="fieldset">
					<input type="submit" value="Kirim" class="wpcf7-form-control wpcf7-submit button small red">
					
				</div>
				
				<div class="wpcf7-response-output wpcf7-display-none"></div>
				<p class="cap_status" style="display:none;"></p>
			</form>
			<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569865&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>

			<!-- form -->

		</div>
	</section>
	<?php get_template_part("widget/breadcrumbs");?>
	</div>
	<?php get_template_part("widget/hargaunit");?>
</div>
<script type="text/javascript">
			// var capch = '<?php echo ($_SESSION['cap_code']); ?>' ;
			// if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// 	$('#img-captcha').hide();
			// 	$('#captcha').attr('type', 'hidden');
			// 	$('#captcha').val(capch);
			// }
			$(document).ready(function(){
		 		//captcha check
				$("#form").submit(function(){
				
				    if($("#form").valid()) {
				    	console.log('1234');
	                    if(capch == $('#captcha').val()){
	                    	console.log('test');
	                    	return true;

	                    }else{
	                        $('.cap_status').html("Captcha yang Anda masukkan salah!").addClass('cap_status_error').fadeIn('slow');
	                    }
				    }else{
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    }

				    return false;
				 });
			});
		</script>

<?php get_footer();?>