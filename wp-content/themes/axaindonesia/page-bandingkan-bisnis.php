<?php/** * Template Name: Bandingkan:Bisnis */?>
<?php get_header();?>

<?php
	//$succ=$_GET=['succ'];
	//session_start();
	$cap = 'notEq';
	//session_start();
	$ranStr = md5(microtime());
	$ranStr = substr($ranStr, 0, 6);
	
	// $this->session->set_userdata('cap_code', $ranStr);
	$_SESSION['cap_code']=$ranStr;
	$newImage = imagecreatefromjpeg(get_bloginfo('template_url').'/images/cap_bg.jpg');
	$txtColor = imagecolorallocate($newImage, 0, 0, 0);
	imagestring($newImage, 5, 5, 5, $ranStr, $txtColor);
	//header("Content-type: image/jpeg");
	// imagejpeg($newImage, get_bloginfo('template_url').'/images/captcha.jpg');
	//imagejpeg($newImage, '/Applications/XAMPP/xamppfiles/htdocs/axaone/wp-content/themes/axaindonesia/images/captcha.jpg');
	imagejpeg($newImage, get_theme_root().'/axaindonesia/images/captcha.jpg');
	
?>
<div id="page-container" style="background-image:url(<?php bloginfo('template_url');?>/images/head-bandingkan.jpg);">
	<div id="masthead" class="row relative">
		<div class="content large-6">
			<h1><?php _e("<!--:en-->Compare Products<!--:--><!--:id-->Bandingkan Produk<!--:-->"); ?></h1>
			<h2 class="c-default">Sandingkan produk-produk dari solusi AXA  dan cari produk yang terbaik untuk Anda</h2>
			
			<ul id="separate" class="personal relative" style="top:0;">
				<?php $slug = basename(get_permalink());?>
				<li class="selected"><a href="<?php echo site_url('bandingkan-produk/solusi-kesehatan');?>">Personal</a></li>
				<li><a href="<?php echo site_url('bandingkan-produk/bisnis/') . $slug;?>">Bisnis</a></li>
			</ul>	
		</div><!--end large 6-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div class="row">  
		<section class="sections bg-white clearfix">
				<div class="clearfix">
					<h3 class="f-24"><?=$_GET['q']?>Temukan produk perlindungan AXA yang terbaik untuk Anda</h3>

						<?php $bandingkanmenu = array(
				                'theme_location'  => '',
				                'container'       => '',
				                'menu'            => 'Bandingkan Bisnis',
				                'echo'            => true,
				                'fallback_cb'     => 'wp_page_menu',
				                'items_wrap'      => '<ul id="bandingkan-menu" class="%2$s ssmall-block-grid-1 medium-block-grid-2 large-block-grid-4 m-top-20 m-bottom-20">%3$s</ul>',
				                'depth'           => 0,
				                'link_before' => '<div class="block p-all-20 bg-greylight3 radius-all-5"><span class="icon"></span><strong class="uppercase">', 'link_after' => '</strong></div>'
				            );
				        wp_nav_menu( $bandingkanmenu );?>

					<ul id="dropdown" class="clearfix small-block-grid-1 medium-block-grid-2 large-block-grid-4">
						<?php for($i = 1; $i < 5; $i++){ ?>
							
							<li class="<?=$i == 1 ? '' : 'disable'?> relative">
								<select class="matrix-cat" name="matrixcat-<?=$i?>" id="matrixcat-<?=$i?>">
									<option value="0">Pilih produk</option>
								<?php 
					     			$args = array("post_type" => "product_matrix","posts_per_page" =>-1, "matrix_category" => $slug."-bisnis");
									query_posts( $args );
									if(have_posts()): while(have_posts()):the_post();
								?>
										<option value="<?=$post->ID?>"><?php the_title(); ?></option>
								<?php
									endwhile; endif; wp_reset_query();
								?>
								</select>
							</li>
						<?php } ?>
					</ul>
					<input type="hidden" id="solutionType" value="<?php echo $post->post_name; ?>">
					<div id="matrix-result">
						
					</div>
				</div><!--end content box-->
		</section>
		<section class="sections bg-greylight2">
			<ul class="clearfix small-block-grid-1 medium-block-grid-2 large-block-grid-4">
				<li id="result-matrix-1">
					<div class="postThumbnail dashed-2 radius-all-5 h-198 m-bottom-10 bg-white">
						<strong class="f-16 text-center block m-top-65">Pilih produk perlindungan AXA untuk dibandingkan</strong>
					</div>
					<div class="manfaat dashed-2 h-322 radius-all-5 m-bottom-10 bg-white"></div>

					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php if(!is_page('solusi-investasi')): ?>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php endif; ?>

				</li>

				<li id="result-matrix-2">
					<div class="postThumbnail dashed-2 radius-all-5 h-198 m-bottom-10 bg-white">
						<strong class="f-16 text-center block m-top-65">Pilih produk perlindungan AXA untuk dibandingkan</strong>
					</div>
					<div class="manfaat dashed-2 h-322 radius-all-5 m-bottom-10 bg-white"></div>

					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php if(!is_page('solusi-investasi')): ?>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php endif; ?>

				</li>

				<li id="result-matrix-3">
					<div class="postThumbnail dashed-2 radius-all-5 h-198 m-bottom-10 bg-white">
						<strong class="f-16 text-center block m-top-65">Pilih produk perlindungan AXA untuk dibandingkan</strong>
					</div>
					<div class="manfaat dashed-2 h-322 radius-all-5 m-bottom-10 bg-white"></div>

					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php if(!is_page('solusi-investasi')): ?>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php endif; ?>

				</li>

				<li id="result-matrix-4">
					<div class="postThumbnail dashed-2 radius-all-5 h-198 m-bottom-10 bg-white">
						<strong class="f-16 text-center block m-top-65">Pilih produk perlindungan AXA untuk dibandingkan</strong>
					</div>
					<div class="manfaat dashed-2 h-322 radius-all-5 m-bottom-10 bg-white"></div>

					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php if(!is_page('solusi-investasi')): ?>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<div class="syarat dashed-2 h-55 radius-all-5 m-bottom-10 bg-white"></div>
					<?php endif; ?>

				</li>
			</ul>
		</section>
		

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<script type="text/javascript">
			// var capch = '<?php echo ($_SESSION['cap_code']); ?>' ;
			// if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// 	$('#img-captcha').hide();
			// 	$('#captcha').attr('type', 'hidden');
			// 	$('#captcha').val(capch);
			// }
			$(document).ready(function(){
		 		//captcha check
				$("#form").submit(function(){
				
				    if($("#form").valid()) {
				    	console.log('1234');
	                    if(capch == $('#captcha').val()){
	                    	console.log('test');
	                    	return true;

	                    }else{
	                        $('.cap_status').html("Captcha yang Anda masukkan salah!").addClass('cap_status_error').fadeIn('slow');
	                    }
				    }else{
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    }

				    return false;
				 });
			});
		</script>

<?php get_footer();?>