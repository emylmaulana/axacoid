<?php
/*
Template Name: All Direktori JSON
Dipakai saat page direktori, direktori-search, direktori-list, direktori-bengkel load untuk menampilkan data marker di peta untuk masing-masing halaman
*/
?>
<?php 
	global $wpdb,$keyword;
	//get parameteer url berasal dari page direktori-search
	$keyword = ($_GET['q']) ? $_GET['q'] : '';
	$type = ($_GET['t'] && $_GET['t'] != 'all') ? array($_GET['t']) : array("rumah_sakit","bengkel");//jika search kosong akan mengambil 2 tipe
	$t=$_GET['t'];// tambahan variable jika hanya satu tipe yg masuk
	$tax = ($_GET['tax']) ? $_GET['tax'] : ''; //get entity
	$tax2 = ($_GET['wilayah']) ? $_GET['wilayah'] : '';//get wilayah
	$providers = ($_GET['providers']) ? $_GET['providers'] : '-1'; //get provider berasal dari pemilihan entity dari page direktori-search rumah sakit
	$nama_produk = ($_GET['nama_produk']) ? $_GET['nama_produk'] : '-1'; //get nama produk didapatkan jika setelah select provider kemudian nama produknya di page direktori-search
	$x =($_GET['x']) ? $_GET['x'] :''; // latitude yg didapatkan dari setiap halaman direktori
	$y =($_GET['y']) ? $_GET['y'] :''; // longitude yg didapatkan dari setiap halaman direktori
	$args = array(); 
	$new_array_post_id = array();
?>
<?php 
header('Content-Type: application/json');
if($tax2 != '' && $keyword == ''){
	$args = array( 'post_type' => $type, 'orderby' => 'title', 'order' => ASC, 'nopaging' => true, 'direktori_wilayah' => $tax2 );
}elseif($tax2 != '' && $keyword != ''){ 
	$args = array( 'post_type' => $type, 'orderby' => 'title', 'order' => ASC, 'nopaging' => true, 's' => $keyword, 'direktori_wilayah' => $tax2 );
}elseif($tax != '' && $keyword == ''){
	$args = array( 'post_type' => $type, 'orderby' => 'title', 'order' => ASC, 'nopaging' => true, 'solusi' => $tax );
}elseif($tax != '' && $keyword != ''){
	$args = array( 'post_type' => $type, 'orderby' => 'title', 'order' => ASC, 'nopaging' => true, 's' => $keyword, 'solusi' => $tax );
}elseif(isset($keyword) && $keyword != ''){
	$args = array( 'post_type' => $type, 'orderby' => 'title', 'order' => ASC, 'nopaging' => true, 's' => $keyword );
}elseif($t=='bengkel' && $keyword!=''){
	$args = array( 'post_type' => 'bengkel','orderby' => 'title', 'order' => ASC, 'nopaging' => true, 's' => $keyword );
}elseif($t=='bengkel' && $keyword==''){
	$args = array( 'post_type' => 'bengkel','orderby' => 'title', 'order' => ASC, 'nopaging' => true );
}elseif($t=='rumah_sakit' && $keyword!=''){
	$args = array( 'post_type' => 'rumah_sakit','orderby' => 'title', 'order' => ASC, 'nopaging' => true, 's' => $keyword );
}elseif($t=='rumah_sakit' && $keyword==''){
	$args = array( 'post_type' =>'rumah_sakit','orderby' => 'title', 'order' => ASC, 'nopaging' => true );
}else{
	$args = array( 'post_type' => array('rumah_sakit','bengkel'),'orderby' => 'title', 'order' => ASC, 'nopaging' => true );
}
if ($x !='' && $y !='') { // jika x dan y itu ada maka query akan menghitung jarak terdekat
	$post_id_and_distance=$wpdb->get_results("SELECT post_id, post_title, ( 6371 * ACOS( COS( RADIANS($x))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS($y))+SIN(RADIANS($x))*SIN(RADIANS(latitude)))) AS distance
																	FROM  `directory_coordinates` 
																	ORDER BY  `distance` ASC");	
	$post_id_and_distance = array_splice($post_id_and_distance, 0);	
	
	foreach($post_id_and_distance as $row => $value)
	{
		$new_array_post_id[] = intval($value->post_id);
		
	}

	$args['post__in'] = $new_array_post_id; //penyesuaian query args dengan variable $new_array_post_id
	$args['orderby'] = 'post__in'; 
	$args['paged']=$paged;
	$args['order']=ASC;
}
if($providers != '-1') {
	$args["tax_query"] = array(
								array(
									'taxonomy' => 'providers',
									'terms' => $providers
								)
							);
}
if($nama_produk != '-1') {
	$args["meta_query"] = array(
								array(
									'key' => 'nama_produk', // name of custom field
									'value' => '"'.$nama_produk.'"', // matches exaclty "123", not just 123. This prevents a match for "1234"
									'compare' => 'LIKE'
								)
							);
}

add_filter('posts_join', 'directory_search_join' );//get function on function.php
add_filter('posts_where', 'directory_search_where' );//get function on function.php
$query = new WP_Query($args);
//echo $myquery->request;
remove_filter('posts_join', 'directory_search_join' );
remove_filter('posts_where', 'directory_search_where' );

	if ( $query->have_posts() ) : 
		$output = array();
		while ( $query->have_posts() ) : $query->the_post(); 
			
				$location = get_field('rs_map');
				$coord = explode(",", $location['coordinates']);
				$data = '<div class="infowindow-data">
							<strong class="c-blue f-16 block m-bottom-10">'.get_the_title().'</strong>
							<div class="icon left '.get_post_type( $post_id ).'"><span></span></div>
							<div class="details left m-left-10 f-12">
								<div class="addressSmall">'.get_field('rs_alamat').'</div>
								<div class="phoneSmall">Phone: '.get_field('rs_telepon').'</div>
								<div class="faxSmall">Fax: '.get_field('rs_fax').'</div>
							</div>
						</div>';
				if ($post->post_type == "rumah_sakit") {
					$wpurl= site_url();
				array_push($output, array('latLng'=>$coord,'data'=>$data, 'options'=>array('iconpng'=>$wpurl.'/wp-content/themes/axaindonesia/images/marker-rs.png')));
				} else {
				array_push($output, array('latLng'=>$coord,'data'=>$data, 'options'=>array('iconpng'=>$wpurl.'/wp-content/themes/axaindonesia/images/marker-bengkel.png')));
				}


		endwhile;
		
	endif;  
	echo json_encode($output);

 ?>