<div id="region-2">
    <div class="row">
        <div class="large-4 columns">
                <?php get_template_part("widget/karir");?>
        </div>
        <div class="large-4 columns">
            <?php get_template_part("widget/footer-direktori");?>
        </div>
        <div class="large-4 columns">
                <?php get_template_part("widget/address");?>
        </div>
    </div>
</div><!--end region 2-->
    
<footer id="footer" class="show-for-large-only">
	<div class="row">
        <div class="large-6  columns clearfix">
        	<div class="footer-menu">
        	<h5><?php _e("<!--:en-->Quick Menu<!--:--><!--:id-->Link Cepat<!--:-->"); ?></h5>
              <?php $quickmenu = array(
                        'theme_location'  => '',
                        'container'       => '',
                        'menu'            => 'Link Cepat',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                        'depth'           => 0,
                    );
                wp_nav_menu( $quickmenu );?>
            </div>
            <div class="footer-menu">
        	<h5>AXA Indonesia</h5>
                <?php $axamenu = array(
                        'theme_location'  => '',
                        'container'       => '',
                        'menu'            => 'AXA Indonesia',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                        'depth'           => 0,
                    );
            wp_nav_menu( $axamenu );?>
              </div>
        </div>
        <div class="large-3 columns">
            <div class="main-sites right m-top-15">
                <a href="https://axa.co.id/" target="_blank"><img src="https://axa.co.id/wp-content/themes/axaindonesia/images/icon-axaindonesia.png" alt="icon-axa"></a>
                <a href="http://www.axa.com" target="_blank"><img src="https://axa.co.id/wp-content/themes/axaindonesia/images/icon-axacom.png" alt="icon-axa"></a>
            </div>
        </div>
	</div>
</footer>
    <div id="customercare-fixed"><?php get_template_part('widget/customer-care');?></div>
    <footer id="footer-mobile" class="hide-for-large-only">
        <div class="row">
          <a href="https://portal.axa.co.id/" target="_blank" style="background: #00235E;color: #ffffff !important;border-radius: 4px 5px; padding: 1px 9px; font-weight: bold; width: 117px;">Agency Portal</a>
            <?php $quickmenu = array(
                'theme_location'  => '',
                'container'       => '',
                'menu'            => 'Link Cepat',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                'depth'           => 0,
            );
        wp_nav_menu( $quickmenu );?>


        </div>
    </footer><!--end footer mobile-->
    <a class="exit-off-canvas"></a>
  </div>
</div><!--end offcanvas-->
   <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=656866527686942";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <?php wp_footer();?>

    <!--[if lt IE 9]>
       <script type="text/javascript">
        jQuery(document).ready(function() {
        jQuery(".popup_box").fancybox({ 
            margin      : 0,
            maxWidth    : 300,
            maxHeight   : 315,
            fitToView   : true,
            scrolling   : "no",
            padding: 0,
            }).trigger('click');
        });
        </script>

        <div class="popup_box" style="display:none;text-align:center;background:#EEE;color:#153389;">
            <h3 style="background:#fff;font-size:1.250em;line-height:1.2;padding:20px; 0;margin:0;color:#153389;">Selamat Datang di Website AXA Indonesia</h3>
            <p style="margin-bottom:0; padding:20px;">Mohon untuk melakukan upgrade<br>
                pada web browser Anda<br>
                untuk tampilan optimal.</p>
        </div> 
    <![endif]-->
  </body>
</html>