<?php/** * Template Name: Layanan Nasabah: Customer Care */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570023&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1><?php _e("<!--:en-->Customer Service<!--:--><!--:id-->Layanan Nasabah<!--:-->"); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>

		<section id="premi" class="clearfix sections grey">
			<h3 class="m-bottom-45">Customer Care</h3>
			<div id="page-half">
			<div class="large-8 columns clearfix">
				<div class="large-6 columns h-300 bg-white clearfix w-48p relative">
					<div class="box  p-all-15 clearfix">
						<h3 class="c-blue f-16 uppercase">CUSTOMER CARE CENTRE</h3>
						<p><?php _e("<!--:en-->You can apply for aid assistance, as well as questions regarding the product and its benefits, on the policy, premium payments, claims, policy changes, and others.<!--:-->
						<!--:id-->Anda dapat mengajukan bantuan asistensi, serta pertanyaan-pertanyaan mengenai produk dan manfaatnya, tentang polis, pembayaran premi, klaim, perubahan polis, dan lain-lainnya.<!--:-->"); ?> </p>
						<a href="<?php echo site_url('layanan-nasabah/customer-care/customer-care-centre/');?>" class="right button red absolute bottom-10 right-10"><?php _e("<!--:en-->Contact<!--:--><!--:id-->Hubungi<!--:-->"); ?></a>
					</div>
				</div>

				<div id="istilah-asuransi" class="large-6 columns h-300 bg-white clearfix  w-48p relative">
					<div class="box  p-all-15 clearfix">
						<h3 class="c-blue f-16 uppercase">PENGAJUAN KELUHAN</h3>
						<p><?php _e("<!--:en-->AXA Indonesia is committed to providing the best service for customers. We appreciate your suggestions and complaints so that we can always improve and enhance our services for the satisfaction and comfort of customers.<!--:-->
									<!--:id-->AXA Indonesia berkomitmen untuk memberikan layanan terbaik bagi pelanggan. Kami menghargai saran maupun keluhan Anda agar kami dapat selalu meningkatkan dan menyempurnakan layanan kami demi kepuasan dan kenyamanan nasabah.<!--:-->"); ?></p>
						<a href="<?php echo site_url('layanan-nasabah/customer-care/pengajuan-keluhan/');?>" class="right button red absolute bottom-10 right-10"><?php _e("<!--:en-->Complaint Procedure<!--:--><!--:id-->Prosedur Pengajuan Keluhan<!--:-->"); ?></a>
					</div>
				</div>
			</div><!--end large 8-->
			<aside class="columns w-322 h-300 o-hidden desktop-content">
				<?php get_template_part("widget/sidebar-axa-akses");?>
			</aside>
			
			<div class="mobile-content">
				<?php get_template_part('widget/sidebar-axa-akses-mobile');?>
			</div>
		</div>
		</section>


		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>