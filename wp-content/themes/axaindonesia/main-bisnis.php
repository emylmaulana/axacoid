<?php/** * Template Name: Bisnis */?>
<?php get_header();?>
	<div id="mainSlider">
		<div class="row box">
			<div class="large-11">
				<ul id="separate" class="bisnis">
					<li class="selected"><a href="<?php echo site_url("home");?>">Personal</a></li>
					<li><a href="<?php echo site_url("bisnis");?>">Bisnis</a></li>
				</ul>	
				<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
			</div>
		</div>
		<ul id="slideContent" class="owl-carousel">
			<?php 
				$args = array("post_type" => "slide_bisnis","posts_per_page" =>5);
				query_posts( $args );
				if(have_posts()): while(have_posts()):the_post();
			?>
			<li>
				<div class="slideBG" style="background-image:url(<?php the_field('slider_image');?>);"><a href="<?php the_field("slide_url");?>" class="slide-link"></a>
					<?php if(get_field('slide_button_text')):?>
					<div class="row">
						<div class="caption">
							<h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3>
							<a href="<?php the_field("slide_url");?>" class="buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
				<?php endif;?>
				</div>
			</li>
			<?php endwhile;?>
		</ul>
		<?php endif;?>
	</div>

	<div class="row solusiContainer">
		<div id="solusiWidget" class="bisnis large-11 clearfix">
			<div id="title"  class="show-for-large-only">
				<h3><?php _e("<!--:en-->Our Solution<!--:--><!--:id-->Solusi Kami<!--:-->"); ?></h3>
			</div>	
			<div class="box clearfix">
				<div id="intro"  class="show-for-large-only">
					<p><?php _e("<!--:en-->Ranging from micro businesses, SMEs up to large corporations, we provide the right insurance solutions to manage risks effectively.<!--:--><!--:id-->Mulai dari usaha mikro, UKM sampai dengan perusahaan besar, kami memberikan solusi asuransi untuk bisnis yang tepat untuk mengelola risiko-risiko secara efektif.<!--:-->"); ?></p>
				</div>
				<ul class="clearfix">
					<?php while(has_sub_field('bisnis_page', 'options')): ?>
					<li class="kesehatan"><a href="<?php echo site_url('bsolusi-kesehatan');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->Healthy<!--:--><!--:id-->Kesehatan<!--:-->"); ?></h4> <div class="desc" style="display:none;"><p><?php the_sub_field('kesehatan'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<li class="proteksi"><a href="<?php echo site_url('bisnis/solusi-proteksi');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->Protection<!--:--><!--:id-->Proteksi<!--:-->"); ?></h4> <div class="desc" style="display:none;"><p><?php the_sub_field('proteksi'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<li class="investasi"><a href="<?php echo site_url('bisnis/solusi-investasi');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->Investation<!--:--><!--:id-->Investasi<!--:-->"); ?></h4><div class="desc" style="display:none;"><p><?php the_sub_field('investasi'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<li class="general"><a href="<?php echo site_url('bisnis/solusi-umum');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->General<!--:--><!--:id-->Umum<!--:-->"); ?></h4> <div class="desc" style="display:none;"><p><?php the_sub_field('kesehatan'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<?php endwhile;?>
				</ul>
			</div>
		</div>
	</div>

	<?php get_template_part("widget/hargaunit");?>


	<div id="layanan">
		<div class="row">
			<div class="large-11 column centered">
				<h3><?php _e("<!--:en-->SERVICE IN YOUR HAND<!--:--><!--:id-->LAYANAN DI TANGAN ANDA<!--:-->"); ?></h3>
				<ul class="clearfix">
					<li class="large-4 columns premi">
						<div class="icon"></div>
						<div class="details">
							<h4><?php _e("<!--:en-->PAYMENT OF PREMIUM<!--:--><!--:id-->PEMBAYARAN PREMI<!--:-->"); ?></h4>
							<p class="show-for-large-only"><?php _e("<!--:en-->Find a wide selection of easy solutions in advanced premium payment service for all customers<!--:--><!--:id-->Temukan berbagai pilihan solusi mudah dalam pelayanan pembayaran premi asuransi lanjutan untuk semua nasabah<!--:-->"); ?></p>
								<a href="<?php echo site_url('layanan-nasabah/pembayaran-premi/');?>" class="button red"><?php _e("<!--:en-->Payment Options<!--:--><!--:id-->Pilihan Pembayaran<!--:-->"); ?></a>
						</div>
					</li>
					<li class="large-4 columns klaim">
						<div class="icon"></div>
						<div class="details">
							<h4><?php _e("<!--:en-->CLAIM<!--:--><!--:id-->PENGAJUAN KLAIM<!--:-->"); ?></h4>
							<p class="show-for-large-only"><?php _e("<!--:en-->Get the documents required for filing a claim and we will immediately give you the support you need<!--:--><!--:id-->Dapatkan dokumen-dokumen yang diperlukan untuk mengajukan klaim asuransi dan kami akan segera memberikan dukungan yang Anda butuhkan<!--:-->"); ?></p>
								<a href="<?php echo site_url('layanan-nasabah/pengajuan-klaim/');?>" class="button red"><?php _e("<!--:en-->Claim Center<!--:--><!--:id-->Pusat Klaim<!--:-->"); ?></a>
						</div>
					</li>
					<li class="large-4 columns akses">
						<div class="icon"></div>
						<div class="details">
							<h4><?php _e("<!--:en-->SEARCH FORM <!--:--><!--:id-->PENCARIAN FORMULIR<!--:-->"); ?></h4>
							<p class="show-for-large-only"><?php _e("<!--:en-->Find important documents and forms you need to enable us serve you<!--:--><!--:id-->Temukan dokumen dan formulir penting yang Anda butuhkan untuk memudahkan kami melayani Anda<!--:-->"); ?></p>
								<a href="<?php echo site_url('layanan-nasabah/formulir');?>" class="button red">Cari Formulir</a>
						</div>
					</li>
				</ul>
			</div>
		</div><!--end row-->
	</div><!--end layanan-->

	<div id="region-1">
		<div class="row">
			<div class="large-11 large-centered columns show-for-large-only">
				<div class="large-4 columns">
					<?php get_template_part("widget/footer-banner-left");?>
				</div>
				<div class="large-4 columns">
					<?php get_template_part("widget/bandingkan");?>
				</div>
				<div class="large-4 columns">
					<?php get_template_part("widget/berita-terbaru");?>
				</div>
			</div>
		</div><!--end row-->
	</div><!--end region 1-->

<?php get_footer();?>