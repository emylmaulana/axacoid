<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php bloginfo('name'); ?><?php wp_title('|',true,''); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Asuransi AXA Indonesia dengan layanan lengkap, layanan terbaik & sebagai perusahaan asuransi yang  memiliki jaringan terluas di seluruh dunia." name="description">
        <meta name="keywords" content="AXA Indonesia, AXA Financial Indonesia, AXA Life Indonesia, AXA General Insurance, AXA Asset Management Indonesia, AXA, insurance">
         
        <!-- CSS Styles  -->
        <link href="<?php bloginfo('template_url');?>/css/normalize.css" rel="stylesheet">
        <link href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" rel="stylesheet">

        <link href="<?php bloginfo('template_url');?>/css/mobile.css" rel="stylesheet">
        
        <!-- Favicon and touch icons  -->
        <link href="<?php bloginfo('template_url');?>/favicon.png" rel="shortcut icon">
         
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
      	<script src="<?php bloginfo('template_url');?>/js/html5shiv.js"></script>
    	<![endif]-->

        <?php wp_head();?>
        <script type="text/javascript">
            var bloginfo = '<?php bloginfo('template_url'); ?>';
            function bodyHeight(){
                height = $(window).height();
                $('body').css("height",height+"px");
                 // console.log (height);
            }

              $(window).resize(bodyHeight);
              $(window).load(bodyHeight);
             

        </script>
    </head>
    <body class="separationpage"> 
        <div style="display:none">
          <h1>AXA Indonesia</h1>
          <h2>Redefining Insurance</h2>
        </div>
        <a href="<?php echo site_url('home/');?>" title="axa" class="axa-left"></a>
        <a href="https://www.axa-mandiri.co.id" title="axa mandiri" class="axa-right"></a>
        <!-- Home Page - Affiperf -  -->
        <img src='https://pixel.mathtag.com/event/img?mt_id=452935&mt_adid=100544&v1=&v2=&v3=&s1=&s2=&s3=' width='1' height='1' alt="mathtag"/>
        <div id="wrapper">
            <div id="panel">
                <div class="box p-all-30">
                    <div class="left">
                        <a href="<?php echo site_url('home/');?>"><img src="<?php bloginfo('template_url');?>/images/logo-axa.png" alt="logo-axa"></a>
                        <p class="c-blue m-top-40"><strong>AXA</strong> beroperasi dengan fokus pada asuransi jiwa, asuransi umum dan manajemen aset melalui beragam jalur distribusi.</p>

                        <a href="<?php echo site_url('home/');?>" class="btn f-18 c-white block block absolute bottom-15 left-25 block" style="padding:10px 60px">AXA</a>
                    </div>
                    <div class="right">
                        <div class="text-right">
                             <a href="https://www.axa-mandiri.co.id"><img src="<?php bloginfo('template_url');?>/images/logo-axa-mandiri.png" alt="axa-mandiri"></a>
                         </div>  
                            <p class="c-blue m-top-20"><strong>AXA Mandiri</strong> berkomitmen untuk memberikan solusi perlindungan terbaik di setiap tahap kehidupan nasabah. </p>

                            <a href="https://www.axa-mandiri.co.id" class="btn f-18 c-white block absolute bottom-15 right-20" style="padding:10px 60px">AXA MANDIRI</a>
                    </div>
                 </div>
                <div class="bottomSection" style="display:none">
                    <a class="btn left"  href="<?php echo site_url('home/');?>"><span class="fa fa-angle-double-left"></span> &nbsp; &nbsp; AXA</a>
                    <a class="btn right" href="https://www.axa-mandiri.co.id">AXA MANDIRI &nbsp; &nbsp;  <span class="fa fa-angle-double-right"></span></a>
                </div>
            </div>
         </div>
          <div id="lead-banner" class="desktop-content" style="position: fixed;margin:auto;width:1180px;bottom:0;left:0;right:0;display:none;z-index:9999999999;">
            <a href="http://guaranteedfund.axa.co.id"><img alt="banner-GF"src="<?php bloginfo('template_url');?>/images/Banner-GF-1180px.png"/ style="width:100%;"></a>
         </div>

        <div id="lead-banner-mobile" class="mobile-content" style="position: fixed;margin:auto;width:100%;bottom:0;left:0;right:0;z-index:9999999999;">
            <a href="http://guaranteedfund.axa.co.id">
                <div style="width:100%;height:67px;position:fixed;bottom:0;background:url(<?php bloginfo('template_url');?>/images/Banner-GF-mobile.png) no-repeat center top"></div>
            </a>
            
         </div>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33864921-7', 'axa.co.id');
          ga('send', 'pageview');

          $(window).load(function() {
                //leadbanner animation
                $("#lead-banner").hide().css("width","690px").delay(1000).slideDown('1000').delay(200).animate({width:970},400);
            });
        </script>


    </body>
</html>