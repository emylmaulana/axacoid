<?php/** * Template Name: Layanan Nasabah: Pembayaran premi */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570011&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1>Layanan Nasabah</h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>
		<section id="page-half" class="white sections clearfix">
			<div class="large-8 columns p-all-0">
				 <?php if( have_posts() ) : the_post(); ?>
				 <?php the_content();?>
				 <?php endif;?>
			
				 <dl class="accordion" data-accordion="">
				 <p><strong>Lihat cara pembayaran premi lanjutan</strong></p>
				  <dd>
				    <a href="#afi">Produk AXA Financial Indonesia</a>
				    <div id="afi" class="content active">
				    	<div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
							<article><?php the_field('premi_produk_axa_financial_indonesia');?></article>
				    </div>
				  </dd>
				  <dd>
				    <a href="#ali">Produk AXA Life Indonesia</a>
				    <div id="ali" class="content">
				    	<div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
							<article><?php the_field('premi_produk_axa_life_indonesia');?></article>
				    </div>
				  </dd>
				  <dd>
				    <a href="#agi">Produk AXA General Insurance Indonesia</a>
				    <div id="agi" class="content">
				    	<div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
							<article><?php the_field('premi_produk_axa_general_insurance_indonesia');?></article>
				    </div>
				  </dd>
<!-- 				  <dd>
				    <a href="#ami">Produk AXA Asset Management Indonesia</a>
				    <div id="ami" class="content">
				    	<div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
							<article><?php the_field('premi_produk_axa_asset_management_indonesia');?></article>
				    </div>
				  </dd> -->
				</dl>


			</div>
			<aside class="columns widget w-322"> 
				<div class="m-bottom-25">
					<?php get_template_part("widget/sidebar-pengajuan-klaim");?>
				</div>
				<div class="m-bottom-25">
					<?php get_template_part("widget/sidebar-pencarian-form");?>
				</div>
			</aside>

		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>