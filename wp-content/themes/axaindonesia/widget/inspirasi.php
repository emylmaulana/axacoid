<div id="inspirasi-section" class="expanded">
<div class="row">
    <div id="intro" class="text-center large-6 columns large-centered">
        <h3><span></span>Inspirasi</h3>
        <p>Temukan dunia inspirasi AXA, menghadirkan berbagai artikel mengenai topik seputar kesehatan, gaya hidup, pendidikan dan topik seru lainnya.</p>
    </div>
<?php // Get RSS Feed(s)
include_once( ABSPATH . WPINC . '/feed.php' );

// Get a SimplePie feed object from the specified feed source.
// $RSSURL='http://feeds.feedburner.com/co/xbsZ';
$rss = fetch_feed( 'http://localhost/axacoid/inspirasi/?feed=short' );

if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly

    // Figure out how many total items there are, but limit it to 5. 
    $maxitems = $rss->get_item_quantity( 2 ); 

    // Build an array of all the items, starting with element 0 (first element).
    $rss_items = $rss->get_items( 0, $maxitems );

endif;

// echo "<pre>";
//     var_dump($rss_items[0]->feed->data['child']['']['rss'][0]['child'][""]["channel"][0]["child"][""]["item"][0]["child"]["http://purl.org/rss/1.0/modules/slash/"]["comments"]);

//This function will get an image from the feed
 
function returnImage ($text) {
    $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
    $pattern = "/<img[^>]+\>/i";
    preg_match($pattern, $text, $matches);
    $text = $matches[0];
    return $text;
}
 
//This function will filter out image url which we got from previous returnImage() function
function scrapeImage($text) {
    $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';     
    preg_match($pattern, $text, $link);
    $link = $link[1];
    $link = urldecode($link);
    return $link;
}
?>
<ul id="article-post" class="large-block-grid-2">
    <?php if ( $maxitems == 0 ) : ?>
        <li>belum ada konten</li>
    <?php else : ?>
        <?php // Loop through each feed item and display each item as a hyperlink. ?>
        <?php $count = 1;  foreach ( $rss_items as $item ) : 
        $item_feed = $rss->get_item(0);   // the first item in feed; do it in loop
        $comments = $item_feed->get_item_tags("http://purl.org/rss/1.0/modules/slash/", "comments");
                // echo "<pre>";
                 // var_dump($comments);
                 $number = $comments[0]['data'];
                 // var_dump($number);
        $countviews = $item_feed->get_item_tags("http://purl.org/rss/1.0/modules/slash/", "viewcount");
                // echo "<pre>";
                 // var_dump($comments);
        $countview = $countviews[0]['data'];

        $comment_child = $item_feed->get_item_tags("http://purl.org/rss/1.0/modules/slash/", "commentschildren");
                // echo "<pre>";
                 // var_dump($comments);
        $commentschildren = $comment_child[0]['data'];
        // var_dump($countview);
        $category = $item->get_category();
        $enclosure = $item->get_enclosure();
        $feedDescription = $item->get_description();
        $image = returnImage($feedDescription);
        $image = scrapeImage($image);
        #$comment =  $item->wp_count_comments($post->ID);
        ?>
            <?php# var_dump($item);?>
            <li class="post nth-child-<?=$count?> clearfix">
                <div class="large-6 columns thumbnail large-push-6" style="background:url(<?=$image?>) no-repeat top center;">
                    <a href="<?php echo esc_url( $item->get_permalink() ); ?>">
                    <div class="overlay"><img src="<?php bloginfo('template_url');?>/images/widget/separator.png" class="separator"><span>Selengkapnya</span></div>
                    </a>
                </div>
                <div class="details large-6 columns large-pull-6">                          
                    <div class="meta clearfix">
                        <div class="category large-6 small-6 columns">
                            <?=$category->get_label();?>
                    
                        </div>
                        <div class="date large-6 small-6 columns small-text-right large-text-right" title="<?php echo esc_html( $item->get_date('Y-m-d H:i:s')); ?>"><?php echo esc_html( $item->get_date('Y-m-d H:i:s')); ?></div>
                    </div>                    
                    <h3><a href="<?php echo esc_url( $item->get_permalink() ); ?>"><?php echo esc_html( $item->get_title() ); ?> </a></h3>
                    <div class="meta-info clearfix">
                        <div class="large-4 columns">
                            <i class="fa fa-eye"></i><span> <?=$countview?></span>
                        </div>
                        <div class="large-4 columns text-center">
                        <i class="fa fa-comments"></i> <?=$number?></div>
                        <div class="large-4 columns text-right">
                            <i class="fa fa-share"></i> <?=$commentschildren?></div>
                    </div>
                </div>
                 
            </li>
        <?php $count++;  endforeach; ?>
    <?php endif; ?>
</ul>
    <div class="toggleNav">
        <a href="#"><span class="tutup">Tutup <i class="fa fa-chevron-circle-up"></i></span><span class="buka">Buka <i class="fa fa-chevron-circle-down"></i></span></a>
    </div>
</div>
</div>