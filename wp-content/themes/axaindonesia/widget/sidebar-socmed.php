<div id="sidebar-socmed" class="bg-grey p-all-20 clearfix radius-all-5">
	<img src="<?php bloginfo('template_url');?>/images/logo-axa-small.png" alt="logo-axa-small" class="left"/>
	<div class="buttons left w-60p m-left-10">
		<h3 class="c-blue f-16">AXA Indonesia</h3>
		<div class="fb-like" data-href="https://www.facebook.com/axaindonesia" data-width="55" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
		<a href="https://twitter.com/axaindonesia" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @Twitter</a>
	</div>
</div>