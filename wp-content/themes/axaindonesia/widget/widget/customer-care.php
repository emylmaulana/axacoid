<div id="customerCare" class="cstCare widget">
	<a class="costumer block" href="#">
		<h3>AXA Customer Care</h3>
		<img src="<?php bloginfo('template_url');?>/images/icon-cstCare.png" class="icon-top absolute"/>
	</a>
	<div id="details" class="clearfix">
		<p>Pilih bantuan yang Anda butuhkan</p>
		<ul class="m-all-0 list-icon">
			<li class="produk-asuransi"><a href="#" class="block">Produk Asuransi</a></li>
			<li class="pengajuan-keluhan"><a href="<?php echo site_url('layanan-nasabah'); ?>" class="block">Pengajuan Keluhan</a></li>
			<li class="saran"><a href="<?php echo site_url('hubungi-kami'); ?>" class="block">Saran</a></li>
		</ul>

		<div class="selectwrapper" style="display:none">
			<select class="productContact" name="productcontact" class="" tabindex="1">
				<option value="0"><?php _e("<!--:en-->Choose Product<!--:--><!--:id-->Pilih Produk<!--:-->"); ?></option>
					<?php $query = new WP_Query( array('post_type'=>'cust_care','nopaging'=> true, 'orderby'=>'title', 'order'=> ASC) ) ?>
					<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
				<option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>				
				<?php
					endwhile;
					endif;		
					wp_reset_query();
				?>
			</select>
		</div>
		<div class="ajaxContact"></div>
	</div>
<!-- 	<div class="find-branch" class="clearfix">
		<p>Cari kantor cabang AXA terdekat di kota Anda</p>
		<form class="branch-locator">
			<select>
				<option>Pilih Kota</option>
			</select>
			<button type="input" class="button red">Cari</button>
		</form>
	</div> -->
</div>