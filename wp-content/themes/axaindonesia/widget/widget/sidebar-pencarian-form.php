<div id="pencarian-form" class="bg-separator clearfix">
	<div class="content-widget akses h-300">
		<form method="post" id="myform1" action="<?=site_url('result-formulir/')?>">
			<input type="hidden" name="a" id="a"/>
			<input type="hidden" name="b" id="b"/>
			<h5 class="f-16 c-blue"><?php _e("<!--:en-->SEARCH FORM<!--:--><!--:id-->PENCARIAN FORMULIR<!--:-->"); ?></h5>
			<p><?php _e("<!--:en-->Select AXA entity that provides forms you need<!--:--><!--:id-->Pilih entity AXA yang menyediakan formulir yang Anda butuhkan<!--:-->"); ?></p>
				<select id="pilih-entity" name="entity">
					<option>Pilih produk</option>
					<?php 
						$tax = array('formulir_produk');
						$args = array('order' => 'ASC', 'hide_empty' => true);
						$term = get_terms( $tax, $args);
						foreach($term as $term_tax)
						{
							$name = $term_tax->name;
							$slug = $term_tax->slug;
							echo "<option value=\"".$slug."\">".$name."</option>";
						}
					?>
				</select>

			<p><?php _e("<!--:en-->Select the type of form you need<!--:--><!--:id-->Pilih jenis formulir yang Anda butuhkan<!--:-->"); ?></p>
			<div id="pilihform-wrap" class="disable relative">
				<select id="pilih-form" name="formulir">
					<option>Pilih formulir</option>
				</select>
			</div>
			
			<button type="submit" class="button red small">Cari Formulir</button>
		</form>
		<script type="text/javascript">
			jQuery(document).ready(function() {	
				$('#pilih-entity').on('change', function (e) {
					var valueSelected = this.value;
					var title = $("#pilih-entity option[value='"+valueSelected+"']").text();
					$("#a").val(title);
					$("#b").val(valueSelected);
					$('#pilih-form').load('<?php bloginfo('template_url');?>/getForm.php?place=' + valueSelected);
					$('#pilihform-wrap').removeClass('disable');
				});
			});
		</script>
	</div>
</div>