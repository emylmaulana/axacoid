<div id="layanan" class="grey-2 w-322 clearfix mobile">
	<div class="content-widget akses">
		<div class="icon"></div>
			<div class="details">
			<h4><?php _e("<!--:en-->SEARCH FORM <!--:--><!--:id-->PENCARIAN FORMULIR<!--:-->"); ?></h4>
			<p><?php _e("<!--:en-->Find important documents and forms you need to enable us serve you<!--:--><!--:id-->Temukan dokumen dan formulir penting yang Anda butuhkan untuk memudahkan kami melayani Anda<!--:-->"); ?></p>
				<br/><br/><a href="<?php echo site_url('layanan-nasabah/formulir');?>" class="button red"><?php _e("<!--:en-->Search Form<!--:--><!--:id-->Cari Formulir<!--:-->"); ?></a>
		</div>
	</div>
</div>