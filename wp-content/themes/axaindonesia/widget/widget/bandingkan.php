<div id="bandingkan-widget" class="bg-greylight radius-all-5 h-255">
	<div class="box p-all-20">
		<div class="icon"></div>
		<h4 class="f-18 c-blue"><?php _e("<!--:en-->COMPARE PRODUCTS<!--:--><!--:id-->BANDINGKAN PRODUK<!--:-->"); ?></h4>
		<p class="m-bottom-10"><?php _e("<!--:en-->View and compare insurance products from AXA suit your protection needs<!--:--><!--:id-->Lihat dan bandingkan produk-produk asuransi dari AXA sesuai dengan kebutuhan perlidungan Anda<!--:-->"); ?></p>
		<a href="<?php echo site_url('bandingkan-produk');?>" class="button red">Bandingkan</a>
	</div>
</div><!--end direktori widget-->