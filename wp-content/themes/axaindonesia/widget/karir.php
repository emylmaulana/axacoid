<div id="karir-widget" class="clearfix">
	<div class="box clearfix">
	<h2 class="uppercase"><?php _e("<!--:en-->Career with AXA<!--:--><!--:id-->BERKARIR BERSAMA AXA<!--:-->"); ?></h4>
	<p>Temukan informasi lengkap untuk bergabung dan memiliki kesempatan berkarir menjadi karyawan atau agen asuransi AXA di seluruh Indonesia.</p>

		<a href="<?php echo site_url('karir'); ?>" class="button red small right"><?php _e("<!--:en-->Career<!--:--><!--:id-->Karir <!--:-->"); ?></a>
	</div>
</div>