<div id="direktori-widget" class="box clearfix">
	<h2><?php _e("<!--:en-->DIRECTORY<!--:--><!--:id-->DIREKTORI<!--:-->"); ?></h4>
	<p><?php _e("<!--:en-->Find the hospital and workshop partner AXA in the city nearest you to help you get the support<!--:--><!--:id-->Cari rumah sakit dan workshop rekanan asuransi AXA terdekat di kota Anda untuk memudahkan Anda mendapat dukungan<!--:-->"); ?></p>
	<form method="get" id="pilih-dir" action="<?php echo home_url( 'direktori/direktori-search/' ); ?>">
		<select id="pilih-direktori" name="wilayah">
			<option><?php _e("<!--:en-->Choose City<!--:--><!--:id-->Pilih Kota<!--:-->"); ?>
				<?php 
					$tax = array('direktori_wilayah');
					$args = array('order' => 'ASC', 'hide_empty' => true);
					$term = get_terms( $tax, $args);
					foreach($term as $term_tax)
					{
						$name = $term_tax->name;
						$slug = $term_tax->slug;
						echo "<option value=\"".$slug."\">".$name."</option>";
					}
				?>
			</option>
		</select>
        <button type="submit" id="searchsubmit-dir" class="button red"><?php _e("<!--:en-->Search<!--:--><!--:id-->Cari<!--:-->"); ?></button>
    </form>


</div><!--end direktori widget-->