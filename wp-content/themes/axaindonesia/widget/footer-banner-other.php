<?php if(get_field('banner_other', 'options')): ?>
	<?php while(has_sub_field('banner_other', 'options')): ?>
		<a href="<?php the_sub_field('file'); ?>" target="_blank"><img src="<?php the_sub_field('image'); ?>" class="banner" alt="banner"/></a>
	<?php endwhile; ?>
<?php endif; ?>