 <dl class="accordion" data-accordion="">
 <p><strong>Lihat persyaratan dan tata cara pengajuan klaim produk AXA</strong></p>

  <?php 
    $first = 0;
    $terms = get_terms("klaim_entity");
    $count = count($terms);
    if ( $count > 0 ){
      foreach ( $terms as $term ) {
        if($term->slug != $slug){
  ?>
  <dd>
    <a href="#<?=$term->slug?>"><?=$term->name?> </a>
    <div id="<?=$term->slug?>" class="content <?php if($first == 0) echo "active"; ?>">
    	<div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
    	<ul class="list-with-arrow column-2 ">
    		<?php 
				$args = array("post_type" => "klaim","posts_per_page" =>-1, "klaim_entity" =>$term->slug);
				query_posts( $args );
				if(have_posts()): while(have_posts()):the_post();
			?>
     		<li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
     		<?php endwhile;?>
    	 </ul>
    	 <?php endif;?>
    </div>
  </dd>
  <?php
        }
      $first++;
      }
    }
  ?>
  <!-- <dd>
    <a href="#asuransi">Produk Asuransi General</a>
    <div id="asuransi" class="content">
    <div class="arrow"><i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></div>
      	<ul class="list-with-arrow column-2 ">
        <?php 
        $args1 = array("post_type" => "klaim","posts_per_page" =>-1, "klaim_entity" =>"produk-asuransi-general");
        query_posts( $args1 );
        if(have_posts()): while(have_posts()):the_post();
      ?>
        <li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
        <?php endwhile;?>
       </ul>
       <?php endif;?>
    </div>
   
  </dd> -->
</dl>
