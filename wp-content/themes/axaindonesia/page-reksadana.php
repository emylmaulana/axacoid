<?php/** * Template Name: Layanan Nasabah: Reksa Dana */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570018&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1>Layanan Nasabah</h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>

		<section id="premi" class="clearfix sections grey">
			<h3 class="m-bottom-45">Reksa Dana</h3>
			<div id="page-half">
			<div class="large-8 columns clearfix">
				<div class="content-box clearfix m-bottom-20 bg-white">
					<h5 class="f-16 c-blue">PEMBELIAN REKSA DANA</h5>
					<p>AXA Indonesia menawarkan berbagai produk reksa dana sebagai solusi investasi bagi kebutuhan finansial Anda. Temukan tata cara pembelian produk reksa dana dari AXA Indonesia, dengan tata cara pembelian awal maupun pembelian lanjutan.</p>
					<ul class="list-with-arrow">
						<li><a href="<?php echo site_url('layanan-nasabah/reksa-dana/cara-pembelian/');?>">Cara Pembelian Awal</a></li>
						<li><a href="<?php echo site_url('layanan-nasabah/reksa-dana/cara-pembelian/');?>">Cara Pembelian Lanjutan</a></li>
						<li><a href="<?php echo site_url('layanan-nasabah/reksa-dana/cara-penjualan-kembali/');?>">Cara Penjualan Kembali</a></li>
						<li><a href="<?php echo site_url('layanan-nasabah/reksa-dana/cara-pengalihan/');?>">Cara Pengalihan</a></li>
					</ul>
					<a href="<?php echo site_url('layanan-nasabah/reksa-dana/cara-pembelian/');?>" class="button red small right">Cara Pembelian</a>
				</div><!--end content box-->

				<div class="large-6 columns p-left-0">
					<div id="" class="h-215 bg-grey clearfix radius-all-5">
					<div class="box  p-all-30 clearfix">
						<h3 class="c-blue f-16 uppercase">Ulasan Pasar Bulanan</h3>
						<form id="fund-fact">
					        	<select id="pilih-bulan">
									<option>Pilih Bulan </option>
									<option value="fmonth=1"> Januari </option>
									<option value="fmonth=2"> Februari </option>
									<option value="fmonth=3"> Maret </option>
									<option value="fmonth=4"> April </option>
									<option value="fmonth=5"> Mei </option>
									<option value="fmonth=6"> Juni </option>
									<option value="fmonth=7"> Juli </option>
									<option value="fmonth=8"> Agustus </option>
									<option value="fmonth=9"> September </option>
									<option value="fmonth=10"> Oktober </option>
									<option value="fmonth=11"> November </option>
									<option value="fmonth=12"> Desember </option>
								</select>

								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'fund_fact_sheet' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="pilih-tahun">
										<option>Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo get_post_type_archive_link( 'media' ).'&fyear='.$result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>
							
						<button type="submit" class="button red right">Cari</button>

						</form>
					</div>
				</div>
				</div>

				<div class="large-6 columns p-right-0">
					<div id="" class="h-215 bg-grey clearfix radius-all-5">
					<div class="box  p-all-30 clearfix">
						<h3 class="c-blue f-16 uppercase">Ulasan Pasar Harian</h3>
						<form id="pasar-harian">
					        	<select id="pilih-bulan-1">
									<option>Pilih Bulan </option>
									<option value="fmonth=1"> Januari </option>
									<option value="fmonth=2"> Februari </option>
									<option value="fmonth=3"> Maret </option>
									<option value="fmonth=4"> April </option>
									<option value="fmonth=5"> Mei </option>
									<option value="fmonth=6"> Juni </option>
									<option value="fmonth=7"> Juli </option>
									<option value="fmonth=8"> Agustus </option>
									<option value="fmonth=9"> September </option>
									<option value="fmonth=10"> Oktober </option>
									<option value="fmonth=11"> November </option>
									<option value="fmonth=12"> Desember </option>
								</select>

								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'ulasan_pasar_harian' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="pilih-tahun-1">
										<option>Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo get_post_type_archive_link( 'media' ).'&fyear='.$result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>
							<button type="submit" class="button red right">Cari</button>
						</form>
					</div>
					</div>
				</div>


			</div><!--end large 8-->

			<aside class="columns w-322">
				<?php get_template_part("widget/sidebar-pengajuan-klaim");?>
			</aside>
			</div>
		</section>

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>