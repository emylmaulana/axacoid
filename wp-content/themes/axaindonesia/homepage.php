<?php/** * Template Name: Homepage */?>
<?php get_header();?>
<!-- Home Page - Affiperf -  -->
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569851&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<img src='https://pixel.mathtag.com/event/img?mt_id=452935&mt_adid=100544&v1=&v2=&v3=&s1=&s2=&s3=' width='1' height='1' alt="mathtag"/>
	<div id="mainSlider">
		<div class="row box">
			<div class="large-11">
				<ul id="separate" class="personal">
					<li class="selected"><a href="<?php echo site_url("home");?>">Personal</a></li>
					<li><a href="<?php echo site_url('bisnis');?>">Bisnis</a></li>
				</ul>	
				<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
			</div>
		</div>
		<ul id="slideContent" class="owl-carousel hide-on-tablet">
			<?php 
				$args = array("post_type" => "slide_personal", //wp_query get post_type slide_personal
					"posts_per_page" =>8, //get 5 item
					'tax_query'=>array(  //taxquery untuk query tambahan jika yg ingin lebih optional
						array('taxonomy'=>"tipe_layar", //misal postype slide_personal ingin mengambil yang tipe_layarnya yg desktop
								'field'=>'slug',
								'terms'=>'desktop'
							)) );
				query_posts( $args ); //compile query 
				if(have_posts()): while(have_posts()):the_post();
			?>

			<?php if(get_field('video_slide'))://jika video_slide itu ada maka akan ditampilakan sesuai prioritas ?>
			<li class="relative">
				<div class="slideBG">
					 <video class="video-bg" width="100%" height="auto" loop autoplay>
					    <source src="<?php the_field('video_slide');?>" type="video/webm">
					    <source src="<?php the_field('video_mp4');?>" type='video/mp4'/>
					    <source src="<?php the_field('video_ogv');?>" type="video/ogg"/>
					  </video>
					<div class="row">
						<div class="caption">
							<!-- <h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3> -->
							<!-- <a href="<?php the_field("slide_url");?>" class="playvideo buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a> -->
							<?php $slide_url = get_field("slide_url"); ?>
							<a href="<?=(isset($slide_url) && $slide_url != "") ? $slide_url : 'javascript:void(0);'?>" class="playvideo buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
				</div>
				<!-- <iframe id="ytvideo" style="display:none" width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('slide_video_url');?>?enablejsapi=1&version=3&playerapiid=ytvideo?rel=0&controls=1&fs=0&modestbranding=1&showinfo=0" frameborder="0" wmode="Opaque" allowfullscreen></iframe> -->
				<iframe id="ytvideo" style="display:none" width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('slide_video_url');?>?enablejsapi=1&version=3&playerapiid=ytvideo-mobile&rel=0&controls=1&fs=0&modestbranding=1&showinfo=0&autoplay=0" frameborder="0" wmode="Opaque" allowfullscreen allowscriptaccess="always"></iframe>
				<a href="#" class="closeVideo" style="display:none;">X</a>
			</li>
			<?php elseif(get_field('slider_image')):?>
			<li class="relative">
				<div class="slideBG" style="background-image:url(<?php the_field('slider_image');?>);"><a href="<?php the_field("slide_url");?>" class="slide-link"></a>
					<?php if(get_field('slide_button_text')):?>
					<div class="row">
						<div class="caption">
							<!-- <h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3> -->
							<a href="<?php the_field("slide_url");?>" class="buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
					<?php endif;?>
				</div>
			</li>
			<?php endif;?>
			<?php endwhile;?>
		</ul>
		<?php endif;?>

		<ul id="mobile-slide" class="tablet-content owl-carousel">
			<?php 
			//mobile slider
				$args = array("post_type" => "slide_personal", //wp_query get post_type slide_personal
					"posts_per_page" =>8, //get 5 item
					'tax_query'=>array(
						array('taxonomy'=>"tipe_layar", //taxquery untuk query tambahan jika yg ingin lebih optional
								'field'=>'slug',
								'terms'=>'mobile' //misal postype slide_personal ingin mengambil yang tipe_layarnya mobile
							)) );
				query_posts( $args );
				if(have_posts()): while(have_posts()):the_post();
			?>
			<?php if(get_field('video_slide')):?>
			<li class="relative">
				
				<div class="slideBG"  style="background-image:url(<?php the_field('slider_image');?>);">
					<div class="video-bg"></div>
					<div class="row">
						<div class="caption">
							<!-- <h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3> -->
							<?php $slide_url = get_field("slide_url"); ?>
							<a href="<?=(isset($slide_url) && $slide_url != "") ? $slide_url : 'javascript:void(0);'?>" class="playvideo buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
				</div>
				<iframe id="ytvideo-mobile" style="display:none" width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('slide_video_url');?>?enablejsapi=1&version=3&playerapiid=ytvideo-mobile&rel=0&controls=1&fs=0&modestbranding=1&showinfo=0&autoplay=0" frameborder="0" wmode="Opaque" allowfullscreen allowscriptaccess="always"></iframe>
				<a href="#" class="closeVideo" style="display:none;">X</a>
			</li>
			<?php elseif(get_field('slider_image')):?>
			<li class="relative">
				<div class="slideBG"  style="background-image:url(<?php the_field('slider_image');?>);"><a href="<?php the_field("slide_url");?>" class="slide-link"></a>
					<?php if(get_field('slide_button_text')):?>
					<div class="row">
						<div class="caption">
							<!-- <h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3> -->
							<a href="<?php the_field("slide_url");?>" class="buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
				<?php endif;?>
				</div>
			</li>
			
			<?php endif;?>
			<?php endwhile;?>
			
		</ul>
		<?php endif;?>
	</div>



	<div class="row solusiContainer">
		<div id="solusiWidget" class="large-11 clearfix">
			<div id="title">
				<h3><?php _e("<!--:en-->Our Solution<!--:--><!--:id-->Solusi Kami<!--:-->"); ?></h3>
			</div>	
			<div class="box clearfix">
				<div id="intro">
					<p><?php _e("<!--:en-->Find the perfect protection solutions with optimum benefits, according to the needs of you and your family.<!--:--><!--:id-->Temukan berbagai jenis asuransi dengan solusi perlindungan sempurna dan manfaat optimal, sesuai dengan kebutuhan Anda dan keluarga.<!--:-->"); ?></p>
				</div>
				<ul class="clearfix">
					<?php while(has_sub_field('personal_page', 'options')): ?>
					<li class="kesehatan"><a href="<?php echo site_url('solusi-kesehatan');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->Healthy<!--:--><!--:id-->Kesehatan<!--:-->"); ?></h4> <div class="desc" style="display:none;"><p><?php the_sub_field('kesehatan'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<li class="proteksi"><a href="<?php echo site_url('solusi-proteksi');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->Protection<!--:--><!--:id-->Proteksi<!--:-->"); ?></h4> <div class="desc" style="display:none;"><p><?php the_sub_field('proteksi'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<li class="investasi"><a href="<?php echo site_url('solusi-investasi');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->Investment<!--:--><!--:id-->Investasi<!--:-->"); ?></h4><div class="desc" style="display:none;"><p><?php the_sub_field('investasi'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<li class="general"><a href="<?php echo site_url('solusi-umum');?>"><div class="icon"></div> <h4><?php _e("<!--:en-->General<!--:--><!--:id-->Umum<!--:-->"); ?></h4> <div class="desc" style="display:none;"><p><?php the_sub_field('kesehatan'); ?></p><button class="button red m-top-5 small"><?php _e("<!--:en-->View Solutions<!--:--><!--:id-->Lihat Solusi<!--:-->"); ?></button></div></a></li>
					<?php endwhile;?>
				</ul>
			</div>
		</div>
	</div>

	<div id="advisor-teaser">
		<div class="row">
			<div class="large-6 columns">
				<h3><span></span>AXA Solution Advisor</h3>
				<p>Cari tahu jenis perlindungan sesuai kebutuhan Anda</p>	
			</div>
			<div class="large-5 columns">
				<form action="<?php echo site_url('solution-advisor'); ?>" method='GET'>
					<label>Tahun ini saya berumur</label>
					<input type="text" id="age" value="25"/>
					<input type="submit" value="Mulai" class="button red"/>
					<input type="hidden" name="age" id="hasil-age" value=""/>
				</form>
			</div>
		</div>	

		<script type="text/javascript">
		$(document).ready(function(){
			
			$("#age").keyup(function(){
				var age = $("#age").val();
				if (age>=1 && age<=25) {
					$("#hasil-age").attr("value", "20");
				}else if (age>=26 && age<=40) {
					$("#hasil-age").attr("value", "30");
				}else if (age>=41 && age<=55) {
					$("#hasil-age").attr("value", "50");
				}else {
					$("#hasil-age").attr("value", "57");
				};
			}) .keyup();
			})
		</script>
	</div>

	<?php get_template_part("widget/hargaunit");?>


	<div id="layanan">
		<div class="row">
			<div class="large-11 column centered">
				<h2><?php _e("<!--:en-->SERVICE IN YOUR HAND<!--:--><!--:id-->LAYANAN DI TANGAN ANDA<!--:-->"); ?></h2>
				<ul class="clearfix">
					<li class="large-4 columns premi">
						<div class="icon"></div>
						<div class="details">
							<h2><?php _e("<!--:en-->PAYMENT OF PREMIUM<!--:--><!--:id-->PEMBAYARAN PREMI<!--:-->"); ?></h2>
							<p class="show-for-large-only"><?php _e("<!--:en-->Find a wide selection of easy solutions in advanced premium payment service for all customers<!--:--><!--:id-->Temukan berbagai pilihan solusi mudah dalam pelayanan pembayaran premi asuransi lanjutan untuk semua nasabah<!--:-->"); ?></p>
								<a href="<?php echo site_url('layanan-nasabah/pembayaran-premi/');?>" class="button red"><?php _e("<!--:en-->Payment Options<!--:--><!--:id-->Pilihan Pembayaran<!--:-->"); ?></a>
						</div>
					</li>
					<li class="large-4 columns klaim">
						<div class="icon"></div>
						<div class="details">
							<h2><?php _e("<!--:en-->CLAIM<!--:--><!--:id-->PENGAJUAN KLAIM<!--:-->"); ?></h2>
							<p class="show-for-large-only"><?php _e("<!--:en-->Get the documents required for filing a claim and we will immediately give you the support you need<!--:--><!--:id-->Dapatkan dokumen-dokumen yang diperlukan untuk mengajukan klaim asuransi dan kami akan segera memberikan dukungan yang Anda butuhkan<!--:-->"); ?></p>
								<a href="<?php echo site_url('layanan-nasabah/pengajuan-klaim/');?>" class="button red"><?php _e("<!--:en-->Claim Center<!--:--><!--:id-->Pusat Klaim<!--:-->"); ?></a>
						</div>
					</li>
					<li class="large-4 columns akses">
						<div class="icon"></div>
						<div class="details">
							<h2><?php _e("<!--:en-->SEARCH FORM <!--:--><!--:id-->PENCARIAN FORMULIR<!--:-->"); ?></h2>
							<p class="show-for-large-only"><?php _e("<!--:en-->Find important documents and forms you need to enable us serve you<!--:--><!--:id-->Temukan dokumen dan formulir penting yang Anda butuhkan untuk memudahkan kami melayani Anda<!--:-->"); ?></p>
								<a href="<?php echo site_url('layanan-nasabah/formulir');?>" class="button red">Cari Formulir</a>
						</div>
					</li>
				</ul>
			</div>
		</div><!--end row-->
	</div><!--end layanan-->
	<?php get_template_part("widget/inspirasi");?>
	<div id="region-1">
		<div class="row">
			<div class="large-11 large-centered columns show-for-large-only">
				<div class="large-4 columns">
					<?php get_template_part("widget/footer-banner-left");?>
				</div>
				<div class="large-4 columns">
					<?php get_template_part("widget/bandingkan");?>
				</div>
				<div class="large-4 columns">
					<?php get_template_part("widget/berita-terbaru");?>
				</div>
			</div>
		</div><!--end row-->
	</div><!--end region 1-->

<?php get_footer();?>