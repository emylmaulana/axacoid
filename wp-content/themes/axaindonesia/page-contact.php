<?php/** * Template Name: Contact Us */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570045&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php the_field('header_image');?>);"></div>
		<div class="content large-4">
			<h1><?php the_title();?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<section id="page-half" class="white clearfix sections radius-all-5">
			<div class="large-8 columns">
					 <?php if( have_posts() ) : the_post(); ?>
						<h3 class="m-bottom-30 f-24"><?php the_title();?></h3>
						<!-- form -->
						<form action="<?=site_url()?>/axaone_form/hubungi_kami/submit_data" id="form" method="post" class="wpcf7-form" >
							<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="13">
							<input type="hidden" name="_wpcf7_version" value="3.6">
							<input type="hidden" name="_wpcf7_locale" value="id_ID">
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f13-p1315-o1">
							<input type="hidden" name="_wpnonce" value="21068c9497">
							<input type="hidden" name="banner_source" value="">
							<input type="hidden" name="utm_source" value="">
							<input type="hidden" name="utm_medium" value="">
							<input type="hidden" name="utm_term" value="">
							<input type="hidden" name="utm_content" value="">
							<input type="hidden" name="utm_campaign" value="">
							<input type="hidden" name="gclid" value="">
							</div>
							<div id="contact-form">
							<div class="fieldset">
							    <label>Nama*</label><br>
							     <span class="wpcf7-form-control-wrap nama" style="width: 508px;"><input type="text" name="nama" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Tanggal Lahir*</label><br>
							     <span class="wpcf7-form-control-wrap tgl_lahir" style="width: 508px;"><input type="text" name="tgl_lahir" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required datepicker " aria-required="true" aria-invalid="false" id="dp1392892890430"></span>
							  </div>
							<div class="fieldset">
							    <label>Email*</label><br>
							     <span class="wpcf7-form-control-wrap email" style="width: 508px;"><input type="email" name="email" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Alamat*</label><br>
							     <span class="wpcf7-form-control-wrap alamat" style="width: 508px;"><input type="text" name="alamat" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>No. HP*</label><br>
							     <span class="wpcf7-form-control-wrap no_hp" style="width: 508px;"><input type="text" name="no_hp" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>No. Telp Rumah</label><br>
							     <span class="wpcf7-form-control-wrap no_tlp" style="width: 508px;"><input type="text" name="no_tlp" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>No. Polis (jika ada)</label><br>
							     <span class="wpcf7-form-control-wrap no_polis" style="width: 508px;"><input type="text" name="no_polis" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Kategori</label><br>
							    <span class="wpcf7-form-control-wrap kategori" style="width: 508px;"><select name="kategori" class="wpcf7-form-control wpcf7-select" aria-invalid="false"><option value="">---</option><option value="Pertanyaan Umum">Pertanyaan Umum</option><option value="Informasi Produk">Informasi Produk</option><option value="Informasi Financial Advisor">Informasi Financial Advisor</option><option value="Saran">Saran</option><option value="Keluhan">Keluhan</option><option value="Lainnya">Lainnya</option></select></span>
							  </div>
							<div class="fieldset">
							    <label>Subjek*</label><br>
							     <span class="wpcf7-form-control-wrap subjek" style="width: 508px;"><input type="text" name="subjek" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Pesan Anda*</label><br>
							     <span class="wpcf7-form-control-wrap pesan" style="width: 508px;"><textarea name="pesan" cols="40" rows="10" class="required wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span>
							  </div>
							<div class="fieldset">
							    <label></label><br>
							    <input type="submit" value="Kirim" class="wpcf7-form-control wpcf7-submit button red m-top-30">
							  </div>
							  
							</div>
							<div class="wpcf7-response-output wpcf7-display-none" style="display: none;"></div>
						<p class="cap_status" style="display:none;"></p>
						<!-- <p class="cap_sukses" style="display:none;"><?php echo $succ; ?></p> -->
						</form>
						<!-- form -->
						
						
					<?php endif;?>
			</div><!--end large 8-->
			<aside class="columns w-322 m-top-45">
			<div class="widget">
				<strong class="c-blue" style="font-size:18px;display:block;">AXA Indonesia</strong><br>
				<ul class="office-details">
	           		<li class="address"><?php the_field('office_address', 'option');?></li>
	           		<li class="phone"><?php the_field('office_phone', 'option');?></li>
           		</ul>
			 </div>
			 <div class="widget"><?php get_template_part("widget/footer-banner-left");?></div>
			 <div class="widget"><?php get_template_part("widget/footer-banner-right");?></div>
			</aside>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<script type="text/javascript">
			// var capch = '<?php echo ($_SESSION['cap_code']); ?>' ;
			// if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// 	$('#img-captcha').hide();
			// 	$('#captcha').attr('type', 'hidden');
			// 	$('#captcha').val(capch);
			// }
			$(document).ready(function(){
		 		//captcha check
				$("#form").submit(function(){
				
				    if($("#form").valid()) {
				    	console.log('1234');
	                    if(capch == $('#captcha').val()){
	                    	console.log('test');
	                    	return true;

	                    }else{
	                        $('.cap_status').html("Captcha yang Anda masukkan salah!").addClass('cap_status_error').fadeIn('slow');
	                    }
				    }else{
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    }

				    return false;
				 });
				// var thisURLSuccess = '<?php echo site_url("hubungi-kami/?succ=Data+terkirim"); ?>';
				// if (window.location.href == thisURLSuccess ) {
				// 	$('.cap_sukses').show();
				// }
		 	});
		</script>

<?php get_footer();?>
