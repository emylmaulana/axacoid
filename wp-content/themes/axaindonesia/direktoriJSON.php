<?php
/*
Template Name: Direktori JSON
*/
?>
<?php 
$keyword = ($_GET['q']) ? $_GET['q'] : '';
	$t=$_GET['t'];// tambahan variable jika hanya satu entity yg masuk
	$tax = ($_GET['tax']) ? $_GET['tax'] : '';
	$tax2 = ($_GET['wilayah']) ? $_GET['wilayah'] : '';
	$nama_providers = ($_GET['nama_providers']) ? $_GET['nama_providers'] : '-1';
	$nama_produk = ($_GET['nama_produk']) ? $_GET['nama_produk'] : '-1';
	$x =($_GET['x']) ? $_GET['x'] :'';
	$y =($_GET['y']) ? $_GET['y'] :'';
	// $args = array();
	// $new_array_post_id = array();
	$tax_arr=explode(",", $_GET['tax']);
	$tax_count=count($tax_arr);

header('Content-Type: application/json');

if($x!=''&& $y!=''){
	$orderdistance="order by distance asc";
	$distance=",( 6371 * ACOS( COS( RADIANS(".$x."))*COS(RADIANS(directory_advanced.latitude))*COS(RADIANS(directory_advanced.longitude)-RADIANS(".$y."))+SIN(RADIANS(".$x."))*SIN(RADIANS(directory_advanced.latitude)))) AS distance";
}else{
	$distance="";
	$orderdistance=" order by post_title asc";
}
if($keyword != '' && $tax == '' && $nama_providers=='-1' && $nama_produk=='-1'){ //keyword
	$args = array("post_type" => "rumah_sakit",
			  "posts_per_page" =>-1, 
			  'orderby' => 'post__in', 
			  'order' => ASC 
			  // 'paged' => $paged,
			  // 's' => $keyword
		);
	$sql= $wpdb->get_results("select *$distance from directory_advanced	
										where ((directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
										) $orderdistance");
	// echo "select *$distance from directory_advanced	
	// 									where ((directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
	// 									) $orderdistance";
	$sql = array_splice($sql, 0);	
	$new_array_args_id = array();
	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump(intval($values->id));
		}

			$args['post__in'] = $new_array_args_id;
			// $args['orderby'] = 'post__in';
			// $args['paged']=$paged;
			// $args['order']=ASC;
}
elseif($tax!='' && $keyword == '' && $nama_providers=='-1' && $nama_produk=='-1') {//tax
	$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'title', 
									  'order' => ASC, 
									  'paged' => $paged,
									 'tax_query'=>array(
									 					'relation'=>'AND',
														array(
																'taxonomy'=>'solusi',
																'field'=>'slug',
																'terms'=>$tax_arr
																
															)
														)
									);	
}
elseif($tax== '' && $keyword=='' && $nama_providers=='-1' && $nama_produk!='-1' ){//produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  'paged' => $paged
									 
									);
							
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where (directory_advanced.nama_produk like '%".$nama_produk."%') $orderdistance");
							
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);
}
elseif($tax=='' && $keyword == '' && $nama_providers!='-1' && $nama_produk=='-1') {//providers
	$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  //'s'=>$keyword,
									  //'paged' => $paged
									  //'solusi'=>$tax
									 // 'tax_query'=>array(
									 // 				'relation'=>'OR',
										// 				array(
										// 						'taxonomy'=>'solusi',
										// 						'field'=>'slug',
										// 						'terms'=>$tax_arr
																
										// 					)
										// 				)
									);	
							
							
								$sql = $wpdb->get_results("select*$distance from directory_advanced	
															where (directory_advanced.providers='".$nama_providers."') $orderdistance");
							
							
							
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									//$args['orderby'] = 'post__in';
									// $args['paged']=$paged;
									//$args['order']=ASC;
							// var_dump($args);
}
elseif($tax== '' && $keyword!='' && $nama_providers=='-1' && $nama_produk!='-1' ){//keyword,produk 

	$args = array("post_type" => "rumah_sakit",
			  "posts_per_page" =>-1, 
			  'orderby' => 'post__in', 
			  'order' => ASC 
			  // 'paged' => $paged
			 
			);
	
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.nama_produk like '%".$nama_produk."%')
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
	
	// var_dump($sql);
	$sql = array_splice($sql, 0);	
	$new_array_args_id = array();
	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
			
	// var_dump($args);

}
elseif($tax=='' && $keyword != '' && $nama_providers!='-1' && $nama_produk=='-1') {//keyword, providers, 
	$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC
									  // 'paged' => $paged
									  
									);
							
	$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.providers='".$nama_providers."' ) 
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
									) $orderdistance");
	
	$sql = array_splice($sql, 0);
	
	$new_array_args_id = array();

	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
			// $args['orderby'] = 'post__in';
			// $args['paged']=$paged;
			// $args['order']=ASC;
	// var_dump($args);
}
elseif($tax != '' && $keyword != '' && $nama_providers=='-1' && $nama_produk=='-1'){ //keyword,tax
	$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  //'s'=>$keyword,
									  //'paged' => $paged
									  //'solusi'=>$tax
									 // 'tax_query'=>array(
									 // 				'relation'=>'OR',
										// 				array(
										// 						'taxonomy'=>'solusi',
										// 						'field'=>'slug',
										// 						'terms'=>$tax_arr
																
										// 					)
										// 				)
									);	
							
							if($tax_count==3){
								$sql = $wpdb->get_results("select*$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															) $orderdistance");
								// echo "3";
								// echo "select*$distance from directory_advanced	
								// 							where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
								// 							and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
								// 							) $orderdistance";
							}elseif($tax_count==2){
								$sql = $wpdb->get_results("select*$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															) $orderdistance");
								// echo "2";
								
							}elseif($tax_count==1){
								$sql = $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
															) $orderdistance");
								// echo "1";
								
							}
							
							
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									//$args['orderby'] = 'post__in';
									// $args['paged']=$paged;
									//$args['order']=ASC;
							// var_dump($args);
	
}
elseif($tax != '' && $nama_providers!='-1' && $keyword=='' && $nama_produk=='-1'){ //tax, provider

	$args = array("post_type" => "rumah_sakit",
			  "posts_per_page" =>-1, 
			  'orderby' => 'title', 
			  'order' => ASC, 
			  'paged' => $paged,
			 'tax_query'=>array(
			 				'relation'=>'AND',
								array(
										'taxonomy'=>'solusi',
										'field'=>'slug',
										'terms'=>$tax_
										
									),
								array(
										'taxonomy'=>'providers',
										'field'=>'id',
										'terms'=>$nama_providers,
										'compare'=>'IN'
										
									)
								)
			);
	if($tax_count==1){
		$sql= $wpdb->get_results("select * from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
									and (directory_advanced.providers='".$nama_providers."'))");
	}elseif($tax_count==2){
		$sql= $wpdb->get_results("select * from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
									and (directory_advanced.providers='".$nama_providers."'))");
	}elseif($tax_count==3){
		$sql= $wpdb->get_results("select * from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
									and (directory_advanced.providers='".$nama_providers."'))");
	}
	$sql = array_splice($sql, 0);	
	$new_array_args_id = array();
	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->post_id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
			$args['orderby'] = 'post__in';
			$args['paged']=$paged;
			$args['order']=ASC;
	// var_dump($args);

}
elseif($tax != '' && $nama_providers=='-1' && $keyword=='' && $nama_produk!='-1'){//tax, produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  // 'paged' => $paged,
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance");
								echo "select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.nama_produk='".$nama_produk."')) $orderdistance";
							}
							// var_dump($sql);	
							$sql = array_splice($sql, 0);

							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
									
							// var_dump($args);
}
elseif($tax == '' && $nama_providers!='-1'&& $nama_produk!='-1' && $keyword=='' ){//providers, produk 

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC, 
									  // 'paged' => $paged
									 
									);
							
							$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
							// echo "select *$distance from directory_advanced	
							// 								where ((directory_advanced.providers='".$nama_providers."')
							// 								and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance";
							
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

}
elseif($keyword!='' && $tax!='' && $nama_providers!='-1'&&  $nama_produk=='-1'){ //keyword, tax, provider

	$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC
									  // 'paged' => $paged
									  
									);
	if($tax_count==1){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
									and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
	}elseif($tax_count==2){
		$sql= $wpdb->get_results("select*$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
									and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
	}elseif($tax_count==3){
		$sql= $wpdb->get_results("select*$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
									and (directory_advanced.providers='".$nama_providers."')) $orderdistance");
		// echo "tiga select*$distance from directory_advanced	
		// 							where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
		// 							and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')
		// 							and (directory_advanced.providers='".$nama_providers."')) $orderdistance";
		
	}
	$sql = array_splice($sql, 0);
	
	$new_array_args_id = array();

	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
			// $args['orderby'] = 'post__in';
			// $args['paged']=$paged;
			// $args['order']=ASC;
	// var_dump($args);

}
elseif($tax != '' && $keyword!='' && $nama_providers=='-1' && $nama_produk!='-1' ){//tax,keyword,produk 

	$args = array("post_type" => "rumah_sakit",
			  "posts_per_page" =>-1, 
			  'orderby' => 'post__in', 
			  'order' => ASC, 
			  'paged' => $paged
			 
			);
	if($tax_count==1){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' )
									and (directory_advanced.nama_produk like '%".$nama_produk."%')
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
	}elseif($tax_count==2){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."')
									and (directory_advanced.nama_produk like '%".$nama_produk."%')
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
	}elseif($tax_count==3){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."')
									and (directory_advanced.nama_produk like '%".$nama_produk."%')
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
	echo "select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."')
									and (directory_advanced.nama_produk like '%".$nama_produk."%')
									and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance";
	}
	// var_dump($sql);
	$sql = array_splice($sql, 0);	
	$new_array_args_id = array();
	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
			
	// var_dump($args);

}
elseif($tax != '' && $nama_providers!='-1'&& $nama_produk!='-1' && $keyword=='' ){ //tax, provider, produk

	$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  // 'paged' => $paged
									 
									);
	if($tax_count==1){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
									and (directory_advanced.providers='".$nama_providers."')
									and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
	}elseif($tax_count==2){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
									and (directory_advanced.providers='".$nama_providers."')
									and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
	}elseif($tax_count==3){
		$sql= $wpdb->get_results("select *$distance from directory_advanced	
									where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
									and (directory_advanced.providers='".$nama_providers."')
									and (directory_advanced.nama_produk like '%".$nama_produk."%')) $orderdistance");
	}
	$sql = array_splice($sql, 0);	
	$new_array_args_id = array();
	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
			
	// var_dump($args);

}
elseif($tax == '' && $keyword!='' && $nama_providers!='-1' && $nama_produk!='-1' ){ //provider, produk, keyword

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  // 'paged' => $paged
									 
									);
							
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

}
elseif($tax != '' && $keyword!='' && $nama_providers!='-1' && $nama_produk!='-1' ){ //tax, provider, produk, keyword

							$args = array("post_type" => "rumah_sakit",
									  "posts_per_page" =>-1, 
									  'orderby' => 'post__in', 
									  'order' => ASC 
									  // 'paged' => $paged
									 
									);
							if($tax_count==1){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' ) 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							}elseif($tax_count==2){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."') 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							}elseif($tax_count==3){
								$sql= $wpdb->get_results("select *$distance from directory_advanced	
															where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
															and (directory_advanced.providers='".$nama_providers."')
															and (directory_advanced.nama_produk like '%".$nama_produk."%')
															and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance");
							// echo "select *$distance from directory_advanced	
							// 								where ((directory_advanced.solusi='".$tax_arr[0]."' or directory_advanced.solusi='".$tax_arr[1]."' or directory_advanced.solusi='".$tax_arr[2]."') 
							// 								and (directory_advanced.providers='".$nama_providers."')
							// 								and (directory_advanced.nama_produk like '%".$nama_produk."%')
							// 								and (directory_advanced.post_title like '%".$keyword."%'or directory_advanced.rs_alamat like '%".$keyword."%' or directory_advanced.wilayah like '%".$keyword."%')) $orderdistance";
							}
							// var_dump($sql);
							$sql = array_splice($sql, 0);	
							$new_array_args_id = array();
							foreach($sql as $row => $values)
								{
									$new_array_args_id[] = intval($values->id);
									// var_dump($new_array_args_id);
								}

									$args['post__in'] = $new_array_args_id;
									
							// var_dump($args);

}




else{
	$args = array( 'post_type' => array('rumah_sakit'),'orderby' => 'post__in', 'order' => ASC, 'nopaging' => true );
	$sql= $wpdb->get_results("select *$distance from directory_advanced $orderdistance");
	// echo "select *$distance from directory_advanced $orderdistance";						
							// var_dump($sql);
	$sql = array_splice($sql, 0);	
	$new_array_args_id = array();
	foreach($sql as $row => $values)
		{
			$new_array_args_id[] = intval($values->id);
			// var_dump($new_array_args_id);
		}

			$args['post__in'] = $new_array_args_id;
}

// if ($x !='' && $y !='') {//query menghitung koordinat
// 	$post_id_and_distance=$wpdb->get_results("SELECT post_id, post_title, ( 6371 * ACOS( COS( RADIANS($x))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS($y))+SIN(RADIANS($x))*SIN(RADIANS(latitude)))) AS distance
// 																	FROM  `directory_coordinates` 
// 																	ORDER BY  `distance` ASC");	
// 	$post_id_and_distance = array_splice($post_id_and_distance, 0);	
	
// 	foreach($post_id_and_distance as $row => $value)
// 	{
// 		$new_array_post_id[] = intval($value->post_id);
		
// 	}

// 	$args['post__in'] = $new_array_post_id;
// 	$args['orderby'] = 'post__in';
// 	$args['paged']=$paged;
// 	$args['order']=ASC;
// }

// $myquery1 = new WP_Query( $args );
// $myquery2 = new WP_Query( $args2 );
// $myquery3 = new WP_Query( $args3 );

$myquery = new WP_Query($args);
// $myquery->posts = array_merge($myquery1->posts,$myquery2->posts,$myquery3->posts);
// $myquery->post_count = $myquery1->post_count + $myquery2->post_count + $myquery3->post_count;   

if ( $myquery->have_posts() ) : 
	$output = array();
	while ( $myquery->have_posts() ) : $myquery->the_post(); 
		
			$location = get_field('rs_map');
			$coord = explode(",", $location['coordinates']);
			$data = '<div class="infowindow-data">
						<strong class="c-blue f-16 block m-bottom-10">'.get_the_title().'</strong>
						<div class="icon left '.get_post_type( $post_id ).'"><span></span></div>
						<div class="details left m-left-10 f-12">
							<div class="addressSmall">'.get_field('rs_alamat').'</div>
							<div class="phoneSmall">Phone: '.get_field('rs_telepon').'</div>
							<div class="faxSmall">Fax: '.get_field('rs_fax').'</div>
						</div>
					</div>';
		if ($post->post_type == "rumah_sakit") {
					$wpurl= site_url();
				array_push($output, array('latLng'=>$coord,'data'=>$data, 'options'=>array('iconpng'=>$wpurl.'/wp-content/themes/axaindonesia/images/marker-rs.png')));
				} else {
				array_push($output, array('latLng'=>$coord,'data'=>$data, 'options'=>array('iconpng'=>$wpurl.'/wp-content/themes/axaindonesia/images/marker-bengkel.png')));
				}
		

	endwhile;
	
endif;  

echo json_encode($output);

 ?>