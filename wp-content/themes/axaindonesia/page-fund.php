<?php/** * Template Name: Fund Fact */?>
<?php $selectedYear = ($_GET['fyear']) ? $_GET['fyear'] : ''; ?>
<?php $selectedMonth = ($_GET['fmonth']) ? $_GET['fmonth'] : ''; ?>
<?php get_header();?>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1>Layanan Nasabah</h1>
			<h2 style="color:#626269">Layanan untuk nasabah AXA Indonesia seputar produk asuransi, polis, pembayaran premi, klaim, dan lainnya.</h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>

		<section class="grey">			
			<div class="sections clearfix">
				<h3>Ulasan Pasar Bulanan</h3>
				<form id="fund-fact" name="kinerja-bulanan" class="clearfix h-35 m-bottom-10" action="<?php the_permalink(); ?>" method="GET">
					<select id="pilih-bulan" class="w-135 left">
						<option>Pilih Bulan </option>
						<option value="fmonth=1"> Januari </option>
						<option value="fmonth=2"> Februari </option>
						<option value="fmonth=3"> Maret </option>
						<option value="fmonth=4"> April </option>
						<option value="fmonth=5"> Mei </option>
						<option value="fmonth=6"> Juni </option>
						<option value="fmonth=7"> Juli </option>
						<option value="fmonth=8"> Agustus </option>
						<option value="fmonth=9"> September </option>
						<option value="fmonth=10"> Oktober </option>
						<option value="fmonth=11"> November </option>
						<option value="fmonth=12"> Desember </option>
					</select>

					<?php 		
						$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'fund_fact_sheet' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
						$results = $wpdb->get_results($query);
						$currentYear = $results[0]->year;
						?>			
							
						<select id="kinerja-tahun" class="w-135 left m-left-10">
							<option>Pilih Tahun</option>
						<?php
						foreach($results as $result){
						$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
						?>		
								<option value="<?php echo get_post_type_archive_link( 'media' ).'&fyear='.$result->year; ?>"><?php echo $result->year; ?></option>
						<?php
									}
						?>		
					</select>
					<button id="button" type="submit" class="button red left m-left-10">Cari</button>
				</form>
				<div id="page-half" style="clear:both;">
					<div class="large-8 columns p-left-0">
						<ul class="pdf-list small-block-grid-1 medium-block-grid-1 large-block-grid-1 clearfix">
						<?php 
							$args = array("post_type" => "fund_fact_sheet","posts_per_page" => -1, "year"=>$selectedYear, "monthnum" => $selectedMonth);
							query_posts( $args );
							if(have_posts()): while(have_posts()):the_post();
						?>

						
							<?php while(has_sub_field('reksa_pdf_file')) : ?>
								<li class="p-bottom-0"><a href="<?php the_sub_field('file');?>" class="clearfix block white shadow-grey-3" download><span class="icon"></span><strong><?php the_sub_field('name');?></strong><i class="fa fa-download right"></i></a></li>
							<?php endwhile; ?>
						

						<?php endwhile;?>
						</ul>
						<?php else:?>
							Not Found
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
					<aside class="columns w-322">
					<div class="widget">
						<?php get_template_part("widget/sidebar-pengajuan-klaim");?>
					 </div>
					 <div class="widget">
						<div id="" class="h-215 bg-grey clearfix radius-all-5">
						<div class="box  p-all-30 clearfix">
							<h3 class="c-blue f-16 uppercase">Ulasan Pasar Harian</h3>
							<form id="pasar-harian">
						        	<select id="pilih-bulan-1">
										<option>Pilih Bulan </option>
										<option value="fmonth=1"> Januari </option>
										<option value="fmonth=2"> Februari </option>
										<option value="fmonth=3"> Maret </option>
										<option value="fmonth=4"> April </option>
										<option value="fmonth=5"> Mei </option>
										<option value="fmonth=6"> Juni </option>
										<option value="fmonth=7"> Juli </option>
										<option value="fmonth=8"> Agustus </option>
										<option value="fmonth=9"> September </option>
										<option value="fmonth=10"> Oktober </option>
										<option value="fmonth=11"> November </option>
										<option value="fmonth=12"> Desember </option>
									</select>

									<?php 		
										$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'ulasan_pasar_harian' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
										$results = $wpdb->get_results($query);
										$currentYear = $results[0]->year;
										?>			
											
										<select id="pilih-tahun-1">
											<option>Pilih Tahun</option>
										<?php
										foreach($results as $result){
										$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
										?>		
												<option value="<?php echo get_post_type_archive_link( 'media' ).'&fyear='.$result->year; ?>"><?php echo $result->year; ?></option>
										<?php
													}
										?>		
									</select>
								<button type="submit" class="button red right">Cari</button>
							</form>
						</div>
						</div>
					</div>
					</aside>	

				</div>
			</div>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div>
</div>
<?php get_footer();?>