<?php/** * Template Name: Direktori Search*/?>
<?php 
	global $keyword, $t;
	$keyword = ($_GET['q']) ? $_GET['q'] : '';
	$type = ($_GET['t'] == 'rumah_sakit' || $_GET['t'] == 'bengkel') ? array($_GET['t']) : array('all') ;
	$t = ($_GET['t'] == 'rumah_sakit' || $_GET['t'] == 'bengkel') ? $_GET['t'] : '' ;
	$tax = ($_GET['tax']) ? $_GET['tax'] : '';
	$wilayah = ($_GET['wilayah']) ? $_GET['wilayah'] : '';
	$providers = ($_GET['providers']) ? $_GET['providers'] : '-1';
	$nama_produk = ($_GET['nama_produk']) ? $_GET['nama_produk'] : '-1';
	$x =($_GET['x']) ? $_GET['x'] :'';
	$y =($_GET['y']) ? $_GET['y'] :'';
?>	

<?php get_header();?>

<div id="mapContainer" class="desktop-content"><div id="mapContent" class="h-650"></div></div>
<div id="mobile-map" class="mobile-content"></div>
	<div id="wrapper" class="row">
		<div id="searchdir" class="p-lr-30 p-tb-15 clearfix block relative bottom-10 bg-white-70p radius-all-5">
			<form id="caridirektori" class="m-bottom-0" name="cari-direktori" action="<?=site_url('direktori-search')?>" method="get">
				<input type="text" name="q" class="inputdir left m-bottom-0" placeholder="Nama Rumah Sakit, Bengkel, Kantor Cabang, Kota, Wilayah" value="<?php echo $keyword; ?>"/>				
				<input type="hidden" name="tax" value="<?php echo $tax;?>">
				<!-- <input type="hidden" name="entity" value="<?php echo $tax;?>"> -->
				<!-- <input type="hidden" name="providers" value="<?php echo $providers;?>"> -->
				<!-- <input type="hidden" name="t" value="<?php echo $type[0];?>"> -->
				<button type="submit" class="button red btn-dir right m-top-3 m-bottom-0">Cari</button>
				<?php if($type[0] == 'rumah_sakit'): ?>
					<ul id="selectEntity" class="small-block-grid-1 medium-block-grid-2 large-block-grid-4">
						<li><input type="checkbox" name="entity" id="afi" value="solusi-kesehatan"><label for="afi">Solusi Kesehatan</label></li>
						<li><input type="checkbox" name="entity" id="ali" value="solusi-proteksi"><label for="ali">Solusi Proteksi</label></li>
						<li><input type="checkbox" name="entity" id="agi" value="solusi-umum"><label for="agi">Solusi Umum</label></li>
						<!-- <li><input type="checkbox" name="t" id="afi" value="axa-financial-indonesia"><label for="afi">AXA Financial Indonesia</label></li> -->
						<!-- <li><input type="checkbox" name="t" id="ali" value="axa-life-indonesia"><label for="ali">AXA Life Indonesia</label></li> -->
						<!-- <li><input type="checkbox" name="t" id="agi" value="axa-general-insurence" <?php //if($tax == 'axa-general-insurence') { echo 'checked'; } ?>><label for="agi">AXA General Insurance</label></li> -->
						<!-- <li><input type="checkbox" name="t" id="ami" value="axa-asset-management-indonesia"><label for="ami">AXA Asset Management Indonesia</label></li> -->
					</ul>
					<div id="advanced-search" <?php //if($tax != 'axa-general-insurence') { echo 'style="display:none;"'; } ?>>
						<?php wp_dropdown_categories(array('taxonomy' => 'providers','hide_empty'=>0,'show_option_none' => 'Pilih provider','name'=>'providers')); ?>
						<?php 
							$providers2 = get_categories( array('taxonomy'=>'providers') );
							$providers_string = array();
				        	foreach ($providers2 as $provider) {
				        		$providers_string[] = $provider->slug;
				        	}
				        	$providers_strings = join(",",$providers_string);
						?>
						<?php $my_query = new WP_Query('post_type=product_matrix&providers='.$providers_strings.'&post_per_page=-1'); ?>  
						<select name="nama_produk" id="nama_produk" class="postform">
								<option value="-1">Pilih produk</option>
						    <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
						    	<option value="<?php the_id(); ?>"><?php the_title(); ?></option>
						    <?php endwhile;?>
						    <?php wp_reset_query(); ?>
						</select>  	
					</div>			
				<?php endif; ?>

			</form>
		</div>

		<section class="bg-white clearfix relative radius-all-5">
			<div class="mobile-content p-lr-15 p-tb-15">
				<h3 class="f-16 c-blue m-bottom-10">Pilih jasa bantuan yang Anda butuhkan</h3>
				<select id="dir-menu">
					<option value="<?php echo site_url('direktori');?>"  selected="selected">Seluruh Direktori</option>
					<option value="<?php echo site_url('direktori/rumah-sakit');?>" >Rumah Sakit</option>
					<option value="<?php echo site_url('direktori/bengkel');?>">Bengkel</option>
				</select>
			</div>

			<ul id="chooseDir" class="clearfix p-all-30 small-block-grid-1 medium-block-grid-1 large-block-grid-3 desktop-content">
				<li class="clearfix semua <?php if($type[0] == 'all') echo "selected"; ?>">
					<a href="<?php echo site_url('direktori');?>?x=<?php echo $x."&";?>y=<?php echo $y;?>">
						<span class="icon left"></span>
						<div class="details right">
							<strong class="f-20 block">Seluruh Direktori</strong>
							<p class="f-12">Temukan Rumah Sakit dan Bengkel AXA</p>
						</div>
					</a>
				</li>
				<li class="clearfix rumah-sakit <?php if($type[0] == 'rumah_sakit') echo "selected"; ?>">
					<a href="<?php echo site_url('direktori/rumah-sakit');?>?t=rumah_sakit&x=<?php echo $x."&";?>y=<?php echo $y;?>">
						<span class="icon left"></span>
						<div class="details right">
							<strong class="f-20 block">Rumah Sakit</strong>
							<p class="f-12">Rumah Sakit &amp; Klinik Rekanan AXA</p>
						</div>
					</a>
				</li>
				<li class="clearfix bengkel <?php if($type[0] == 'bengkel') echo "selected"; ?>">
					<a href="<?php echo site_url('direktori/bengkel');?>?t=bengkel&x=<?php echo $x."&";?>y=<?php echo $y;?>">
						<span class="icon left"></span>
						<div class="details right">
							<strong class="f-20 block">Bengkel</strong>
							<p class="f-12">Bengkel mobil rekanan AXA untuk pemegang polis AXA General</p>
						</div>
					</a>
				</li>
			</ul>
		</section>
		<section id="maincontent">
			<div id="direktori-wrapper" class="large-8 columns clearfix">
				<?php 
					$i = 0;
					$args = array();
					$new_array_post_id = array();
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
					if($wilayah != '' && $keyword == ''){ //from widget
						$args = array("post_type" => ($type[0] != 'all') ? $type : array("rumah_sakit","bengkel"),
								  "posts_per_page" =>10, 
								  'orderby' => 'title', 
								  'order' => ASC, 
								  'paged' => $paged,
								  'direktori_wilayah' => $wilayah
							);
					}elseif($wilayah != '' && $keyword != ''){ 
						$args = array("post_type" => ($type[0] != 'all') ? $type : array("rumah_sakit","bengkel"),
								  "posts_per_page" =>10, 
								  'orderby' => 'title', 
								  'order' => ASC, 
								  'paged' => $paged,
								  's' => $keyword,
								  'direktori_wilayah' => $wilayah
							);
					}elseif($tax != '' && $keyword == ''){ //search produk
						$args = array("post_type" => ($type[0] != 'all') ? $type : array("rumah_sakit","bengkel"),
								  "posts_per_page" =>10, 
								  'orderby' => 'title', 
								  'order' => ASC, 
								  'paged' => $paged,
								  'solusi' => $tax
							);
					}elseif($tax != '' && $keyword != ''){ //advance search
						$args = array("post_type" => ($type[0] != 'all') ? $type : array("rumah_sakit","bengkel"),
								  "posts_per_page" =>10, 
								  'orderby' => 'title', 
								  'order' => ASC, 
								  'paged' => $paged,
								  's' => $keyword,
								  'solusi' => $tax
							); 
					}else{ // default
						$args = array("post_type" => ($type[0] != 'all') ? $type : array("rumah_sakit","bengkel"),
									  "posts_per_page" =>10, 
									  //"providers"=>"gesa-excess-dibayar-di-tempat-oleh-nasabah",
									  // 'meta_query' => array(
											// 				array(
											// 					'key' => 'rs_alamat', // name of custom field
											// 					'value' => '"'.$keyword.'"', // matches exaclty "123", not just 123. This prevents a match for "1234"
											// 					'compare' => 'LIKE'
											// 				)
											// 			),
									  'orderby' => 'title', 
									  'order' => ASC, 
									  'paged' => $paged,
									  's' => $keyword
								);
					}

					if ($x !='' && $y !='') {
						$post_id_and_distance=$wpdb->get_results("SELECT post_id, post_title, ( 6371 * ACOS( COS( RADIANS($x))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS($y))+SIN(RADIANS($x))*SIN(RADIANS(latitude)))) AS distance
																						FROM  `directory_coordinates` 
																						ORDER BY  `distance` ASC");	
						$post_id_and_distance = array_splice($post_id_and_distance, 0);	
						
						foreach($post_id_and_distance as $row => $value)
						{
							$new_array_post_id[] = intval($value->post_id);
							// $args = array(
							//    'post_type' 		=> array('rumah_sakit','bengkel'),
							//    'post__in'      	=> $new_array_post_id,
							//    'posts_per_page' => 10,
							//    'orderby' 		=> 'post__in',
							//    'order'			=> ASC,
							//    'paged'			=> $paged
							// );
						}

						$args['post__in'] = $new_array_post_id;
						$args['orderby'] = 'post__in';
						$args['paged']=$paged;
						$args['order']=ASC;
					}

					if($providers != '-1') {
						$args["tax_query"] = array(
													array(
														'taxonomy' => 'providers',
														'terms' => $providers
													)
												);
					}
					if($nama_produk != '-1') {
						$args["meta_query"] = array(
													array(
														'key' => 'nama_produk', // name of custom field
														'value' => '"'.$nama_produk.'"', // matches exaclty "123", not just 123. This prevents a match for "1234"
														'compare' => 'LIKE'
													)
												);
					}
					

					// $args2 = array(
					// 				'post_type' => (isset($_GET['t']) ? $_GET['t'] : array('rumah_sakit','bengkel') ),
					// 				'meta_query' => array(
					// 						array(
					// 							'key' => 'rs_alamat',
					// 							'value' => $keyword,
					// 							'compare' => 'LIKE'
					// 						)
					// 				)
					// 			);

					// $args3 = array(
					// 				'post_type' => (isset($_GET['t']) ? $_GET['t'] : array('rumah_sakit','bengkel') ),
					// 				'direktori_wilayah' => $keyword
					// 			);

					// $myquery1 = new WP_Query( $args );
					// $myquery2 = new WP_Query( $args2 );
					// $myquery3 = new WP_Query( $args3 );
					add_filter('posts_join', 'directory_search_join' );
					add_filter('posts_where', 'directory_search_where' );
					$myquery = new WP_Query($args);
					//echo $myquery->request;
					remove_filter('posts_join', 'directory_search_join' );
					remove_filter('posts_where', 'directory_search_where' );
					// $myquery->posts = array_merge($myquery1->posts,$myquery2->posts,$myquery3->posts);
					// $myquery->post_count = $myquery1->post_count + $myquery2->post_count + $myquery3->post_count;


					if($myquery->have_posts()): 
				?>
				<div id="geodata"></div>
				<div id="head-direktori" class="clearfix p-lr-30 p-tb-15 direktori-list ">
					<div style="display:none">
						<span id="lat"><?php echo $_GET['x'] ?></span>
						<span id="long"><?php echo $_GET['y'] ?></span>
					</div>
					<h1 class="c-blue m-bottom-0 uppercase f-24"><?php the_title();?></h1>
					<p class="m-all-0">*Rekanan AXA berlaku bagi pemilik produk asuransi kesehatan AXA.</p>
				</div>
				<ul id="main-direktori" class="m-all-0 list-style-none bg-greylight radius-all-5 o-hidden">

					<?php
						while($myquery->have_posts()):$myquery->the_post();

						$field = get_field_object('jenis_rs');
						$value = get_field('jenis_rs');
						$label = $field['choices'][ $value ];
						$locations = get_field('rs_map');
						$coords = explode(",", $location['coordinates']);
						$terms = get_the_terms($post->id, 'solusi');
						$location = explode(',', $locations['coordinates']);
					?>
					<li class="clearfix direktori-list p-lr-30 p-tb-15 c-grey">
						<div class="details left">
							<?php if ($post->post_type == "rumah_sakit"):?>
							<strong class="block f-18 c-blue"><?php the_title();?>, <?php echo $label;?></strong>
							<?php else:?>
							<strong class="block f-18 c-blue"><?php the_title();?></strong>
							<?php endif;?>
							<span><i class="fa fa-map-marker"></i> <?php the_field('rs_alamat');?></span><br/>

							<?php if(get_field('rs_telepon')):?><span>
								<i class="fa fa-phone"></i> <?php the_field('rs_telepon');?> 
							<?php endif;?>
							<?php if(get_field('rs_fax')):?><span>
								&nbsp;&nbsp; <i class="fa fa-print"></i> <?php the_field('rs_fax');?></span>
							<?php endif;?>
							<br/>
							<?php foreach ($terms as $term) {?>
								<span class="tag f-12 p-all-5 bg-white radius-all-5 c-grey"><?php echo $term->name; ?></span>
							<?php } ?>									
							
						</div>

						<div class="map-details text-right right ">
							<a class="c-red f-14 view-map" href="#" data-id-infowindow="<?php echo $i + (($paged-1) * 10); ?>" data-location="<?php echo $locations['coordinates']; ?>">Lihat peta</a><br/>
							<span class="nearby f-14 distance"></span><br/>
							<a target="_blank" href="https://www.google.com/maps/dir//''/@<?=$locations['coordinates']?>,15z/data=!4m6!4m5!1m0!1m3!2m2!1d<?=$location[1]?>!2d<?=$location[0]?>" class="c-red f-14">Get direction >></a>
						</div>
					</li>
						<?php $i++; endwhile;?>
						<li class="direktori-list text-center clearfix block p-tb-20"> <?php wp_pagenavi( array( 'query' => $myquery ) );
; ?></li>
						<?php  wp_reset_postdata(); ?>
				<?php
					else:
				?>
				<div id="geodata"></div>
				<div id="head-direktori" class="clearfix p-lr-30 p-tb-15 direktori-list ">
					<div style="display:none">
						<span id="lat"><?php echo $_GET['x'] ?></span>
						<span id="long"><?php echo $_GET['y'] ?></span>
					</div>
					<h3 class="c-blue m-bottom-0 uppercase f-24">Data tidak dapat ditemukan</h3>
					<!-- <p class="m-all-0">*Rekanan AXA berlaku bagi pemilik produk asuransi kesehatan AXA.</p> -->
				</div>
				<ul id="main-direktori" class="m-all-0 list-style-none bg-greylight radius-all-5 o-hidden">
				<?php
					endif;
				?>
				</ul>
			</div>
			<?php wp_reset_query(); ?>
			<aside class="columns w-322 desktop-content">
				<div class="widget"><?php get_template_part("widget/footer-banner-left");?></div>
				<div class="widget"><?php get_template_part("widget/footer-banner-other");?></div>
				<div class="widget"><?php get_template_part("widget/footer-banner-right");?></div>
			</aside>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div>
<?php get_template_part("widget/hargaunit");?>

<script type="text/javascript">
		var markers = [];
		var center = new google.maps.LatLng("<?php if(isset($coord)){ echo $coord[0]; }else{ echo "-5.3107012"; }?>", "<?php if(isset($coord)){ echo $coord[1]; }else{ echo "119.8605666"; } ?>");
		var map = new google.maps.Map(document.getElementById('mapContent'), {
          zoom: <?php if(isset($coord)){ echo "15"; }else{ echo "5"; } ?>,
          center: center,
         scrollwheel: false
        });
		var infowindowArray = [];
		function addInfoWindow(marker, message) {
            var info = message;

            var infoWindow = new google.maps.InfoWindow({
                content: message
            });

            google.maps.event.addListener(marker, 'click', function () {
				map.setCenter(marker.getPosition());
				map.panBy(0, -100);
				for (var i = 0; i < infowindowArray.length; i++)
					infowindowArray[i].close();
				infoWindow.open(map, marker);
            });
			infowindowArray.push(infoWindow);
        }
		function initialize() {
			jQuery.getJSON('<?php echo site_url();?>/?page_id=2377&q=<?=$keyword?>&t=<?=$type[0]?>&tax=<?=$tax?>&wilayah=<?=$wilayah?>&providers=<?=$providers?>&nama_produk=<?=$nama_produk?>&x=<?=$x?>&y=<?=$y?>' ,function(data){
				if(data){
					for (var i = 0; i < data.length; i++) {
						var dataPhoto = data[i];
						var iconpng = data[i].options.iconpng;
						var latLng = new google.maps.LatLng(dataPhoto["latLng"][0],dataPhoto["latLng"][1]);

						var marker = new google.maps.Marker({
							position: latLng,
							icon: iconpng					
						});
						addInfoWindow(marker, dataPhoto["data"]);
						markers.push(marker);
					}
					var mcOptions = {maxZoom: 15};
					var markerCluster = new MarkerClusterer(map, markers, mcOptions);
				}
			});
		}
      	google.maps.event.addDomListener(window, 'load', initialize);

      		$(document).ready(function(){
			$('input[name=entity]').change(function(){
      			var tax = $('#tax').val();
      			if(this.checked){
      				if(tax == ''){
      					tax = this.value;
      				}
      				else{
      					tax = tax + ',' + this.value;	
      				}
      				$('#tax').val(tax);
      			}
      		});
      		// $('#uniform-agi').click(function(){
      		// 	if($('span',this).hasClass('checked')) {
      		// 		$('input[name=t]').value('rumah_sakit')
      		// 	}else {

      		// 	}
      		// });
      		//refill select box is value is present
      		$('#providers option[value=<?php echo $providers;?>]').attr('selected','selected');
      		$('#nama_produk option[value=<?php echo $nama_produk;?>]').attr('selected','selected');

			//console.log(latLang);
			$(".view-map").click(function(){
				var latLang = $(this).attr("data-location");
				var idinfowindow = $(this).attr("data-id-infowindow");
	       		var latlon_array = latLang.split(','); 
    	  		var lat = latlon_array[0]; 
      			var lon = latlon_array[1]; 
      			var b = new google.maps.LatLng(lat,lon);

      			 $('html, body').animate({
			          scrollTop: $("#mapContainer").offset().top
			      }, 1000);

				map.setCenter(b);
				google.maps.event.trigger(markers[idinfowindow], 'click');
				map.setZoom(16);
				
			});
		});

      		$(document).ready(function() {
			    //$("#direktori-wrapper").geolocator({ distanceBigStr: 'km', distanceSmallStr: 'm',debugMode: true, sorting: false, enableHighAccuracy: true });
				var currentLat;
				var currentLong;


		     function initGeolocation()
		     {
		        if( navigator.geolocation )
		        {
		           // Call getCurrentPosition with success and failure callbacks
		           navigator.geolocation.getCurrentPosition( success, fail );
		        }
		        else
		        {
		           alert("Sorry, your browser does not support geolocation services.");
		        }
		     }

		     function success(position)
		     {

		         $('#long').text(position.coords.longitude);
		         $('#lat').text(position.coords.latitude);
		         <?php if($_GET["x"] == null && $_GET["y"] == null): ?>
			         thisUrl="<?php echo site_url('direktori-search');?>"
			         window.location.href=thisUrl+"?x="+position.coords.latitude+"&y="+position.coords.longitude+"&q=<?=$keyword?>&t=<?=$type[0]?>&tax=<?=$tax?>&wilayah=<?=$wilayah?>&providers=<?=$providers?>&nama_produk=<?=$nama_produk?>";
		         <?php endif; ?>
		     }

		     function fail()
		     {
		        // Could not obtain location
		     }
				
		     initGeolocation();

		     
			});



		function distance(lat1,lon1,lat2,lon2) {
       var R = 6371; // km (change this constant to get miles)
       var dLat = (lat2-lat1) * Math.PI / 180;
       var dLon = (lon2-lon1) * Math.PI / 180;
       var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
               Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
               Math.sin(dLon/2) * Math.sin(dLon/2);
       var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
       var d = R * c;
       if (d>1) return Math.round(d)+"km";
       else if (d<=1) return Math.round(d*1000)+"m";
       return d;
}


$(window).load(function(){

	currentLat = $('#lat').html();
	currentLong = $('#long').html();
	//only calculate distance when found 
	if(currentLat != "" && currentLong !="") {
		$('.distance').each(function(){		
			var latLang = $(this).prev().prev().attr("data-location");
			var idinfowindow = $(this).attr("data-id-infowindow");
				var latlon_array = latLang.split(','); 
				var lat = latlon_array[0]; 
				var lon = latlon_array[1]; 
			$(this).text(distance(currentLat,currentLong,lat,lon));
			$(this).parent().prev().find(".mobile-distance").text(" ("+distance(currentLat,currentLong,lat,lon)+") ");
		});	
	}else{
		$('.distance').each(function(){		
			$(this).text("");
		});
	}	

});

</script>

<?php #var_dump($post);?>
<?php get_footer();?>