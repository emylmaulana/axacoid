<?php/** * Template Name: Layanan Nasabah: Lain-lain */?>
<?php get_header();?>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
			<h1>Layanan Nasabah</h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>

		<section id="premi" class="clearfix sections grey">
			<h3 class="m-bottom-45">Lain-lain</h3>
			<div id="page-half">
			<div class="large-8 columns clearfix p-left-0">
				<div class=" h-300 bg-white clearfix relative">
					<div class="box  p-all-20 clearfix">
						<h3 class="c-blue f-16 uppercase">AXA FINANCIAL AIRPORT LOUNGE PROGRAM</h3>
						<p>AXA Financial meluncurkan layanan terbaru yang merupakan terobosan baru di industri asuransi asuransi Indonesia, yaitu AXA Financial Airport Lounge. AXA Financial Airport Lounge tersedia khusus bagi Nasabah VIP AXA Financial yang memberikan akses tanpa batas secara cuma-cuma di exclusive lounge yang tersedia di bandara kota-kota utama di Indonesia.</p>
						<a href="<?php echo site_url('hubungi-kami');?>" class="right button red absolute bottom-10 right-10">Hubungi</a>
					</div>
				</div>
			</div><!--end large 8-->
			<aside class="columns w-322 h-300 o-hidden desktop-content">
				<?php get_template_part("widget/sidebar-axa-akses");?>
			</aside>

			<div class="mobile-content">
				<?php get_template_part('widget/sidebar-axa-akses-mobile');?>
			</div>
			</div>
		</section>

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>