<?php/** * Template Name: Layanan Nasabah: Informasi Umum */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570021&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php echo bloginfo('template_url');?>/images/head-layanan.jpg);"></div>
		<div class="content large-4">
		<h1><?php _e("<!--:en-->Customer Service<!--:--><!--:id-->Layanan Nasabah<!--:-->"); ?></h1>
		<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php _e("<!--:en-->Services to customers around the Indonesian AXA insurance products, policy, premium payments, claims, and others.<!--:--><!--:id-->Layanan untuk nasabah AXA Indonesia seputar produk asuransi, polis, pembayaran premi, klaim, dan lainnya.<!--:-->"); ?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<?php get_template_part("widget/search-premi");?>
		<?php get_template_part("widget/layanan-submenu");?>

		<section id="premi" class="clearfix sections grey">
			<h3 class="m-bottom-45"><?php _e("<!--:en-->General Information!--:--><!--:id-->Informasi Umum<!--:-->"); ?></h3>
			<div id="page-half">
			<div class="large-8 columns clearfix">
				<div class="large-6 columns h-300 bg-white clearfix w-48p relative">
					<div class="box  p-all-15 clearfix">
						<h3 class="c-blue f-16 uppercase"><?php _e("<!--:en-->Introduction to Insurance!--:--><!--:id-->PERKENALAN ASURANSI<!--:-->"); ?></h3>
						<p><?php _e("<!--:en-->AXA Indonesia provides information about insurance that you know insurance topics to help you in all your self-protection process.<!--:-->
							<!--:id-->AXA Indonesia menyediakan informasi seputar asuransi agar Anda mengenal topik-topik  asuransi untuk membantu Anda dalam segala proses perlindungan diri Anda.<!--:-->"); ?></p>
						<a href="<?php echo site_url('layanan-nasabah/informasi-umum/perkenalan-asuransi/');?>" class="right button red absolute bottom-10 right-10"><?php _e("<!--:en-->Introduction to Insurance<!--:--><!--:id-->Perkenalan Asuransi<!--:-->"); ?></a>
					</div>
				</div>

				<div id="istilah-asuransi" class="large-6 columns h-300 bg-white clearfix  w-48p relative">
					<div class="box  p-all-15 clearfix">
						<h3 class="c-blue f-16 uppercase"><?php _e("<!--:en-->FUND TRANSFER<!--:--><!--:id-->KAMUS ISTILAH ASURANSI'<!--:-->"); ?></h3>
						<p><?php _e("<!--:en-->Understand more deeply the meaning of terms that are often used in insurance to help you plan your self-protection.<!--:-->
									<!--:id-->Pahami lebih dalam arti istilah-istilah yang sering digunakan dalam asuransi untuk memudahkan Anda merencanakan perlindungan diri Anda.<!--:-->"); ?></p>
						<a href="<?php echo site_url('layanan-nasabah/informasi-umum/istilah-asuransi/');?>" class="right button red absolute bottom-10 right-10"><?php _e("<!--:en-->Insurance Terms<!--:--><!--:id-->Istilah Asuransi<!--:-->"); ?></a>
					</div>
				</div>
			</div><!--end large 8-->
			<aside class="columns w-322 h-300 o-hidden desktop-content">
				<?php get_template_part("widget/sidebar-axa-akses");?>
			</aside>

			<div class="mobile-content">
				<?php get_template_part('widget/sidebar-axa-akses-mobile');?>
			</div>
		</section>
		</div>

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<?php get_footer();?>