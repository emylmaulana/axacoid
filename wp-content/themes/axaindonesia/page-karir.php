<?php/** * Template Name: Karir */?>
<?php get_header();?>

<?php
	$fail=$_GET['fail'];
	$succ=$_GET['succ'];
	//session_start();
	$cap = 'notEq';
	//session_start();
	$ranStr = md5(microtime());
	$ranStr = substr($ranStr, 0, 6);
	
	// $this->session->set_userdata('cap_code', $ranStr);
	$_SESSION['cap_code']=$ranStr;
	$newImage = imagecreatefromjpeg(get_bloginfo('template_url').'/images/cap_bg.jpg');
	$txtColor = imagecolorallocate($newImage, 0, 0, 0);
	imagestring($newImage, 5, 5, 5, $ranStr, $txtColor);
	//header("Content-type: image/jpeg");
	// imagejpeg($newImage, get_bloginfo('template_url').'/images/captcha.jpg');
	//imagejpeg($newImage, '/Applications/XAMPP/xamppfiles/htdocs/axaone/wp-content/themes/axaindonesia/images/captcha.jpg');
	imagejpeg($newImage, get_theme_root().'/axaindonesia/images/captcha.jpg');
	
?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570030&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php the_field('header_image');?>);"></div>
		<div class="content large-6">
			<h1><?php the_title(); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
			<ul id="separate" class="personal relative" style="top:0;">
				<?php $slug = basename(get_permalink());?>
				<li class="selected"><a href="<?php echo site_url('/') .$slug;?>">Corporate</a></li>
				<li><a href="<?php echo site_url('/') . $slug;?>-agent">Agent</a></li>
			</ul>
		</div><!--end large 6-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->
	<div id="wrapper" class="row">
		<section class="bg-white clearfix radius-top-5">
			<div class="sections">
				<h3 class="f-24"><?php _e("<!--:en-->Why Join the AXA?<!--:--><!--:id-->Mengapa Bergabung dengan AXA?<!--:-->"); ?></h3>
				<?php if( have_posts() ) : the_post(); ?>
					<div class="column-2 f-16">
						 <?php the_content();?>
					 </div>
				<?php endif; wp_reset_postdata();?>
			</div>
		</section>
		<section class="bg-karir clearfix bg-bluedark">
			<div class="sections p-all-60">
				<h3 class="f-24 c-white"><?php _e("<!--:en-->Career With AXA Indonesia<!--:--><!--:id-->Berkarir Bersama AXA Indonesia<!--:-->"); ?></h3>
				<p class="c-white f-16"><?php _e("<!--:en-->Dynamic workplace to help optimize your potential in developing a career<!--:--><!--:id-->Tempat kerja yang dinamis untuk membantu mengoptimalkan potensi Anda dalam mengembangkan karir<!--:-->"); ?></p>
				<ul class="clearfix small-block-grid-1 medium-block-grid-2 large-block-grid-4">
					<?php $count = 1; while(has_sub_field('karir_group')): ?>
						<li class="text-center h-220 <?=$count%2 == 0? 'bg-blue-2' : ''?>">
							<div class="p-all-15 block">
								<img src="<?php the_sub_field('image');?>" alt="karir-group"/>
								<strong class="block c-white m-top-15 f-14"><?php the_sub_field('body');?></strong>
							</div>
						</li>
					<?php $count++; endwhile;?>
				</ul>
			</div>
		</section>
		<section class="bg-bluelight2 clearfix">
			<div class="sections p-all-60">
				<h3 class="f-24 c-blue"><?php _e("<!--:en-->Career choices<!--:--><!--:id-->Pilihan Karir<!--:-->"); ?></h3>
				<p class="f-16"><?php _e("<!--:en-->Find a career position that suits your interests and skills<!--:--><!--:id-->Cari posisi karir yang sesuai dengan minat dan keterampilan Anda<!--:-->"); ?></p>
				<ul class="karir-listing clearfix small-block-grid-1 medium-block-grid-2 large-block-grid-4">
					<!-- <li class="semua"><a href="<?php echo site_url('semua-posisi'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->All Position<!--:--><!--:id-->Semua posisi<!--:-->"); ?></strong></a></li>
					<li class="aktuaria"><a href="<?php echo site_url('aktuaria'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->AKTUARIA<!--:--><!--:id-->AKTUARIA<!--:-->"); ?></strong></a></li>
					<li class="keuangan"><a href="<?php echo site_url('perbankan-keuangan'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->BANKING & FINANCE<!--:--><!--:id-->PERBANKAN & KEUANGAN<!--:-->"); ?></strong></a></li>
					<li class="humanresources"><a href="<?php echo site_url('semua-posisi'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->HUMAN RESOURCES<!--:--><!--:id-->HUMAN RESOURCES<!--:-->"); ?></strong></a></li>
					<li class="media"><a href="<?php echo site_url('semua-posisi'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->MEDIA<!--:--><!--:id-->MEDIA<!--:-->"); ?></strong></a></li>
					<li class="surveying"><a href="<?php echo site_url('quantity-surveying'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->QUANTITY SURVEYING<!--:--><!--:id-->QUANTITY SURVEYING<!--:-->"); ?></strong></a></li>
					<li class="bisnisdev"><a href="<?php echo site_url('semua-posisi'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->MARKETING / business development<!--:--><!--:id-->MARKETING / business development<!--:-->"); ?></strong></a></li>
					<li class="sales"><a href="<?php echo site_url('sales'); ?>" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->Sales<!--:--><!--:id-->Sales<!--:-->"); ?></strong></a></li> -->
					<li class="semua"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->All Position<!--:--><!--:id-->Semua posisi<!--:-->"); ?></strong></a></li>
					<li class="aktuaria"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->AKTUARIA<!--:--><!--:id-->AKTUARIA<!--:-->"); ?></strong></a></li>
					<li class="keuangan"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->BANKING & FINANCE<!--:--><!--:id-->PERBANKAN & KEUANGAN<!--:-->"); ?></strong></a></li>
					<li class="humanresources"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->HUMAN RESOURCES<!--:--><!--:id-->HUMAN RESOURCES<!--:-->"); ?></strong></a></li>
					<li class="media"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->MEDIA<!--:--><!--:id-->MEDIA<!--:-->"); ?></strong></a></li>
					<li class="surveying"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->QUANTITY SURVEYING<!--:--><!--:id-->QUANTITY SURVEYING<!--:-->"); ?></strong></a></li>
					<li class="bisnisdev"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->MARKETING / business development<!--:--><!--:id-->MARKETING / business development<!--:-->"); ?></strong></a></li>
					<li class="sales"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target="_blank" class="block bg-white h-70 p-all-20"><span class="icon"></span><strong class="uppercase f-13"><?php _e("<!--:en-->Sales<!--:--><!--:id-->Sales<!--:-->"); ?></strong></a></li>
				</ul>
			</div>
		</section>
		<section id="kontak" class="sections grey clearfix o-hidden">
			<div class="transparent-separate absolute" style="left:0;top:0;"><img src="<?php bloginfo('template_url'); ?>/images/separate-transparent.png" alt="sparate-transparent"/></div>
			<h3 class="fw-normal f-17 uppercase m-bottom-0 m-top-45">Kontak</h3>
			
			<div class="large-4 columns p-left-0">
				<h4 class="c-blue">Kirim CV Anda </h4>
				<p class="f-16">kunjungi halaman AXA E-Recruitment untuk mengetahui informasi terkini tentang lowongan pekerjaan di AXA Indonesia, atau langsung upload CV Anda.</p>
			</div>
			<div class="large-7 columns contact-form">
				
				<!-- form -->
				<form action="<?=site_url()?>/axaone_form/karir/submit_data" method="post" id="form" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
					<div style="display: none;">
						<input type="hidden" name="_wpcf7" value="2058">
						<input type="hidden" name="_wpcf7_version" value="3.6">
						<input type="hidden" name="_wpcf7_locale" value="id_ID">
						<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2058-p3263-o1">
						<input type="hidden" name="_wpnonce" value="dee286a3e7">
						<input type="hidden" name="type" value="Corporate"/>
						<input type="hidden" name="banner_source" value="">
						<input type="hidden" name="utm_source" value="">
						<input type="hidden" name="utm_medium" value="">
						<input type="hidden" name="utm_term" value="">
						<input type="hidden" name="utm_content" value="">
						<input type="hidden" name="utm_campaign" value="">
						<input type="hidden" name="gclid" value="">

					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap nama">
							<input type="text" name="nama_lengkap" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Lengkap">
						</span>
					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap notelp">
							<input type="text" name="no_tlp" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nomor Telepon">
						</span>
					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap email">
							<input type="email" name="email" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email">
						</span>
					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap tanggallahir">
							<input type="text" name="tgl_lahir" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required datepicker " aria-required="true" aria-invalid="false" placeholder="Tanggal Lahir" id="dp1392899738518">
						</span>
					</div>
					<!-- <div class="fieldset">
						<span class="wpcf7-form-control-wrap referral">
							<input type="text" name="no_referral" value="" size="40" class="required wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="No. Referral">
						</span>
					</div> -->
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap upload-cv">
							<!-- <div class="NFI-wrapper wpcf7-form-control wpcf7-file" id="NFI-wrapper-13928997386852315" style="overflow: auto; display: inline-block;">
								<div class="NFI-button NFI13928997386852315 button red right" style="overflow: hidden; position: relative; display: block; float: left; white-space: nowrap; text-align: center;">
									Browse<input type="file" name="file_name" value="1" size="40" class="required wpcf7-form-control wpcf7-file NFI-current" aria-invalid="false" data-styled="true" style="opacity: 0; position: absolute; border: none; margin: 0px; padding: 0px; top: 0px; right: 0px; cursor: pointer; height: 60px;">
								</div>
							<input type="text"  readonly="readonly" class="NFI-filename NFI13928997386852315" placeholder="Upload CV" style="display: block; float: left; margin: 0px; padding: 0px 5px; width: 182px;">
						</div> -->
					<input type="file" name="file_name" class="required" accept="pdf/*|msword/*|MIME_type" >
					</span>
					</div>
					<div class="fieldset">
					
						<input type="submit" value="Kirim" class="wpcf7-form-control wpcf7-submit button red">
					</div>
					
						<!-- <div class="fieldset">
							<span class="left m-top-10 f-13 block">atau</span>
							<a href="https://www.linkedin.com/company/pt.-axa-services-indonesia" class="block left btn-linkedin"></a>
						</div> -->
						
					<div class="wpcf7-response-output wpcf7-display-none"></div>
				<p class="cap_status" style="display:none;"><?php echo $fail; ?></p>
				<p class="cap_sukses" style="display:none;"><?php echo $succ; ?></p>
				</form>
				<!-- form -->

				
			</div>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div>
</div>

<script type="text/javascript">
			// var capch = '<?php echo ($_SESSION['cap_code']); ?>' ;
			// if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// 	$('#img-captcha').hide();
			// 	$('#captcha').attr('type', 'hidden');
			// 	$('#captcha').val(capch);
			// }
			$(document).ready(function(){
		 		//captcha check
				$("#form").submit(function(){
				
				    if($("#form").valid()) {
				  //   	$('input[type=file]').fileValidator({
						//   onInvalid:    function(type, file){ $(this).val(null); },
						//   type:        'image'
						// });

	                    if(capch == $('#captcha').val()){
	                    	console.log('test');
	                    	return true;

	                    }
	                    else
	                    {
	                        $('.cap_status').html("Captcha yang Anda masukkan salah!").addClass('cap_status_error').fadeIn('slow');
	                    }


				    }else{
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    }

				    return false;
				 });
				var thisURL = '<?php echo site_url("karir/?fail=File+harus+berformat+doc,+docx,+dan+pdf,+Mohon+upload+ulang"); ?>';					
				if (window.location.href == thisURL ) {
						$('.cap_status').show();
					}
				
				var thisURLSuccess = '<?php echo site_url("karir/?succ=Data%20Tersimpan"); ?>';
				if (window.location.href == thisURLSuccess ) {
					$('.cap_sukses').show();
				}
				
					console.log(thisURL);
				
				
		 	});


// just for the demos, avoids form submit


		</script>

<?php get_footer();?>